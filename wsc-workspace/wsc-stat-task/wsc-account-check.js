//每日数据汇总
const config = require('./config.js');
const mysqlUtil = require('./mysql-util.js');
const log4js = require('log4js');

log4js.configure({
    appenders: {
        today: {
            type: 'file',
            filename: '/home/deploy/app/wsc-stat-task/logs/today-account-check.log',
            pattern: 'trace-yyyy-MM-dd.log',
            alwaysIncludePattern: true
        }
    },
    categories: { default: { appenders: ['today'], level: 'debug' } }
});

const logger = log4js.getLogger('today');

let mysql = null;
var yester = 1;

Date.prototype.Format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "q+": Math.floor((this.getMonth() + 3) / 3) //季度 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

Date.prototype.addDays = function (days) {
    let d = new Date(this);
    d.setTime(d.getTime() + 24 * 60 * 60 * 1000);
    return d;
}

Date.prototype.subDays = function (days) {
    let d = new Date(this);
    d.setTime(d.getTime() - 24 * 60 * 60 * 1000);
    return d;
}

function getData(data) {
    return data instanceof Array ? Object.values(data[0])[0] : Object.values(data)[0]
}

let before_day=1;
//个人日结账单
async function wsc_check_account(isfirst,startTemp, endTemp) {

    //插入数据
    let insert_wsc_check = `	
    insert into wsc_account_check(uid,clear_time,phone,username) select fId ,'${startTemp}',fTelephone,floginName from fuser;
    `
    await mysql.queryAsync(insert_wsc_check);

    logger.info("check account stat.");



    await mysql.startTransactionAsync();

    if(isfirst){

    //今日余额wsc_out usd  wsc兑换usdt
    let wsc_usd_today = `UPDATE wsc_account_check w
    INNER JOIN (
        SELECT
            userId,
            IFNULL( sum( outerAmout ), 0 ) AS total 
        FROM
            c2c_order 
        WHERE
            outerCurrencyId = 58 
            AND innerCurrencyId = 6 
            AND orderStatus = 2 
            AND createDate >= '${startTemp}' 
            AND createDate < '${endTemp}' 
        GROUP BY
            userId 
        ) AS t1 
        SET w.wsc_out = t1.total
    WHERE
        t1.userId = w.uid AND w.clear_time='${startTemp}'`

    await mysql.queryAsync(wsc_usd_today);
    logger.info("update today wsc_usd_today_out stat.");

    //wsc_out eth  wsc兑换eth
    let wsc_eth_today = `UPDATE wsc_account_check w
    INNER JOIN (
        SELECT
            userId,
            IFNULL( sum( outerAmout ), 0 ) AS total 
        FROM
            c2c_order 
        WHERE
            outerCurrencyId = 58 
            AND innerCurrencyId = 4 
            AND orderStatus = 2 
            AND createDate >= '${startTemp}' 
            AND createDate < '${endTemp}' 
        GROUP BY
            userId 
        ) AS t1 
        SET w.wsc_out = t1.total+w.wsc_out 
    WHERE
        t1.userId = w.uid AND w.clear_time='${startTemp}'`

    await mysql.queryAsync(wsc_eth_today);
    logger.info("update today wsc_eth_today stat.");

    //wsc收益
    let wsc_value = `	
    UPDATE wsc_account_check w INNER JOIN  ( SELECT user_id, IFNULL( sum( rec_num ), 0 ) AS total FROM fvirtualwallet_swc_records WHERE in_type = 0 AND creat_time >= '${startTemp}' 
        AND creat_time < '${endTemp}' 
        GROUP BY
            user_id 
        ) AS t1 
        SET w.wsc_dig = CAST(t1.total AS decimal(28,8)) 
    WHERE
        t1.user_id = w.uid AND w.clear_time='${startTemp}'`
    await mysql.queryAsync(wsc_value);
    logger.info("update today wsc_value stat.");


    //昨日余额
    let yester_balance = `UPDATE wsc_account_check w INNER JOIN ( SELECT fuid, IFNULL( sum( fTotal ), 0 ) AS total FROM fvirtualwallet_main WHERE fVi_fId = 58 
    AND fLastUpdateTime >=DATE_SUB('${startTemp}',INTERVAL '${before_day}' DAY) 
	AND fLastUpdateTime < '${startTemp}' 
	GROUP BY
		fuid 
	) AS t1 
	    SET w.yester_balance = CAST(
	    t1.total AS DECIMAL ( 28, 8 )) 
    WHERE
        t1.fuid = w.uid AND w.clear_time='${startTemp}'`
    await mysql.queryAsync(yester_balance);
    logger.info("update today yester_balance stat.");



    //今日销毁的wsc的数量
    let wsc_destory = `UPDATE wsc_account_check w INNER JOIN ( SELECT userId, IFNULL( sum( gratuity ), 0 ) AS total FROM c2c_order WHERE createDate >= '${startTemp}' 
    	AND createDate < '${endTemp}' 
    	GROUP BY
    		userId 
    	) AS t1 
    	SET w.wsc_destory = CAST(
        t1.total AS DECIMAL ( 28, 8 ))
    WHERE
        t1.userId = w.uid AND w.clear_time='${startTemp}'`;
    await mysql.queryAsync(wsc_destory);
    logger.info("update today wsc_destory stat.");
    }

    //更新今日余额
    let today_balance = `UPDATE wsc_account_check 
        SET today_balance = IFNULL( yester_balance, 0 )+ IFNULL( wsc_dig, 0 )- IFNULL( wsc_out, 0 )-IFNULL( wsc_destory, 0 )
         WHERE clear_time = '${startTemp}'`
    await mysql.queryDataAsync(today_balance);

    logger.info("update today today_balance stat.");

    //平账
    let account_differ=`UPDATE wsc_account_check 
    SET account_differ = IFNULL( yester_balance, 0 )+ IFNULL( wsc_dig, 0 )- IFNULL( wsc_out, 0 )-IFNULL( wsc_destory, 0 )-IFNULL( today_balance, 0 )
     WHERE clear_time = '${startTemp}'`;
    await mysql.queryAsync(account_differ);

    await mysql.commitAsync();
    console.log("end");



}


async function startService() {
    logger.info("compute starting...");
    try {
        mysql = await mysqlUtil.connectAsync(config.dbOption);
    } catch (e) {
        logger.error("mysql conn failed." + e);
    }

    if (mysql == null) {
        logger.error("mysql conn failed.");
        return;
    }
    logger.info("compute end.");
    let isfirst=false;
    while (true) {
        let start = new Date();
        let startTemp = start.getFullYear() + "-" + (start.getMonth() + 1) + "-" + start.getDate();
        let secondes = start.getSeconds();
        let dateTemp = start;
        dateTemp.setDate(dateTemp.getDate() + 1);
        let endTemp = dateTemp.getFullYear() + "-" + (dateTemp.getMonth() + 1) + "-" + dateTemp.getDate();
        console.log(startTemp + "-" + secondes);
        console.log(secondes);
        logger.info("start record check_account  a  day  .. " + startTemp + endTemp);
        console.log(startTemp);
        let date = new Date();
        let start_date = date.Format('yyyy-MM-dd');
        let end_date = date.addDays(1).Format('yyyy-MM-dd');
       // await wsc_check_account(isfirst,start_date, end_date);
        await wsc_check_account(true, "2019-08-14", "2019-08-15");
        isfirst = true;
        // await mysql.closeAsync();
        // logger.info("check_account compture end");
        await new Promise((resolve, reject) => { setTimeout(resolve, 2000); });  //秒
       // await new Promise((resolve, reject) => { setTimeout(resolve, 1000 * 60 * 60 * 24 ); });  //秒
    }





}
startService();



