const mysql = require('mysql')

let queryAsync = function(sql, data) {
  return new Promise((resolve, reject)=>{
    this.query(sql, data, (err, results, fields) => {
      if (err) {
        reject(err)
      } else {
        resolve(results)
      }
    })
  })
};

let queryDataAsync = function(sql, data) {
  return new Promise((resolve, reject)=>{
    this.query(sql, data, (err, results, fields) => {
      if (err) {
        reject(err)
      } else {
        let result = results instanceof Array ? Object.values(results[0])[0] : Object.values(results)[0];
        resolve(result)
      }
    })
  })
};

let startTransactionAsync = function() {
  return new Promise((resolve, reject)=>{
    this.beginTransaction((err) => {
      if (err) {
        reject(err)
      } else {
        resolve()
      }
    })
  })
};

let commitAsync = function() {
  return new Promise((resolve, reject)=>{
    this.commit((err) => {
      if (err) {
        reject(err)
      } else {
        resolve()
      }
    })
  })
};

let rollbackAsync = function() {
  return new Promise((resolve, reject)=>{
    this.rollback(() => {
      resolve()
    })
  })
};

//关闭连接
let closeAsync = function () {
  return new Promise((resolve, reject)=>{
    this.end(function (err) {
      if (err) {
          reject(err);
      }
      resolve();
    });
  });
}

let connectAsync = function(config) {
  let connection = mysql.createConnection(config);
  return new Promise((resolve,reject)=>{
    connection.connect((err)=> {
      if (err) {
        reject('[mysql] error connecting: ' + err)
      } else {
        connection.queryAsync = queryAsync;
        connection.queryDataAsync = queryDataAsync;
        connection.startTransactionAsync = startTransactionAsync;
        connection.commitAsync = commitAsync;
        connection.rollbackAsync = rollbackAsync;
        connection.closeAsync = closeAsync;
        resolve(connection)
      }
    })
  })
};


module.exports = {
  connectAsync: connectAsync
};