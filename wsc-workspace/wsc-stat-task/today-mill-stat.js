//每日数据汇总
const log4js = require('log4js');
const config = require('./config.js');
const mysqlUtil = require('./mysql-util.js');
const _ = require('./date-util.js');

log4js.configure({
    appenders: {
        today: {
            type: 'file',
            filename: '/home/deploy/app/wsc-stat-task/logs/today-mill-stat.log',
            pattern: 'trace-yyyy-MM-dd.log',
            alwaysIncludePattern: true
        }
    },
    categories: { default: { appenders: ['today'], level: 'debug' } }
});
const logger = log4js.getLogger('today');


var mysql = null;

var todayDataSum = {
    "added_today_mill_max": 0,
    "added_today_mill_middle": 0,
    "added_today_mill_min": 0,

    "redeem_mill_today_max": 0,
    "redeem_mill_today_middle": 0,
    "redeem_mill_today_min": 0,

    "due_toady_mill_max": 0,
    "due_toady_mill_middle": 0,
    "due_toady_mill_min": 0
}

//今日矿机 
async function today_mill_stat(start_date, end_date) {
    //今日新增大型矿机
    let sql = `select IFNULL(count(*),0) from mill_order  
        where mill_code = '大型矿机' and pay_status=1 
        and create_time >= '${start_date}' and create_time < '${end_date}' `;
    todayDataSum.added_today_mill_max = await mysql.queryDataAsync(sql);

    //今日新增中型矿机
    todayDataSum.added_today_mill_middle = await mysql.queryDataAsync(
        `select IFNULL(count(*),0) from mill_order 
         where mill_code = '中型矿机' and pay_status=1 and create_time  >= '${start_date}' and create_time < '${end_date}' `);

    //今日新增小型矿机
    todayDataSum.added_today_mill_min = await mysql.queryDataAsync(
        `select IFNULL(count(*),0) from mill_order 
         where mill_code = '小型矿机' and pay_status=1 and create_time  >= '${start_date}' and create_time < '${end_date}' `);


    //今日赎回大型矿机数量
    todayDataSum.redeem_mill_today_max = await mysql.queryDataAsync(
        `select IFNULL(count(*),0) from mill_recycle_order 
         where money = 5000 and create_time >= '${start_date}' and create_time < '${end_date}'  and status = 1 `);

    //今日赎回中型矿机数量
    todayDataSum.redeem_mill_today_middle = await mysql.queryDataAsync(
        `select IFNULL(count(*),0) from mill_recycle_order 
         where money = 1000 and create_time >= '${start_date}' and create_time < '${end_date}'  and status = 1 `);

    //今日赎回小型矿机数量
    todayDataSum.redeem_mill_today_min = await mysql.queryDataAsync(
        `select IFNULL(count(*),0) from mill_recycle_order 
         where money = 500 and create_time >= '${start_date}' and create_time < '${end_date}'  and status = 1 `);


    //今日到期大型矿机数量
    todayDataSum.due_toady_mill_max = await mysql.queryDataAsync(
        `select IFNULL(count(*),0) from my_mill 
         where mill_code  = '大型矿机' and end_time >=  '${start_date}' and end_time < DATE_SUB('${start_date}',INTERVAL 1 DAY)  and 'status'=0`);

    //今日到期中型矿机数量
    todayDataSum.due_toady_mill_middle = await mysql.queryDataAsync(
        `select IFNULL(count(*),0) from my_mill 
         where mill_code = '中型矿机' and end_time >=  '${start_date}' and end_time < DATE_SUB('${start_date}',INTERVAL 1 DAY)  and 'status'=0`);

    // 今日到期小型矿机数量
    todayDataSum.due_toady_mill_min = await mysql.queryDataAsync(
        `select IFNULL(count(*),0) from my_mill 
         where mill_code = '小型矿机' and end_time >=  '${start_date}' and  end_time < DATE_SUB('${start_date}',INTERVAL 1 DAY)  and 'status'=0 `);
}

//今日累计新增：矿机 
async function today_mill_total_stat(start_date, end_date) {

    logger.info("sel_sql_wsc_mill_statistics_summary exec end.");

    //累计新增大型矿机
    let added_mill_total_max = await mysql.queryDataAsync(`
    SELECT IFNULL(added_mill_total_max,0) FROM wsc_mill_statistics_summary
     WHERE  clear_time >=DATE_SUB('${start_date}',INTERVAL 1 DAY) and clear_time < '${start_date}' order by  clear_time desc limit 1`) + todayDataSum.added_today_mill_max;

    //累计新增中型矿机
    let added_mill_total_middle = await mysql.queryDataAsync(`
    SELECT IFNULL(added_mill_total_middle,0) FROM wsc_mill_statistics_summary
     WHERE  clear_time >=DATE_SUB('${start_date}',INTERVAL 1 DAY) and clear_time < '${start_date}' order by  clear_time desc limit 1`) + todayDataSum.added_today_mill_middle;

    //累计新增小型矿机
    let added_mill_total_min = await mysql.queryDataAsync(`
    SELECT IFNULL(added_mill_total_min,0) FROM wsc_mill_statistics_summary
     WHERE  clear_time >=DATE_SUB('${start_date}',INTERVAL 1 DAY) and clear_time < '${start_date}' order by  clear_time desc limit 1`) + todayDataSum.added_today_mill_min;


    //累计赎回大型矿机数量
    let redeem_mill_total_max = await mysql.queryDataAsync(`
    select IFNULL(redeem_mill_total_max,0) FROM wsc_mill_statistics_summary
     WHERE  clear_time >=DATE_SUB('${start_date}',INTERVAL 1 DAY) and clear_time < '${start_date}' order by  clear_time desc limit 1`) + todayDataSum.redeem_mill_today_max;

    //累计赎回中型矿机数量
    let redeem_mill_total_middle = await mysql.queryDataAsync(`
    select IFNULL(redeem_mill_total_middle,0) FROM wsc_mill_statistics_summary
     WHERE  clear_time >=DATE_SUB('${start_date}',INTERVAL 1 DAY) and clear_time < '${start_date}' order by  clear_time desc  limit 1`) + todayDataSum.redeem_mill_today_middle;

    //累计赎回小型矿机数量
    let redeem_mill_total_min = await mysql.queryDataAsync(`
    select IFNULL(redeem_mill_total_min,0) FROM wsc_mill_statistics_summary
     WHERE  clear_time >=DATE_SUB('${start_date}',INTERVAL 1 DAY) and clear_time < '${start_date}' order by  clear_time desc  limit 1`) + todayDataSum.redeem_mill_today_min;

    //累计到期大型矿机数量
    let due_mill_total_max = await mysql.queryDataAsync(`
    select IFNULL(due_mill_total_max,0) FROM wsc_mill_statistics_summary
     WHERE  clear_time >=DATE_SUB('${start_date}',INTERVAL 1 DAY) and clear_time < '${start_date}' order by  clear_time desc  limit 1`) + todayDataSum.due_toady_mill_max;

    //累计到期中型矿机数量
    let due_mill_total_middle = await mysql.queryDataAsync(`
    select IFNULL(due_mill_total_middle,0) FROM wsc_mill_statistics_summary
     WHERE  clear_time >=DATE_SUB('${start_date}',INTERVAL 1 DAY) and clear_time < '${start_date}' order by  clear_time desc  limit 1`) + todayDataSum.due_toady_mill_middle;

    // 累计到期小型矿机数量
    let due_mill_total_min = await mysql.queryDataAsync(`
    select IFNULL(due_mill_total_min,0) FROM wsc_mill_statistics_summary
     WHERE  clear_time >=DATE_SUB('${start_date}',INTERVAL 1 DAY) and clear_time < '${start_date}' order by  clear_time desc  limit 1`) + todayDataSum.due_toady_mill_min;

    let count = await mysql.queryDataAsync(
        `select IFNULL(count(*),0) from wsc_mill_statistics_summary
         where clear_time = '${start_date}' `);

    if (count == 0) {
        let sql = `insert into wsc_mill_statistics_summary
        (
            added_today_mill_max, added_today_mill_middle, added_today_mill_min,
    
            added_mill_total_max, added_mill_total_middle, added_mill_total_min,
    
            redeem_mill_today_max, redeem_mill_today_middle, redeem_mill_today_min,
    
            redeem_mill_total_max, redeem_mill_total_middle, redeem_mill_total_min,
    
            due_toady_mill_max, due_toady_mill_middle, due_toady_mill_min,
    
            due_mill_total_max, due_mill_total_middle, due_mill_total_min,
            
            clear_time
            ) 
        VALUES (
            ${todayDataSum.added_today_mill_max}, ${todayDataSum.added_today_mill_middle}, ${todayDataSum.added_today_mill_min},
    
            ${added_mill_total_max}, ${added_mill_total_middle}, ${added_mill_total_min},
    
            ${todayDataSum.redeem_mill_today_max}, ${todayDataSum.redeem_mill_today_middle}, ${todayDataSum.redeem_mill_today_min},
    
            ${redeem_mill_total_max}, ${redeem_mill_total_middle},${redeem_mill_total_min},
    
            ${todayDataSum.due_toady_mill_max}, ${todayDataSum.due_toady_mill_middle}, ${todayDataSum.due_toady_mill_min},
    
            ${due_mill_total_max}, ${due_mill_total_middle}, ${due_mill_total_min},
            
            '${start_date}'
            )`;

        console.log(sql);
        await mysql.queryDataAsync(sql);
        
        logger.info(`insert sql_wsc_mill_statistics_summary ${start_date} done.`);
    } else {
        let sql =
            `UPDATE wsc_mill_statistics_summary m1
    set 
        m1.added_today_mill_max = ${todayDataSum.added_today_mill_max},
        m1.added_today_mill_middle = ${todayDataSum.added_today_mill_middle},
        m1.added_today_mill_min = ${todayDataSum.added_today_mill_min},

        m1.added_mill_total_max  =   ${added_mill_total_max},
        m1.added_mill_total_middle = ${added_mill_total_middle},
        m1.added_mill_total_min =  ${added_mill_total_min}, 

        m1.redeem_mill_today_max = ${todayDataSum.redeem_mill_today_max},
        m1.redeem_mill_today_middle = ${todayDataSum.redeem_mill_today_middle}, 
        m1.redeem_mill_today_min  = ${todayDataSum.redeem_mill_today_min}, 

        m1.redeem_mill_total_max = ${redeem_mill_total_max},
        m1.redeem_mill_total_middle =${redeem_mill_total_middle}, 
        m1.redeem_mill_total_min = ${redeem_mill_total_min},

        m1.due_toady_mill_max =  ${todayDataSum.due_toady_mill_max},
        m1.due_toady_mill_middle = ${todayDataSum.due_toady_mill_middle}, 
        m1.due_toady_mill_min =${todayDataSum.due_toady_mill_min},

        m1.due_mill_total_max =  ${due_mill_total_max},
        m1.due_mill_total_middle= ${due_mill_total_middle},
        m1.due_mill_total_min =  ${due_mill_total_min},
        m1.clear_time ='${start_date}'
        WHERE m1.clear_time = '${start_date}' `

        console.log(sql);
        await mysql.queryDataAsync(sql);

        logger.info(`update swsc_mill_statistics_summary ${start_date} done.`);
    }
}

async function startService() {
    logger.info("compute starting...");
    try {
        mysql = await mysqlUtil.connectAsync(config.dbOption);
    } catch (e) {
        logger.error("mysql conn failed." + e);
    }

    if (mysql == null) {
        logger.error("mysql conn failed.");
        return;
    }

    let start = new Date();
    let start_date = start.Format('yyyy-MM-dd');
    let end_date = start.addDays(1).Format('yyyy-MM-dd');
    
    await today_mill_stat(start_date, end_date);
    await today_mill_total_stat(start_date, end_date);

    await mysql.closeAsync();
    logger.info("compute end.");
}

//执行初始化
async function init() {
    logger.info("start init...");
    console.log("start init..." );
    try {
        mysql = await mysqlUtil.connectAsync(config.dbOption);
    } catch (e) {
        logger.error("mysql conn failed." + e);
    }
    if (mysql == null) {
        logger.error("mysql conn failed.");
        return;
    }

    console.log("start ... ");

    let start = new Date(2020, 4 - 1, 17, 0, 0); // 4 月 17 日
    let end = new Date(2020, 4 - 1, 28, 0, 0, 0);

    for (let d = start; d.getTime() < end.getTime(); d = d.addDays(1)) {
        let start_date = d.Format('yyyy-MM-dd 00:00:00');
        let end_date = d.addDays(1).Format('yyyy-MM-dd 00:00:00');
        
        logger.info("start compute : " + start_date);
        console.log("start compute : " + start_date);
        
        await today_mill_stat(start_date, end_date);
        await today_mill_total_stat(start_date, end_date);

        console.log("end : " + end_date);
        logger.info("end : " + end_date);
    }

    await mysql.closeAsync();
    console.log("end init." );
    logger.info("end init.");
}

init();

//startService();
