//每日数据汇总
const config = require('./config.js');
const mysqlUtil = require('./mysql-util.js');
const log4js = require('log4js');

log4js.configure({
    appenders: {
        today: {
            type: 'file',
            filename: '/home/deploy/app/wsc-stat-task/logs/today-eth-account-check.log',
            pattern: 'trace-yyyy-MM-dd.log',
            alwaysIncludePattern: true
        }
    },
    categories: { default: { appenders: ['today'], level: 'debug' } }
});

const logger = log4js.getLogger('today');

let mysql = null;
let before_day = 1;

Date.prototype.Format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "q+": Math.floor((this.getMonth() + 3) / 3) //季度 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

Date.prototype.addDays = function (days) {
    let d = new Date(this);
    d.setTime(d.getTime() + 24 * 60 * 60 * 1000);
    return d;
}

Date.prototype.subDays = function (days) {
    let d = new Date(this);
    d.setTime(d.getTime() - 24 * 60 * 60 * 1000);
    return d;
}


//个人日结账单
async function eth_check_account(isfirst, startTemp, endTemp) {

    //插入数据
    let insert_wsc_check = `	
    insert into eth_account_check(uid,clear_time,phone,username) select fId ,'${startTemp}',fTelephone,floginName from fuser;
    `
    await mysql.queryAsync(insert_wsc_check);

    logger.info("check eth account stat.");



    await mysql.startTransactionAsync();

    if (isfirst) {
        console.log("start");
        //今日充值ETH数量
        let eth_top_on = `UPDATE eth_account_check e
    INNER JOIN (
        SELECT
            IFNULL( sum( fAmount ), 0 ) AS total,
            FUs_fId2 AS userId 
        FROM
            fvirtualcaptualoperation 
        WHERE
            fVi_fId2 = 4 
            AND fType = 1 
            AND fStatus = 3 
            AND fCreateTime >= '${startTemp}' 
            AND fCreateTime < '${endTemp}' 
        GROUP BY
            FUs_fId2 
        ) AS t1 
        SET e.eth_top_up = t1.total 
    WHERE
        e.uid = t1.userId 
        AND e.clear_time = '${startTemp}'`
        await mysql.queryAsync(eth_top_on);
        logger.log("eth eth_top_on account update  ")


        //eth提现
        let eth_withdraw = `UPDATE eth_account_check e
    INNER JOIN (
        SELECT
            IFNULL( sum( fAmount ), 0 ) AS total,
            FUs_fId2 AS userId 
        FROM
            fvirtualcaptualoperation 
        WHERE
            fVi_fId2 = 4 
            AND fType = 2 
            AND fStatus = 3 
            AND fCreateTime >= '${startTemp}' 
            AND fCreateTime < '${endTemp}' 
        GROUP BY
            FUs_fId2 
        ) AS t1 
        SET e.eth_withdraw = t1.total 
    WHERE
        e.uid = t1.userId 
        AND e.clear_time = '${startTemp}'`
        await mysql.queryAsync(eth_withdraw);
        logger.log("eth eth_withdraw account update ")

        //eth兑入 wsc兑入eth
        let wsc_eth_in = `UPDATE eth_account_check e
    INNER JOIN (
        SELECT
            userId,
            IFNULL( sum( innerAmout ), 0 ) AS total 
        FROM
            c2c_order 
        WHERE
            outerCurrencyId = 58 
            AND innerCurrencyId = 4 
            AND orderStatus = 2 
            AND createDate >= '${startTemp}' 
            AND createDate < '${endTemp}' 
        GROUP BY
            userId 
        ) AS t1 
        SET e.eth_in = t1.total 
    WHERE
        e.uid = t1.userId 
        AND e.clear_time = '${startTemp}'`
        await mysql.queryAsync(wsc_eth_in);
        logger.log("wsc_eth_in  account update ")
        //usdt兑入eth
        let usdt_eth_in = `UPDATE eth_account_check e
        INNER JOIN (
            SELECT
                IFNULL( sum( innerAmout ), 0 ) AS total,
                userId 
            FROM
                c2c_order 
            WHERE
                outerCurrencyId = 6 
                AND innerCurrencyId = 4 
                AND orderStatus = 2 
                AND createDate >= '${startTemp}' 
                AND createDate < '${endTemp}' 
            GROUP BY
                userId 
            ) AS t1 
            SET e.eth_in = e.eth_in + t1.total 
        WHERE
            e.uid = t1.userId 
            AND e.clear_time = '${startTemp}'`
        await mysql.queryAsync(usdt_eth_in);
        logger.log("eth usdt_eth_in account update");
        //eth兑出usdt
        let eth_usdt_out = `UPDATE eth_account_check e
        INNER JOIN (
            SELECT
                userId,
                IFNULL( sum( outerAmout ), 0 ) AS total
            FROM
                c2c_order 
            WHERE
                outerCurrencyId = 4 
                AND innerCurrencyId = 6 
                AND orderStatus = 2 
                AND createDate >= '${startTemp}' 
                AND createDate < '${endTemp}' 
            GROUP BY
                userId 
            ) AS t1 
            SET e.eth_out = t1.total
        WHERE
            e.uid = t1.userId 
            AND e.clear_time = '${startTemp}'`
        await mysql.queryAsync(eth_usdt_out);
        logger.log("eth eth_usdt_out account update");

        //昨日余额
        let yester_balance = `UPDATE wsc_account_check w
        INNER JOIN (
            SELECT
                fuid,
                IFNULL( sum( fTotal ), 0 ) AS total 
            FROM
                fvirtualwallet_main 
            WHERE
                fVi_fId = 4 
                AND fLastUpdateTime > SUBDATE( '${startTemp}', INTERVAL '${before_day}' DAY ) 
                AND fLastUpdateTime < '${startTemp}' 
            GROUP BY
                fuid 
            ) AS t1 
            SET w.yester_balance = CAST(
            t1.total AS DECIMAL ( 28, 8 )) 
        WHERE
            t1.fuid = w.uid 
            AND w.clear_time = '${startTemp}'`;
        await mysql.queryAsync(yester_balance);
        console.log("eth yester_balance account update");


    }

    //更新今日余额
    let today_balance = `UPDATE eth_account_check 
    SET today_balance = IFNULL( yester_balance, 0 )+ IFNULL( eth_top_up, 0 )+ IFNULL( eth_in, 0 )- IFNULL( eth_withdraw, 0 )- IFNULL( eth_out, 0 ) 
    WHERE
        clear_time = '${startTemp}'`;
    await mysql.queryDataAsync(today_balance);

    //平账
    let account_differ = `UPDATE eth_account_check 
    SET account_differ = IFNULL( yester_balance, 0 )+ IFNULL( eth_top_up, 0 )+ IFNULL( eth_in, 0 )- IFNULL( eth_withdraw, 0 )- IFNULL( eth_out, 0 ) - IFNULL( today_balance, 0)
    WHERE
        clear_time = '${startTemp}'`;
    await mysql.queryAsync(account_differ);

    await mysql.commitAsync();

}


async function startService() {
    logger.info("compute starting...");
    try {
        mysql = await mysqlUtil.connectAsync(config.dbOption);
    } catch (e) {
        logger.error("mysql conn failed." + e);
    }

    if (mysql == null) {
        logger.error("mysql conn failed.");
        return;
    }
    logger.info("compute end.");
    let isfirst = false;
    while (true) {
        let start = new Date();
        let startTemp = start.getFullYear() + "-" + (start.getMonth() + 1) + "-" + start.getDate();
        let secondes = start.getSeconds();
        let dateTemp = start;
        dateTemp.setDate(dateTemp.getDate() + 1);
        let endTemp = dateTemp.getFullYear() + "-" + (dateTemp.getMonth() + 1) + "-" + dateTemp.getDate();
        console.log(startTemp + "-" + secondes);
        console.log(secondes);
        logger.info("start record check_account  a  day  .. " + startTemp + endTemp);
        console.log(startTemp);
        let date = new Date();
        let start_date = date.Format('yyyy-MM-dd');
        let end_date = date.addDays(1).Format('yyyy-MM-dd');
        await eth_check_account(isfirst, start_date, end_date);
        //await eth_check_account(true, "2019-08-05", "2019-08-06");
        isfirst = true;
        // await mysql.closeAsync();
        // logger.info("check_account compture end");
        await new Promise((resolve, reject) => { setTimeout(resolve, 1000 * 60 * 60 * 24 ); });  //秒
    }


}
startService();



