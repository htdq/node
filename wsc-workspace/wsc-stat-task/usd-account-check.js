//每日数据汇总
const config = require('./config.js');
const mysqlUtil = require('./mysql-util.js');
const log4js = require('log4js');

log4js.configure({
    appenders: {
        today: {
            type: 'file',
            filename: '/home/deploy/app/wsc-stat-task/logs/today-usd-account-check.log',
            pattern: 'trace-yyyy-MM-dd.log',
            alwaysIncludePattern: true
        }
    },
    categories: { default: { appenders: ['today'], level: 'debug' } }
});

const logger = log4js.getLogger('today');

let mysql = null;
let before_day = 1;

Date.prototype.Format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "q+": Math.floor((this.getMonth() + 3) / 3) //季度 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

Date.prototype.addDays = function (days) {
    let d = new Date(this);
    d.setTime(d.getTime() + 24 * 60 * 60 * 1000);
    return d;
}

Date.prototype.subDays = function (days) {
    let d = new Date(this);
    d.setTime(d.getTime() - 24 * 60 * 60 * 1000);
    return d;
}


//个人日结账单
async function usd_check_account(isfirst, startTemp, endTemp) {

    //插入数据
    let insert_wsc_check = `	
    insert into eth_account_check(uid,clear_time,phone,username) select fId ,'${startTemp}',fTelephone,floginName from fuser;;
    `
    await mysql.queryAsync(insert_wsc_check);

    logger.info("check eth account stat.");



    await mysql.startTransactionAsync();

    if (isfirst) {
        //USD转出
        let usd_roll_out = `UPDATE usd_account_check u
        INNER JOIN (
            SELECT
                IFNULL( SUM( amout ), 0 ) AS total,
                outerUser AS username 
            FROM
                transfer_order 
            WHERE
                orderStatus = 2 
                AND outerCurrencyId = 6 
                AND createDate >= '${startTemp}' 
                AND createDate < '${endTemp}' 
            GROUP BY
                outerUser 
            ) AS t1 
            SET u.usd_roll_out = t1.total 
        WHERE
            t1.username = u.username 
            AND u.clear_time = '${startTemp}'`;
        await mysql.queryAsync(usd_roll_out);
        logger.log("usd usd_roll_out account update");

        //USD转入
        let usd_into = `UPDATE usd_account_check u
        INNER JOIN (
            SELECT
                IFNULL( SUM( amout ), 0 ) AS total,
                innerUser AS username 
            FROM
                transfer_order 
            WHERE
                orderStatus = 2 
                AND outerCurrencyId = 6 
                AND createDate >= '${startTemp}' 
                AND createDate < '${endTemp}' 
            GROUP BY
                innerUser 
            ) AS t1 
            SET u.usd_into = t1.total 
        WHERE
            t1.username = u.username 
            AND u.clear_time = '${startTemp}'`;
        mysql.queryAsync(usd_into);
        logger.log("usd usd_into account update");
        //USD兑出
        let usd_out = `UPDATE usd_account_check u
        INNER JOIN (
            SELECT
                userId,
                IFNULL( sum( outerAmout ), 0 ) AS total 
            FROM
                c2c_order 
            WHERE
                outerCurrencyId = 6 
                AND innerCurrencyId = 4 
                AND orderStatus = 2 
                AND createDate >= '${startTemp}' 
                AND createDate < '${endTemp}' 
            GROUP BY
                userId 
            ) AS t1 
            SET u.usd_out = t1.total 
        WHERE
            t1.userId = u.uid 
            AND u.clear_time = '${startTemp}'`;
        await mysql.queryAsync(usd_out);
        logger.log("usd usd_out account update");


        //WSC兑换USD  兑入usd
        let wsc_usd_in = `UPDATE usd_account_check u
        INNER JOIN (
            SELECT
                userId,
                IFNULL( sum( innerAmout ), 0 ) AS total 
            FROM
                c2c_order 
            WHERE
                outerCurrencyId = 58 
                AND innerCurrencyId = 6 
                AND orderStatus = 2 
                AND createDate >= '${startTemp}' 
                AND createDate < '${endTemp}' 
            GROUP BY
                userId 
            ) AS t1 
            SET u.usd_in = t1.total 
        WHERE
            t1.userId = u.uid 
            AND u.clear_time = '${startTemp}'`;
        await mysql.queryAsync(wsc_usd_in);
        logger.log(" usd wsc_usd_in account update");

        //ETH兑换USD 兑入usd
        let eth_usd_in = `UPDATE usd_account_check u
        INNER JOIN (
            SELECT
                userId,
                IFNULL( sum( innerAmout ), 0 ) AS total 
            FROM
                c2c_order 
            WHERE
                outerCurrencyId = 4 
                AND innerCurrencyId = 6 
                AND orderStatus = 2 
                AND createDate >= '${startTemp}' 
                AND createDate < '${endTemp}' 
            GROUP BY
                userId 
            ) AS t1 
            SET u.usd_in = t1.total + u.usd_in 
        WHERE
            t1.userId = u.uid 
            AND u.clear_time = '${startTemp}'`;

        await mysql.queryAsync(eth_usd_in);
        logger.log("usd eth_usd_in account update");

        //昨日余额
        let yester_balance = `UPDATE usd_account_check u
        INNER JOIN (
            SELECT
                fuid,
                IFNULL( sum( fTotal ), 0 ) AS total 
            FROM
                fvirtualwallet_main 
            WHERE
                fVi_fId = 6 
                AND fLastUpdateTime > SUBDATE( '${startTemp}', INTERVAL '${before_day}' DAY ) 
                AND fLastUpdateTime < '${startTemp}' 
            GROUP BY
                fuid 
            ) AS t1 
            SET u.yester_balance = CAST(
            t1.total AS DECIMAL ( 28, 8 )) 
        WHERE
            t1.fuid = u.uid 
            AND u.clear_time = '${startTemp}'`;
        await mysql.queryAsync(yester_balance);
        console.log("usd yester_balance account update");

    }

    //更新今日余额
    let today_balance = `UPDATE usd_account_check 
    SET today_balance = IFNULL( yester_balance, 0 )+ IFNULL( usd_into, 0 )+ IFNULL( usd_in, 0 )- IFNULL( usd_roll_out, 0 )- IFNULL( usd_out, 0 ) 
    WHERE
        clear_time = '${startTemp}'`;
    await mysql.queryDataAsync(today_balance);

    //平账
    let account_differ = `UPDATE usd_account_check 
    SET account_differ = IFNULL( yester_balance, 0 )+ IFNULL( usd_into, 0 )+ IFNULL( usd_in, 0 )- IFNULL( usd_roll_out, 0 )- IFNULL( usd_out, 0 ) - IFNULL( today_balance, 0)
    WHERE
        clear_time = '${startTemp}'`;
    await mysql.queryAsync(account_differ);
    logger.log("usd account end");
    console.log("end");
    await mysql.commitAsync();

}


async function startService() {
    logger.info("compute starting...");
    try {
        mysql = await mysqlUtil.connectAsync(config.dbOption);
    } catch (e) {
        logger.error("mysql conn failed." + e);
    }

    if (mysql == null) {
        logger.error("mysql conn failed.");
        return;
    }
    logger.info("compute end.");
    let isfirst = false;
    while (true) {
        let start = new Date();
        let startTemp = start.getFullYear() + "-" + (start.getMonth() + 1) + "-" + start.getDate();
        let secondes = start.getSeconds();
        let dateTemp = start;
        dateTemp.setDate(dateTemp.getDate() + 1);
        let endTemp = dateTemp.getFullYear() + "-" + (dateTemp.getMonth() + 1) + "-" + dateTemp.getDate();
        console.log(startTemp + "-" + secondes);
        console.log(secondes);
        logger.info("start record check_account  a  day  .. " + startTemp + endTemp);
        console.log(startTemp);
        let date = new Date();
        let start_date = date.Format('yyyy-MM-dd');
        let end_date = date.addDays(1).Format('yyyy-MM-dd');
        await usd_check_account(isfirst, start_date, end_date);
        isfirst = true;
        logger.info("check_account compture end");
        await new Promise((resolve, reject) => { setTimeout(resolve, 1000 * 60 * 60 * 24 ); });  //秒
    }

    await mysql.closeAsync();
    console.log("end");


}
startService();



