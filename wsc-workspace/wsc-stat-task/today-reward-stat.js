﻿// 使用?表示占位，可以防止sql注入
const config = require('./config.js');
const mysqlUtil = require('./mysql-util.js');
const log4js = require('log4js');

log4js.configure({
    appenders: {
        today: {
            type: 'file',
            filename: '/home/deploy/app/wsc-stat-task/logs/today-reward-stat.log',
            pattern: 'trace-yyyy-MM-dd.log',
            alwaysIncludePattern: true
        }
    },
    categories: { default: { appenders: ['today'], level: 'debug' } }
});
const logger = log4js.getLogger('today');
let mysql = null;

function getData(data) {
    return data instanceof Array ? Object.values(data[0]) : Object.values(data)[0]
}
Date.prototype.Format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "q+": Math.floor((this.getMonth() + 3) / 3) //季度 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
Date.prototype.addDays = function (days) {
    let d = new Date(this);
    d.setTime(d.getTime() + 24 * 60 * 60 * 1000);
    return d;
}

var tempData = {
    "user_id": 0,
    "today_mining_profit": 0,
    "today_share_profit": 0,
    "today_community_profit": 0,
    "today_community_range": 0,
    "today_node_profit": 0,
    "today_node_range": 0,
    "today_level_profit": 0,
    "today_bonus_profit": 0,
    "today_supernode_profit": 0
}


//收益汇总
async function totalImcomeData(start, end) {

    let user_all = `
        insert into wsc_reward_sum(user_id,clear_time,phone) select fId ,'${start}',fTelephone from fuser`;
    await mysql.queryAsync(user_all);

    //今日所有用户数据
    var all_user = await mysql.queryAsync(
        `select t.user_id, t.in_type, t.is_cal, sum(t.rec_num) as rec_num , sum(t.usd_num) as usd_num
        from fvirtualwallet_swc_records t
        where creat_time > '${start}' and creat_time < '${end}'
        group by t.user_id, t.in_type, t.is_cal`);

    await mysql.startTransactionAsync();

    for (let i = 0; i < all_user.length; i++) {
        let in_type = all_user[i].in_type;
        let is_cal = all_user[i].is_cal;
        let user_id = all_user[i].user_id;
        if (in_type == 0) {
            tempData.today_mining_profit = all_user[i].rec_num;
            let wsc_reward_total = `
            update wsc_reward_sum set 
            today_mining_profit = ${ tempData.today_mining_profit}
            where user_id =${user_id} and clear_time ='${start}';`;
            await mysql.queryAsync(wsc_reward_total);
        }
        if (in_type == 1 && is_cal == 0) {
            tempData.today_share_profit = all_user[i].usd_num;
            let wsc_reward_total = `
            update wsc_reward_sum 
            set 
            today_share_profit = ${ tempData.today_share_profit}
            where user_id =${user_id} and clear_time ='${start}';`;
            await mysql.queryAsync(wsc_reward_total);
        }
        if (in_type == 2 && is_cal == 0) {
            tempData.today_community_profit = all_user[i].usd_num;
            let wsc_reward_total = `
            update wsc_reward_sum 
            set 
            today_community_profit = ${ tempData.today_community_profit}
            where user_id =${user_id} and clear_time ='${start}';`;
            await mysql.queryAsync(wsc_reward_total);
        }
        if (in_type == 2 && is_cal == 1) {
            tempData.today_community_range = all_user[i].usd_num;
            let wsc_reward_total = `
            update wsc_reward_sum 
            set 
            today_community_range = ${ tempData.today_community_range}
            where user_id =${user_id} and clear_time ='${start}';`;
            await mysql.queryAsync(wsc_reward_total);
        }
        if (in_type == 5 && is_cal == 0) {
            tempData.today_node_profit = all_user[i].usd_num;
            let wsc_reward_total = `
            update wsc_reward_sum 
            set 
            today_node_profit= ${ tempData.today_node_profit}
            where user_id =${user_id} and clear_time ='${start}';`;
            await mysql.queryAsync(wsc_reward_total);
        }
        if (in_type == 5 && is_cal == 1) {
            tempData.today_node_range = all_user[i].usd_num;
            let wsc_reward_total = `
            update wsc_reward_sum 
            set 
            today_node_range = ${ tempData.today_node_range}
            where user_id =${user_id} and clear_time ='${start}';`;
            await mysql.queryAsync(wsc_reward_total);
        }
        if (in_type == 5 && is_cal == 2) {
            tempData.today_node_range += all_user[i].usd_num;
            let wsc_reward_total = `
            update wsc_reward_sum 
            set 
            today_node_range = ${ tempData.today_node_range}
            where user_id =${user_id} and clear_time ='${start}';`;
            await mysql.queryAsync(wsc_reward_total);
        }
        if (in_type == 2 && is_cal == 2) {
            tempData.today_level_profit = all_user[i].usd_num;
            let wsc_reward_total = `
            update wsc_reward_sum 
            set 
            today_level_profit = ${ tempData.today_level_profit}
            where user_id =${user_id} and clear_time ='${start}';`;
            await mysql.queryAsync(wsc_reward_total);
        }
        if (in_type == 3 && is_cal == 0) {
            tempData.today_bonus_profit = all_user[i].usd_num;
            let wsc_reward_total = `
            update wsc_reward_sum 
            set 
            today_bonus_profit = ${ tempData.today_bonus_profit}
            where user_id =${user_id} and clear_time ='${start}';`;
            await mysql.queryAsync(wsc_reward_total);
        }
        if (in_type == 4 && is_cal == 0) {
            tempData.today_supernode_profit = all_user[i].usd_num;
            let wsc_reward_total = `
            update wsc_reward_sum 
            set today_supernode_profit = ${ tempData.today_supernode_profit}
            where user_id =${user_id} and clear_time ='${start}';`;
            await mysql.queryAsync(wsc_reward_total);
        }
    }

    await mysql.commitAsync();

    let wsc_reward_total = `
    update wsc_reward_sum t1 left join wsc_reward_sum t2 on t1.user_id = t2.user_id and t1.clear_time=(t2.clear_time + interval 1 day)
    set t1.total_mining_profit= IFNULL((t1.today_mining_profit + t2.total_mining_profit),0),
     t1.total_share_profit= IFNULL((t1.today_share_profit + t2.total_share_profit),0),
     t1.total_community_profit= IFNULL((t1.today_community_profit + t2.total_community_profit),0),
     t1.total_community_range= IFNULL((t1.today_community_range + t2.total_community_range),0),
     t1.total_node_profit= IFNULL((t1.today_node_profit + t2.total_node_profit),0),
     t1.total_node_range= IFNULL((t1.today_node_range + t2.total_node_range),0),
     t1.total_level_profit= IFNULL((t1.today_level_profit + t2.total_level_profit),0),
     t1.total_bonus_profit= IFNULL((t1.today_bonus_profit + t2.total_bonus_profit),0),
     t1.total_supernode_profit= IFNULL((t1.today_supernode_profit + t2.total_supernode_profit),0)
    where t1.clear_time='${start}'`;
    await mysql.queryAsync(wsc_reward_total);

}

//执行每天
async function startService() {
    logger.info("compute starting...");
    try {
        mysql = await mysqlUtil.connectAsync(config.dbOption);
    } catch (e) {
        logger.error("mysql conn failed." + e);
    }
    if (mysql == null) {
        logger.error("mysql conn failed.");
        return;
    }

    //while (true) {
    
    let start = new Date();
    let startStr = start.Format('yyyy-MM-dd 00:00:00');
    let endStr = start.addDays(1).Format('yyyy-MM-dd 00:00:00');
    
    await totalImcomeData(startStr, endStr);

    //    await new Promise((resolve, reject) => { setTimeout(resolve, 3600000); });
    //}
    await mysql.closeAsync();
    logger.info("compute end.");
}

//执行初始化
async function initStartService() {
    try {
        mysql = await mysqlUtil.connectAsync(config.dbOption);
    } catch (e) {
        logger.error("mysql conn failed." + e);
    }
    if (mysql == null) {
        logger.error("mysql conn failed.");
        return;
    }

    console.log("start totalImcomeData ... ");
    console.time("测试 fn 速度: ");


    let start = new Date(2020, 4 - 1, 21, 0, 0); // 1 月 1 日
    let end = new Date(2020, 4 - 1, 24, 0, 0, 0);

    for (let d = start; d.getTime() < end.getTime(); d = d.addDays(1)) {
        let startStr = d.Format('yyyy-MM-dd 00:00:00');
        let endStr = d.addDays(1).Format('yyyy-MM-dd 00:00:00');
        logger.info("start compute : " + startStr);
        console.log("start compute : " + startStr);
        await totalImcomeData(startStr, endStr);
        console.log("end : " + endStr);
        logger.info("end : " + endStr);
    }

    //await totalImcomeData('2020-01-01', '2020-01-02');

    console.timeEnd("测试 fn 速度: ");
    console.log("                  ");
    console.log("下一次运行结果     ");
    await mysql.closeAsync();
    logger.info("service exit...");
}
//执行每天
startService();


//执行初始化
//initStartService();