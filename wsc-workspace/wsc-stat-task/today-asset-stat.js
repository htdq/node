//每日数据汇总
const config = require('./config.js');
const mysqlUtil = require('./mysql-util.js');
const log4js = require('log4js');

log4js.configure({
    appenders: {
        today: {
            type: 'file',
            filename: '/home/deploy/app/wsc-stat-task/logs/today-asset-stat.log',
            pattern: 'trace-yyyy-MM-dd.log',
            alwaysIncludePattern: true
        }
    },
    categories: { default: { appenders: ['today'], level: 'debug' } }
});

const logger = log4js.getLogger('today');

let mysql = null;
var yester = 1;

Date.prototype.Format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "q+": Math.floor((this.getMonth() + 3) / 3) //季度 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

Date.prototype.addDays = function (days) {
    let d = new Date(this);
    d.setTime(d.getTime() + 24 * 60 * 60 * 1000);
    return d;
}

Date.prototype.subDays = function (days) {
    let d = new Date(this);
    d.setTime(d.getTime() - 24 * 60 * 60 * 1000);
    return d;
}

function getData(data) {
    return data instanceof Array ? Object.values(data[0])[0] : Object.values(data)[0]
}

//今日：全站资产增长
async function sel_wsc_assets_data(startTemp, endTemp) {

    //全站ETH今日
    let total_eth = getData(await mysql.queryAsync(`
    select IFNULL(sum(fTotal),0) from fvirtualwallet_main  where fVi_fId = 4`));

    //全站USD今日
    let total_usd = getData(await mysql.queryAsync(`
    select IFNULL(sum(fTotal),0) from fvirtualwallet_main  where fVi_fId = 6`));

    //全站WSC今日
    let total_wsc = getData(await mysql.queryAsync(`
    select IFNULL(sum(fTotal1),0) from fvirtualwallet_main  where fVi_fId = 58`));


    //昨日全站ETH
    let eth_incr =  getData(await mysql.queryAsync(`
    SELECT IFNULL(sum(total_eth),0) from wsc_assets_data 
    where clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc  limit 1`));

    //昨日全站USD
    let usd_incr =  getData(await mysql.queryAsync(`
    SELECT IFNULL(sum(total_usd),0) from wsc_assets_data 
    where clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc limit 1`));

    //昨日全站WSC
    let wsc_incr =  getData(await mysql.queryAsync(`
    SELECT IFNULL(sum(total_wsc),0) from wsc_assets_data 
    where clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc  limit 1`));

 
    let count = getData(await mysql.queryAsync(
        `select IFNULL(count(total_eth),0) from wsc_assets_data where clear_time =  '${startTemp}' `));

    if (count == 0) {
        sql_wsc_assets_data = `insert into wsc_assets_data
            (
                 total_eth,
                 total_usd,
                 total_wsc,

                 eth_incr,
                 usd_incr,
                 wsc_incr,

                clear_time
            )
        VALUES(
            ${total_eth},
            ${total_usd},
            ${total_wsc},

            ${eth_incr == 0 ? a= 0 : a= total_eth - eth_incr},
            ${usd_incr == 0 ? a= 0 : a= total_usd - usd_incr},
            ${wsc_incr == 0 ? a= 0 : a= total_wsc - wsc_incr},

            '${startTemp}')`;
        await mysql.queryAsync(sql_wsc_assets_data);
        logger.info("insert a asset stat.");
    } else {
        let upd_wsc_assets_data =
            `UPDATE wsc_assets_data s1 set
            s1.total_eth= ${total_eth},
            s1.total_usd = ${total_usd},
            s1.total_wsc= ${total_wsc},
 
            s1.eth_incr= ${eth_incr},
            s1.usd_incr= ${usd_incr},
            s1.wsc_incr= ${wsc_incr},

            s1.clear_time = '${startTemp}'
            WHERE s1.clear_time = '${startTemp}'`
        await mysql.queryAsync(upd_wsc_assets_data);
        logger.info("update today asset stat.");
    }
}


async function startService() {
    logger.info("compute starting...");
    try {
        mysql = await mysqlUtil.connectAsync(config.dbOption);
    } catch (e) {
        logger.error("mysql conn failed." + e);
    }

    if (mysql == null) {
        logger.error("mysql conn failed.");
        return;
    }

    let start = new Date();
    let start_date = start.Format('yyyy-MM-dd');
    console.log(start_date);
    let end_date = start.addDays(1).Format('yyyy-MM-dd');
    console.log(end_date);
    await sel_wsc_assets_data(start_date, end_date);
    await mysql.closeAsync();
    logger.info("compute end.");
}
startService();



