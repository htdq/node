﻿//初始化数据汇总统计
const config = require('./config.js');
const mysqlUtil = require('./mysql-util.js');
const log4js = require('log4js');

log4js.configure({
    appenders: {
        init: {
            type: 'file',
            filename: 'logs/init',
            pattern: 'trace-yyyy-MM-dd.log',
            alwaysIncludePattern: true
        }
    },
    categories: { default: { appenders: ['init'], level: 'debug' } }
});
const logger = log4js.getLogger('init');
let mysql = null;

function getData(data) {
    return data instanceof Array ? Object.values(data[0])[0] : Object.values(data)[0]
}
Date.prototype.Format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "q+": Math.floor((this.getMonth() + 3) / 3) //季度 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
Date.prototype.addDays = function (days) {
    let d = new Date(this);
    d.setTime(d.getTime() + 24 * 60 * 60 * 1000);
    return d;
}

async function delete_init() {
    let delete_wsc_mill_statistics_summary = `
    delete from wsc_mill_statistics_summary`;

    let delete_wsc_statistics_summary = `
    delete from wsc_statistics_summary`;

    let delete_wsc_income_summary = `
    delete from wsc_income_summary`;

    await mysql.queryAsync(delete_wsc_mill_statistics_summary);
    await mysql.queryAsync(delete_wsc_statistics_summary);
    await mysql.queryAsync(delete_wsc_income_summary);

        logger.info("init data >>>>: ");
        console.log("init data >>>>: ");
}

var startDay = 0;
var yester = 1;

async function startService() {
    logger.info("service staring...");
    try {
        mysql = await mysqlUtil.connectAsync(config.dbOption);
    } catch (e) {
        logger.error("mysql conn failed." + e);
    }
    if (mysql == null) {
        logger.error("mysql conn failed.");
        return;
    }

    await delete_init();
    let start = new Date(2020, 3 - 1, 1, 0, 0); // 3 月 1 日
    let end = new Date(2020, 4 - 1, 17, 0, 0, 0);

    for (let d = start; d.getTime() < end.getTime(); d = d.addDays(1)) {
        let startTemp = d.Format('yyyy-MM-dd');
        let endTemp = d.addDays(1).Format('yyyy-MM-dd');

        logger.info("start compute : " + startTemp);
        console.log("start compute : " + startTemp);

        if (startDay == 0) {
            await dataSum(startTemp, endTemp);
        }
        await totalMillData(startTemp, endTemp);
        await totalImcomeData(startTemp, endTemp);
        await totalStatData(startTemp, endTemp);
        console.log("end : " + endTemp);
        console.log(" ");
        logger.info("end : " + endTemp);
        logger.info(" ");

        logger.info("service start..." + startTemp + endTemp);
        startDay++;
    }
    logger.info("service end.");
}

//今日矿机
async function todayMillData(ymd, end) {
    logger.info("start todayMillData ... ");

    //今日新增大型矿机
    todayDataSum.added_today_mill_max = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from mill_order  
   where mill_code = '大型矿机' and pay_status=1 and create_time >='${ymd}'  and create_time < '${end}'`));

    //今日新增中型矿机
    todayDataSum.added_today_mill_middle = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from mill_order  
    where mill_code = '中型矿机' and pay_status=1 and create_time >='${ymd}'  and create_time < '${end}'`));

    //今日新增小型矿机
    todayDataSum.added_today_mill_min = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from mill_order  
    where mill_code = '小型矿机' and pay_status=1 and create_time >='${ymd}'  and create_time < '${end}'`));


    //今日赎回大型矿机数量
    todayDataSum.redeem_mill_today_max = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from mill_recycle_order  
    where money = 5000 and status =1 and create_time >='${ymd}'  and create_time < '${end}'`));

    //今日赎回中型矿机数量
    todayDataSum.redeem_mill_today_middle = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from mill_recycle_order  
    where money = 1000 and status =1 and create_time >='${ymd}'  and create_time < '${end}'`));

    //今日赎回小型矿机数量
    todayDataSum.redeem_mill_today_min = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from mill_recycle_order  
    where money = 500 and status =1 and create_time >='${ymd}'  and create_time < '${end}'`));


    //今日到期大型矿机数量
    todayDataSum.due_toady_mill_max = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from my_mill  
    where money  = '5000' and status =0 and end_time >='${ymd}' and end_time <'${end}'`));

    //今日到期中型矿机数量
    todayDataSum.due_toady_mill_middle = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from my_mill  
    where money = '1000'and status =0 and end_time >='${ymd}' and end_time <'${end}'`));

    // 今日到期小型矿机数量
    todayDataSum.due_toady_mill_min = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from my_mill  
    where money = '500' and status =0 and end_time >='${ymd}' and end_time <'${end}'`));

    logger.info("todayMillData End ...");
}
//今日USD收益
async function todayImcomeData(ymd, end) {
    logger.info("start todayImcomeData ... ");

    //今日分享收益
    todayDataSum.today_revenue_sharing = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
            where in_type = 1 and is_cal = 0 and creat_time >='${ymd}'  and creat_time < '${end}'`));

    //今日社区收益
    todayDataSum.today_community_benefits = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
            where in_type = 2 and is_cal = 0 and creat_time >='${ymd}'  and creat_time < '${end}'`));

    //今日平级收益
    todayDataSum.today_peer_income = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records
            where in_type = 2 and is_cal = 2 and creat_time >='${ymd}'  and creat_time < '${end}'`));

    //今日节点收益
    todayDataSum.today_node_income = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
            where in_type = 5 and is_cal = 0 and creat_time >='${ymd}'  and creat_time < '${end}'`));

    //今日分红收益
    todayDataSum.today_dividend_income = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
            where in_type = 3 and is_cal = 0 and creat_time >='${ymd}'  and creat_time < '${end}'`));

    //今日社区极差
    todayDataSum.today_very_poor_community = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
            where in_type = 2 and is_cal = 1 and creat_time >='${ymd}'  and creat_time < '${end}'`));

    //今日节点极差
    todayDataSum.today_node_range = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
            where in_type = 5 and is_cal = 1 and creat_time >='${ymd}'  and creat_time < '${end}'`));

    //今日超级节点
    todayDataSum.today_supernode = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
            where in_type = 4 and is_cal = 0 and creat_time >='${ymd}'  and creat_time < '${end}'`));

    logger.info("todayImcomeData End ...");
}
//今日会员，转账，ETH，WSC
async function todayStatData(ymd, end) {
    logger.info("start todayStatData ... ");

    //今日新增用户数量
    todayDataSum.today_new_user = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from fuser 
            where fRegisterTime >='${ymd}'  and fRegisterTime < '${end}'`));

    //全站用户数量
    todayDataSum.total_user = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from fuser 
            where fRegisterTime < '${ymd}'`));

    //今日新增有效用户数量
    todayDataSum.added_today_user = getData(await mysql.queryAsync(
        `SELECT IFNULL(count(*),0)
            from (
                select fId,u.fLoginName,IFNULL(count(*),0),m.status 
                from fuser u inner JOIN my_mill m on u.fLoginName = m.fLoginName 
                where m.status!=0 and u.fRegisterTime >='${ymd}' and u.fRegisterTime < '${end}' GROUP BY u.fId 
                ) a`));

    //全站有效用户数量
    todayDataSum.all_valid_user = getData(await mysql.queryAsync(
        `SELECT IFNULL(count(*),0) from (
                select fId,u.fLoginName,IFNULL(count(*),0),m.status 
                from fuser u inner JOIN my_mill m on u.fLoginName = m.fLoginName 
                where m.status!=0 and u.fRegisterTime < '${ymd}'   GROUP BY u.fId 
                ) a `));


    //今日ETH兑换USD兑出ETH
    todayDataSum.eth_usd_today_eth = getData(await mysql.queryAsync(
        `select IFNULL(sum(outerAmout),0) from c2c_order  
            where outerCurrencyId = 4 and innerCurrencyId = 6 and orderStatus = 2 and createDate >='${ymd}' and createDate < '${end}'`));

    //今日ETH兑换USD兑入USD
    todayDataSum.eth_usd_today_usd = getData(await mysql.queryAsync(
        `select IFNULL(sum(innerAmout),0) from c2c_order  
            where outerCurrencyId = 4 and innerCurrencyId = 6 and orderStatus = 2 and createDate >='${ymd}' and createDate < '${end}'`));

    //今日WSC兑换USD兑出WSC
    todayDataSum.wsc_usd_today_wsc = getData(await mysql.queryAsync(
        `select IFNULL(sum(outerAmout),0) from c2c_order  
            where outerCurrencyId = 58 and innerCurrencyId = 6 and orderStatus = 2 and createDate >='${ymd}' and createDate < '${end}'`));

    //今日WSC兑换USD兑入USD
    todayDataSum.wsc_usd_today_usd = getData(await mysql.queryAsync(
        `select IFNULL(sum(innerAmout),0)  from c2c_order  
            where outerCurrencyId = 58 and innerCurrencyId = 6 and orderStatus = 2 and createDate >='${ymd}' and createDate < '${end}'`));

    //今日WSC兑换ETH兑出WSC
    todayDataSum.wsc_eth_today_wsc = getData(await mysql.queryAsync(
        `select IFNULL(sum(outerAmout),0) from c2c_order  
            where outerCurrencyId = 58 and innerCurrencyId = 4 and orderStatus = 2 and createDate >='${ymd}' and createDate < '${end}'`));

    //今日WSC兑换ETH兑入ETH
    todayDataSum.wsc_eth_today_eth = getData(await mysql.queryAsync(
        `select IFNULL(sum(innerAmout),0)  from c2c_order  
            where outerCurrencyId = 58 and innerCurrencyId = 4 and orderStatus = 2 and createDate >='${ymd}' and createDate < '${end}'`));

    //今日USD兑换ETH兑出USD
    todayDataSum.usd_eth_today_usd = getData(await mysql.queryAsync(
        `select IFNULL(sum(outerAmout),0) from c2c_order  
            where outerCurrencyId = 6 and innerCurrencyId = 4 and orderStatus = 2 and createDate >='${ymd}' and createDate < '${end}'`));

    //今日USD兑换ETH兑入ETH
    todayDataSum.usd_eth_today_eth = getData(await mysql.queryAsync(
        `select IFNULL(sum(innerAmout),0) from c2c_order  
            where outerCurrencyId = 6 and innerCurrencyId = 4 and orderStatus = 2 and createDate >='${ymd}' and createDate < '${end}'`));



    //今日充值ETH数量
    todayDataSum.recharge_eth_today = getData(await mysql.queryAsync(
        `select IFNULL(sum(fAmount),0) from fvirtualcaptualoperation 
            where fVi_fId2 = 4 and fType = 1 and fStatus = 3 and fCreateTime >='${ymd}' and fCreateTime < '${end}'`));

    //今日提现ETH数量
    todayDataSum.withdraw_eth_today = getData(await mysql.queryAsync(
        `select IFNULL(sum(fAmount),0) from fvirtualcaptualoperation 
            where fVi_fId2 = 4 and fType = 2 and fStatus = 3 and fCreateTime >='${ymd}' and fCreateTime < '${end}'`));


    //今日挖矿收益wsc
    todayDataSum.today_mining_income = getData(await mysql.queryAsync(`select IFNULL(sum(rec_num),0) from fvirtualwallet_swc_records  
        where in_type = 0 and creat_time >='${ymd}' and creat_time < '${end}'`));
    //今日销毁wsc
    todayDataSum.today_destroy = getData(await mysql.queryAsync(`select IFNULL(sum(gratuity),0) from c2c_order  
        where createDate >='${ymd}' and createDate < '${end}'`));

    logger.info("todayStatData End ...");
}

//昨天累计矿机数据
async function totalMillData(ymd, end) {
    logger.info("start totalMillData ... ");

    await todayMillData(ymd, end);

    var add_total_max = 0;
    var add_total_middle = 0;
    var add_total_min = 0;
    //第一次运行加上累计
    if (startDay != 0) {
        add_total_max = getData(await mysql.queryAsync(
            `SELECT IFNULL(added_mill_total_max,0) FROM wsc_mill_statistics_summary
             WHERE  clear_time >=DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
        add_total_middle = getData(await mysql.queryAsync(
            `SELECT IFNULL(added_mill_total_middle,0) FROM wsc_mill_statistics_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
        add_total_min = getData(await mysql.queryAsync(
            `SELECT IFNULL(added_mill_total_min,0) FROM wsc_mill_statistics_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
    }
    //累计新增大型矿机
    var added_mill_total_max = todayDataSum.added_today_mill_max + totalDataSum.added_mill_total_max + add_total_max;
    totalDataSum.added_mill_total_max = 0;
    //累计新增中型矿机
    let added_mill_total_middle = todayDataSum.added_today_mill_middle + totalDataSum.added_mill_total_middle + add_total_middle;
    totalDataSum.added_mill_total_middle = 0;
    //累计新增小型矿机
    let added_mill_total_min = todayDataSum.added_today_mill_min + totalDataSum.added_mill_total_min + add_total_min;
    totalDataSum.added_mill_total_min = 0;

    //获取昨天累计
    var redeem_total_max = 0;
    var redeem_total_middle = 0;
    var redeem_total_min = 0;
    if (startDay != 0) {
        redeem_total_max = getData(await mysql.queryAsync(
            `SELECT IFNULL(redeem_mill_total_max,0) FROM wsc_mill_statistics_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
        redeem_total_middle = getData(await mysql.queryAsync(
            `SELECT IFNULL(redeem_mill_total_middle,0) FROM wsc_mill_statistics_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
        redeem_total_min = getData(await mysql.queryAsync(
            `SELECT IFNULL(redeem_mill_total_min,0) FROM wsc_mill_statistics_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
    }
    //累计赎回大型矿机数量
    let redeem_mill_total_max = todayDataSum.redeem_mill_today_max + totalDataSum.redeem_mill_total_max + redeem_total_max
    totalDataSum.redeem_mill_total_max = 0;
    //累计赎回中型矿机数量
    let redeem_mill_total_middle = todayDataSum.redeem_mill_today_middle + totalDataSum.redeem_mill_total_middle + redeem_total_middle
    totalDataSum.redeem_mill_total_middle = 0;
    //累计赎回小型矿机数量
    let redeem_mill_total_min = todayDataSum.redeem_mill_today_min + totalDataSum.redeem_mill_total_min + redeem_total_min
    totalDataSum.redeem_mill_total_min = 0;

    //获取昨天累计
    var due_total_max = 0;
    var due_total_middle = 0;
    var due_total_min = 0;
    if (startDay != 0) {
        due_total_max = getData(await mysql.queryAsync(
            `SELECT IFNULL(due_toady_mill_max,0) FROM wsc_mill_statistics_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
        due_total_middle = getData(await mysql.queryAsync(
            `SELECT IFNULL(due_toady_mill_middle,0) FROM wsc_mill_statistics_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
        due_total_min = getData(await mysql.queryAsync(
            `SELECT IFNULL(due_toady_mill_min,0) FROM wsc_mill_statistics_summary
             WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
    }
    //累计到期大型矿机数量
    let due_mill_total_max = todayDataSum.due_toady_mill_max + totalDataSum.due_mill_total_max + due_total_max
    totalDataSum.due_mill_total_max = 0;
    //累计到期中型矿机数量
    let due_mill_total_middle = todayDataSum.due_toady_mill_middle + totalDataSum.due_mill_total_middle + due_total_middle
    totalDataSum.due_mill_total_middle = 0;
    // 累计到期小型矿机数量
    let due_mill_total_min = todayDataSum.due_toady_mill_min + totalDataSum.due_mill_total_min + due_total_min
    totalDataSum.due_mill_total_min = 0;
    let sql_wsc_mill_statistics_summary = `insert into wsc_mill_statistics_summary
    (
        added_today_mill_max,
        added_today_mill_middle,
        added_today_mill_min,

        added_mill_total_max,
        added_mill_total_middle,
        added_mill_total_min,

        redeem_mill_today_max,
        redeem_mill_today_middle,
        redeem_mill_today_min,

        redeem_mill_total_max,
        redeem_mill_total_middle,
        redeem_mill_total_min,

        due_toady_mill_max,
        due_toady_mill_middle,
        due_toady_mill_min,

        due_mill_total_max,
        due_mill_total_middle,
        due_mill_total_min ,   
        clear_time
       ) 
    VALUES (
    ${todayDataSum.added_today_mill_max},
    ${todayDataSum.added_today_mill_middle},
    ${todayDataSum.added_today_mill_min},

    ${added_mill_total_max},
    ${added_mill_total_middle},
    ${added_mill_total_min},

    ${todayDataSum.redeem_mill_today_max},
    ${todayDataSum.redeem_mill_today_middle},
    ${todayDataSum.redeem_mill_today_min},

    ${redeem_mill_total_max},
    ${redeem_mill_total_middle},
    ${redeem_mill_total_min},

    ${todayDataSum.due_toady_mill_max},
    ${todayDataSum.due_toady_mill_middle},
    ${todayDataSum.due_toady_mill_min},

    ${due_mill_total_max},
    ${due_mill_total_middle},
    ${due_mill_total_min},
    '${ymd}')`;

    await mysql.queryAsync(sql_wsc_mill_statistics_summary);

    logger.info("totalMillData End ...");
}
//昨天累计USD收益
async function totalImcomeData(ymd, end) {
    logger.info("start totalImcomeData ... ");

    await todayImcomeData(ymd, end);

    //获取昨天累计
    var revenue_sharing_tem = 0;
    var community_benefits_tem = 0;
    var peer_income_tem = 0;
    var node_income_tem = 0;
    var dividend_income_tem = 0;
    var very_poor_community_tem = 0;
    var node_range_tem = 0;
    var supernode_tem = 0;

    //判断第一次运行时有没有前一天数据
    if (startDay != 0) {
        revenue_sharing_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(total_revenue_sharing,0) FROM wsc_income_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY)  and clear_time < '${ymd}' order by clear_time desc limit 1`));
        community_benefits_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(total_community_benefits,0) FROM wsc_income_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
        peer_income_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(total_peer_income,0) FROM wsc_income_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
        node_income_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(total_node_income,0) FROM wsc_income_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
        dividend_income_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(total_dividend_income,0) FROM wsc_income_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
        very_poor_community_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(total_very_poor_community,0) FROM wsc_income_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
        node_range_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(total_node_range,0) FROM wsc_income_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
        supernode_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(total_supernode,0) FROM wsc_income_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
    }
    //累计分享收益
    let total_revenue_sharing = todayDataSum.today_revenue_sharing + totalDataSum.total_revenue_sharing + revenue_sharing_tem;
    totalDataSum.total_revenue_sharing = 0;
    //累计社区收益
    let total_community_benefits = todayDataSum.today_community_benefits + totalDataSum.total_community_benefits + community_benefits_tem;
    totalDataSum.total_community_benefits = 0;
    //累计平级收益
    let total_peer_income = todayDataSum.today_peer_income + totalDataSum.total_peer_income + peer_income_tem;
    totalDataSum.total_peer_income = 0;
    //累计节点收益
    let total_node_income = todayDataSum.today_node_income + totalDataSum.total_node_income + node_income_tem;
    totalDataSum.total_node_income = 0;
    //累计分红收益
    let total_dividend_income = todayDataSum.today_dividend_income + totalDataSum.total_dividend_income + dividend_income_tem;
    totalDataSum.total_dividend_income = 0;
    //累计社区极差
    let total_very_poor_community = todayDataSum.today_very_poor_community + totalDataSum.total_very_poor_community + very_poor_community_tem;
    totalDataSum.total_very_poor_community = 0;
    //累计节点极差
    let total_node_range = todayDataSum.today_node_range + totalDataSum.total_node_range + node_range_tem;
    totalDataSum.total_node_range = 0;
    //累计超级节点
    let total_supernode = todayDataSum.today_supernode + totalDataSum.total_supernode + supernode_tem;
    totalDataSum.total_supernode = 0;

    let sql_wsc_wsc_income_summary = `insert into wsc_income_summary
            (today_revenue_sharing,
                today_community_benefits,
                today_peer_income,
                today_node_income,
                today_dividend_income,
                today_very_poor_community,
                today_node_range,
                today_supernode,

                total_revenue_sharing,
                total_community_benefits,
                total_peer_income,
                total_node_income,

                total_dividend_income,
                total_very_poor_community,
                total_node_range,
                total_supernode,
                clear_time
            )
        VALUES(
        ${todayDataSum.today_revenue_sharing},
        ${todayDataSum.today_community_benefits},
        ${todayDataSum.today_peer_income},
        ${todayDataSum.today_node_income},
        ${todayDataSum.today_dividend_income},
        ${todayDataSum.today_very_poor_community},
        ${todayDataSum.today_node_range},
        ${todayDataSum.today_supernode},

        ${total_revenue_sharing},
        ${total_community_benefits},
        ${total_peer_income},
        ${total_node_income},

        ${total_dividend_income},
        ${total_very_poor_community},
        ${total_node_range},
        ${total_supernode},
        '${ymd}')`;

    await mysql.queryAsync(sql_wsc_wsc_income_summary);
    logger.info("totalImcomeData End ...");
}
//昨天累计会员，转账，ETH，WSC
async function totalStatData(ymd, end) {
    logger.info("start totalStatData ... ");
    await todayStatData(ymd, end);

    //获取昨天累计
    var eth_usd_total_eth_tem = 0;
    var eth_usd_total_usd_tem = 0;

    var wsc_usd_total_wsc_tem = 0;
    var wsc_usd_total_usd_tem = 0;

    var wsc_eth_total_wsc_tem = 0;
    var wsc_eth_total_eth_tem = 0;

    var usd_eth_total_usd_tem = 0;
    var usd_eth_total_eth_tem = 0;

    if (startDay != 0) {
        eth_usd_total_eth_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(eth_usd_total_eth,0) FROM wsc_statistics_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
        eth_usd_total_usd_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(eth_usd_total_usd,0) FROM wsc_statistics_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));

        wsc_usd_total_wsc_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(wsc_usd_total_wsc,0) FROM wsc_statistics_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
        wsc_usd_total_usd_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(wsc_usd_total_usd,0) FROM wsc_statistics_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));

        wsc_eth_total_wsc_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(wsc_eth_total_wsc,0) FROM wsc_statistics_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
        wsc_eth_total_eth_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(wsc_eth_total_eth,0) FROM wsc_statistics_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));

        usd_eth_total_usd_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(usd_eth_total_usd,0) FROM wsc_statistics_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
        usd_eth_total_eth_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(usd_eth_total_eth,0) FROM wsc_statistics_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
    }
    //累计ETH兑换USD兑出ETH
    let eth_usd_total_eth = todayDataSum.eth_usd_today_eth + totalDataSum.eth_usd_total_eth + eth_usd_total_eth_tem;
    totalDataSum.eth_usd_total_eth = 0;
    //累计ETH兑换USD兑入USD
    let eth_usd_total_usd = todayDataSum.eth_usd_today_usd + totalDataSum.eth_usd_total_usd + eth_usd_total_usd_tem;
    totalDataSum.eth_usd_total_usd = 0;
    //累计WSC兑换USD兑出WSC
    let wsc_usd_total_wsc = todayDataSum.wsc_usd_today_wsc + totalDataSum.wsc_usd_total_wsc + wsc_usd_total_wsc_tem;
    totalDataSum.wsc_usd_total_wsc = 0;
    //累计WSC兑换USD兑入USD
    let wsc_usd_total_usd = todayDataSum.wsc_usd_today_usd + totalDataSum.wsc_usd_total_usd + wsc_usd_total_usd_tem;
    totalDataSum.wsc_usd_total_usd = 0;
    //累计WSC兑换ETH兑出WSC
    let wsc_eth_total_wsc = todayDataSum.wsc_eth_today_wsc + totalDataSum.wsc_eth_total_wsc + wsc_eth_total_wsc_tem;
    totalDataSum.wsc_eth_total_wsc = 0;
    //累计WSC兑换ETH兑入ETH
    let wsc_eth_total_eth = todayDataSum.wsc_eth_today_eth + totalDataSum.wsc_eth_total_eth + wsc_eth_total_eth_tem;
    totalDataSum.wsc_eth_total_eth = 0;
    //累计USD兑换ETH兑出USD
    let usd_eth_total_usd = todayDataSum.usd_eth_today_usd + totalDataSum.usd_eth_total_usd + usd_eth_total_usd_tem;
    totalDataSum.usd_eth_total_usd = 0;
    //累计USD兑换ETH兑入ETH
    let usd_eth_total_eth = todayDataSum.usd_eth_today_eth + totalDataSum.usd_eth_total_eth + usd_eth_total_eth_tem
    totalDataSum.usd_eth_total_eth = 0;


    var recharge_eth_tem = 0;
    var withdraw_eth_tem = 0;
    if (startDay != 0) {
        recharge_eth_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(recharge_eth_total,0) FROM wsc_statistics_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
        withdraw_eth_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(withdraw_eth_total,0) FROM wsc_statistics_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
    }

    //累计充值ETH数量
    let recharge_eth_total = todayDataSum.recharge_eth_today + totalDataSum.recharge_eth_total + recharge_eth_tem;
    totalDataSum.recharge_eth_total = 0;
    //累计提现ETH数量
    let withdraw_eth_total = todayDataSum.withdraw_eth_today + totalDataSum.withdraw_eth_total + withdraw_eth_tem;
    totalDataSum.withdraw_eth_total = 0;

    var total_mining_income_tem = 0;
    var total_destroy_tem = 0;

    if (startDay != 0) {
        total_mining_income_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(total_mining_income,0) FROM wsc_statistics_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
        total_destroy_tem = getData(await mysql.queryAsync(
            `SELECT IFNULL(total_destroy,0) FROM wsc_statistics_summary 
            WHERE  clear_time >= DATE_SUB('${ymd}',INTERVAL ${yester} DAY) and clear_time < '${ymd}' order by clear_time desc limit 1`));
    }

    //累计挖矿收益wsc
    let total_mining_income = todayDataSum.today_mining_income + totalDataSum.total_mining_income + total_mining_income_tem;
    totalDataSum.total_mining_income = 0;
    //累计销毁wsc
    let total_destroy = todayDataSum.today_destroy + totalDataSum.total_destroy + total_destroy_tem;
    totalDataSum.total_destroy = 0;

    //V0
    todayDataSum.v0 = getData(await mysql.queryAsync(
        `SELECT IFNULL(count(*),0) 
    from (
        select fId,u.fLoginName,IFNULL(count(*),0),m.status 
        from fuser u inner JOIN my_mill m on u.fLoginName = m.fLoginName 
        where m.status!=0 and u.fRegisterTime < '${ymd}' GROUP BY u.fId 
        ) 
        a INNER JOIN fvirtualwallet_swc_result r on a.fId=r.fuserId 
        where r.vip_level = 0`));
    //V1
    todayDataSum.v1 = getData(await mysql.queryAsync(
        `SELECT IFNULL(count(*),0) 
        from (
            select fId,u.fLoginName,IFNULL(count(*),0),m.status 
            from fuser u inner JOIN my_mill m on u.fLoginName = m.fLoginName 
            where m.status!=0 and u.fRegisterTime < '${ymd}' GROUP BY u.fId 
            ) 
            a INNER JOIN fvirtualwallet_swc_result r on a.fId=r.fuserId 
            where r.vip_level = 1`));
    //V2
    todayDataSum.v2 = getData(await mysql.queryAsync(
        `SELECT IFNULL(count(*),0) 
        from (
            select fId,u.fLoginName,IFNULL(count(*),0),m.status 
            from fuser u inner JOIN my_mill m on u.fLoginName = m.fLoginName 
            where m.status!=0 and u.fRegisterTime < '${ymd}' GROUP BY u.fId 
            ) 
            a INNER JOIN fvirtualwallet_swc_result r on a.fId=r.fuserId 
            where r.vip_level = 2`));
    //V3
    todayDataSum.v3 = getData(await mysql.queryAsync(
        `SELECT IFNULL(count(*),0) 
        from (
            select fId,u.fLoginName,IFNULL(count(*),0),m.status 
            from fuser u inner JOIN my_mill m on u.fLoginName = m.fLoginName 
            where m.status!=0 and u.fRegisterTime < '${ymd}' GROUP BY u.fId 
            ) 
            a INNER JOIN fvirtualwallet_swc_result r on a.fId=r.fuserId 
            where r.vip_level = 3`));
    //V4
    todayDataSum.v4 = getData(await mysql.queryAsync(
        `SELECT IFNULL(count(*),0) 
        from (
            select fId,u.fLoginName,IFNULL(count(*),0),m.status 
            from fuser u inner JOIN my_mill m on u.fLoginName = m.fLoginName 
            where m.status!=0 and u.fRegisterTime < '${ymd}' GROUP BY u.fId 
            ) 
            a INNER JOIN fvirtualwallet_swc_result r on a.fId=r.fuserId 
            where r.vip_level = 4`));
    //V5
    todayDataSum.v5 = getData(await mysql.queryAsync(
        `SELECT IFNULL(count(*),0) 
            from (
                select fId,u.fLoginName,IFNULL(count(*),0),m.status 
                from fuser u inner JOIN my_mill m on u.fLoginName = m.fLoginName 
                where m.status!=0 and u.fRegisterTime < '${ymd}' GROUP BY u.fId 
                ) 
                a INNER JOIN fvirtualwallet_swc_result r on a.fId=r.fuserId 
                where r.vip_level = 5`));

    let sql_wsc_statistics_summary = `insert into wsc_statistics_summary
            (

                today_new_user,
                total_user,
                added_today_user,
                all_valid_user,

                eth_usd_today_eth,
                eth_usd_today_usd,
                wsc_usd_today_wsc,
                wsc_usd_today_usd,
                wsc_eth_today_wsc,
                wsc_eth_today_eth,
                usd_eth_today_usd,
                usd_eth_today_eth,

                eth_usd_total_eth,
                eth_usd_total_usd,
                wsc_usd_total_wsc,
                wsc_usd_total_usd,
                wsc_eth_total_wsc,
                wsc_eth_total_eth,
                usd_eth_total_usd,
                usd_eth_total_eth,

                 v0 ,
                 v1 ,
                 v2 ,
                 v3 ,
                 v4 ,
                 v5 ,


     
                recharge_eth_today,
                withdraw_eth_today,
                recharge_eth_total,
                withdraw_eth_total,

                today_mining_income,
                today_destroy,
                total_mining_income,
                total_destroy,
                clear_time
            )
        VALUES(

            ${todayDataSum.today_new_user},
            ${todayDataSum.total_user},
            ${todayDataSum.added_today_user},
            ${todayDataSum.all_valid_user},

            ${todayDataSum.eth_usd_today_eth},
            ${todayDataSum.eth_usd_today_usd},
            ${todayDataSum.wsc_usd_today_wsc},
            ${todayDataSum.wsc_usd_today_usd},
            ${todayDataSum.wsc_eth_today_wsc},
            ${todayDataSum.wsc_eth_today_eth},
            ${todayDataSum.usd_eth_today_usd},
            ${todayDataSum.usd_eth_today_eth},

            ${eth_usd_total_eth},
            ${eth_usd_total_usd},
            ${wsc_usd_total_wsc},
            ${wsc_usd_total_usd},
            ${wsc_eth_total_wsc},
            ${wsc_eth_total_eth},
            ${usd_eth_total_usd},
            ${usd_eth_total_eth},

            ${todayDataSum.v0},
            ${todayDataSum.v1},
            ${todayDataSum.v2},
            ${todayDataSum.v3},
            ${todayDataSum.v4},
            ${todayDataSum.v5},



            ${todayDataSum.recharge_eth_today},
            ${todayDataSum.withdraw_eth_today},
            ${recharge_eth_total},
            ${withdraw_eth_total},

            ${todayDataSum.today_mining_income},
            ${todayDataSum.today_destroy},
            ${total_mining_income},
            ${total_destroy},
            '${ymd}')`;
    await mysql.queryAsync(sql_wsc_statistics_summary);
    logger.info("totalStatData End ...");
}

var todayDataSum = {

    "today_new_user": 0,
    "total_user": 0,
    "added_today_user": 0,
    "all_valid_user": 0,

    "added_today_mill_max": 0,
    "added_today_mill_middle": 0,
    "added_today_mill_min": 0,

    "redeem_mill_today_max": 0,
    "redeem_mill_today_middle": 0,
    "redeem_mill_today_min": 0,

    "due_toady_mill_max": 0,
    "due_toady_mill_middle": 0,
    "due_toady_mill_min": 0,


    "today_revenue_sharing": 0,
    "today_community_benefits": 0,
    "today_peer_income": 0,
    "today_node_income": 0,
    "today_dividend_income": 0,
    "today_very_poor_community": 0,
    "today_node_range": 0,
    "today_supernode": 0,

    "v0": 0,
    "v1": 0,
    "v2": 0,
    "v3": 0,
    "v4": 0,
    "v5": 0,


    "today_dividend_income": 0,
    "today_very_poor_community": 0,
    "today_node_range": 0,
    "today_supernode": 0,

    "eth_usd_today_eth": 0,
    "eth_usd_today_usd": 0,

    "wsc_usd_today_wsc": 0,
    "wsc_usd_today_usd": 0,

    "wsc_eth_today_wsc": 0,
    "wsc_eth_today_eth": 0,

    "usd_eth_today_usd": 0,
    "usd_eth_today_eth": 0,

    "recharge_eth_today": 0,
    "withdraw_eth_today": 0,
    "today_mining_income": 0,
    "today_destroy": 0
}

//指定日期之前sum
var totalDataSum = {

    "total_revenue_sharing": 0,
    "total_community_benefits": 0,
    "total_peer_income": 0,
    "total_node_income": 0,
    "total_dividend_income": 0,
    "total_very_poor_community": 0,
    "total_node_range": 0,
    "total_supernode": 0,
    "added_mill_total_max": 0,
    "added_mill_total_middle": 0,
    "added_mill_total_min": 0,
    "redeem_mill_total_max": 0,
    "redeem_mill_total_middle": 0,
    "redeem_mill_total_min": 0,
    "due_mill_total_max": 0,
    "due_mill_total_middle": 0,
    "due_mill_total_min": 0,
    "eth_usd_total_eth": 0,
    "eth_usd_total_usd": 0,
    "wsc_usd_total_wsc": 0,
    "wsc_usd_total_usd": 0,
    "wsc_eth_total_wsc": 0,
    "wsc_eth_total_eth": 0,
    "usd_eth_total_usd": 0,
    "usd_eth_total_eth": 0,
    "recharge_eth_total": 0,
    "withdraw_eth_total": 0,
    "total_mining_income": 0,
    "total_destroy": 0
}
async function dataSum(ymd, end) {
    logger.info("start dataSum ...");

    //累计分享收益
    totalDataSum.total_revenue_sharing = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
        where in_type = 1 and is_cal = 0 and creat_time < '${ymd}'`));

    //累计社区收益
    totalDataSum.total_community_benefits = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
        where in_type = 2 and is_cal = 0 and creat_time < '${ymd}'`));

    //累计平级收益
    totalDataSum.total_peer_income = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records
        where in_type = 2 and is_cal = 2 and creat_time < '${ymd}'`));

    //累计节点收益
    totalDataSum.total_node_income = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
        where in_type = 5 and is_cal = 0 and creat_time < '${ymd}'`));

    //累计分红收益
    totalDataSum.total_dividend_income = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
        where in_type = 3 and is_cal = 0 and creat_time < '${ymd}'`));

    //累计社区极差
    totalDataSum.total_very_poor_community = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
        where in_type = 2 and is_cal = 1 and creat_time < '${ymd}'`));

    //累计节点极差
    totalDataSum.total_node_range = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
        where in_type = 5 and is_cal = 1 and creat_time < '${ymd}'`));

    //累计超级节点
    totalDataSum.total_supernode = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
        where in_type = 4 and is_cal = 0 and creat_time < '${ymd}'`));


    //累计新增大型矿机
    totalDataSum.added_mill_total_max = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from mill_order  
        where mill_code = '大型矿机' and pay_status=1  and create_time < '${ymd}'`));

    //累计新增中型矿机
    totalDataSum.added_mill_total_middle = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from mill_order  
        where mill_code = '中型矿机' and pay_status=1  and create_time < '${ymd}'`));

    //累计新增小型矿机
    totalDataSum.added_mill_total_min = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from mill_order  
        where mill_code = '小型矿机' and pay_status=1  and create_time < '${ymd}'`));

    //累计赎回大型矿机数量
    totalDataSum.redeem_mill_total_max = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from mill_recycle_order  
        where money = 5000 and status =1  and create_time < '${ymd}'`));

    //累计赎回中型矿机数量
    totalDataSum.redeem_mill_total_middle = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from mill_recycle_order  
        where money = 1000 and status =1  and create_time < '${ymd}'`));

    //累计赎回小型矿机数量
    totalDataSum.redeem_mill_total_min = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from mill_recycle_order  
        where money = 500 and status =1  and create_time < '${ymd}'`));


    //累计到期大型矿机数量
    totalDataSum.due_mill_total_max = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from my_mill  
        where money  = '5000' and status =0 and end_time >='${ymd}' and create_time >='${ymd}' and create_time < '${end}'`));

    //累计到期中型矿机数量
    totalDataSum.due_mill_total_middle = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from my_mill  
        where money = '1000'and status =0 and end_time >='${ymd}' and create_time >='${ymd}' and create_time <'${end}' `));

    // 累计到期小型矿机数量
    totalDataSum.due_mill_total_min = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from my_mill  
        where money = '500' and status =0 and end_time >='${ymd}' and create_time >='${ymd}' and create_time < '${end}'`));


    //累计ETH兑换USD兑出ETH
    totalDataSum.eth_usd_total_eth = getData(await mysql.queryAsync(
        `select IFNULL(sum(outerAmout),0) from c2c_order  
            where outerCurrencyId = 4 and innerCurrencyId = 6 and orderStatus = 2  and createDate < '${ymd}'`));

    //累计ETH兑换USD兑入USD
    totalDataSum.eth_usd_total_usd = getData(await mysql.queryAsync(
        `select IFNULL(sum(innerAmout),0) from c2c_order  
        where outerCurrencyId = 4 and innerCurrencyId = 6 and orderStatus = 2  and createDate < '${ymd}'`));

    //累计WSC兑换USD兑出WSC
    totalDataSum.wsc_usd_total_wsc = getData(await mysql.queryAsync(
        `select IFNULL(sum(outerAmout),0) from c2c_order  
        where outerCurrencyId = 58 and innerCurrencyId = 6 and orderStatus = 2  and createDate < '${ymd}'`));


    //累计WSC兑换USD 兑入USD
    totalDataSum.wsc_usd_total_usd = getData(await mysql.queryAsync(
        `select IFNULL(sum(innerAmout),0)  from c2c_order  
        where outerCurrencyId = 58 and innerCurrencyId = 6 and orderStatus = 2  and createDate < '${ymd}'`));


    //累计WSC兑换ETH兑出WSC
    totalDataSum.wsc_eth_total_wsc = getData(await mysql.queryAsync(
        `select IFNULL(sum(outerAmout),0) from c2c_order  
        where outerCurrencyId = 58 and innerCurrencyId = 4 and orderStatus = 2  and createDate < '${ymd}'`));


    //累计WSC兑换ETH兑入ETH
    totalDataSum.wsc_eth_total_eth = getData(await mysql.queryAsync(
        `select IFNULL(sum(innerAmout),0)  from c2c_order  
        where outerCurrencyId = 58 and innerCurrencyId = 4 and orderStatus = 2  and createDate < '${ymd}'`));


    //累计USD兑换ETH兑出USD
    totalDataSum.usd_eth_total_usd = getData(await mysql.queryAsync(
        `select IFNULL(sum(outerAmout),0) from c2c_order  
        where outerCurrencyId = 6 and innerCurrencyId = 4 and orderStatus = 2  and createDate < '${ymd}'`));


    //累计USD兑换ETH兑入ETH
    totalDataSum.usd_eth_total_eth = getData(await mysql.queryAsync(
        `select IFNULL(sum(innerAmout),0) from c2c_order  
        where outerCurrencyId = 6 and innerCurrencyId = 4 and orderStatus = 2  and createDate < '${ymd}'`));



    //累计充值ETH数量
    totalDataSum.recharge_eth_total = getData(await mysql.queryAsync(
        `select IFNULL(sum(fAmount),0) from fvirtualcaptualoperation 
        where fVi_fId2 = 4 and fType = 1 and fStatus = 3 and fCreateTime < '${ymd}'`));


    //累计提现ETH数量
    totalDataSum.withdraw_eth_total = getData(await mysql.queryAsync(
        `select IFNULL(sum(fAmount),0) from fvirtualcaptualoperation 
        where fVi_fId2 = 4 and fType = 2 and fStatus = 3 and fCreateTime < '${ymd}'`));


    //累计挖矿收益wsc
    totalDataSum.total_mining_income = getData(await mysql.queryAsync(
        `select IFNULL(sum(rec_num),0) from fvirtualwallet_swc_records  
        where in_type = 0 and creat_time < '${ymd}'`));

    //累计销毁wsc
    totalDataSum.total_destroy = getData(await mysql.queryAsync(
        `select IFNULL(sum(gratuity),0) from c2c_order  
        where createDate < '${ymd}'`));

    logger.info("todayStatData end ...");

}
startService();
