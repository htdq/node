/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : localhost:3306
 Source Schema         : futuresdb

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 13/04/2020 00:47:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for wsc_assets_data
-- ----------------------------
DROP TABLE IF EXISTS `wsc_assets_data`;
CREATE TABLE `wsc_assets_data`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `total_eth` decimal(20, 8) NOT NULL DEFAULT 0.00000000 COMMENT '全站ETH数量',
  `total_usd` decimal(20, 8) NOT NULL DEFAULT 0.00000000 COMMENT '全站USD数量',
  `total_wsc` decimal(20, 8) NOT NULL DEFAULT 0.00000000 COMMENT '全站WSC数量',
  `eth_incr` decimal(20, 8) NOT NULL DEFAULT 0.00000000 COMMENT 'ETH增长',
  `usd_incr` decimal(20, 8) NOT NULL DEFAULT 0.00000000 COMMENT 'USD增长',
  `wsc_incr` decimal(20, 8) NOT NULL DEFAULT 0.00000000 COMMENT 'WSC增长',
  `clear_time` date NULL DEFAULT NULL COMMENT '结算时间',
  `create_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
