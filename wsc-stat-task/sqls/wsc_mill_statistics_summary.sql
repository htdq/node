/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : localhost:3306
 Source Schema         : futuresdb

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 24/03/2020 14:08:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for wsc_mill_statistics_summary
-- ----------------------------
DROP TABLE IF EXISTS `wsc_mill_statistics_summary`;
CREATE TABLE `wsc_mill_statistics_summary`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `added_today_mill_max` int(11) NOT NULL DEFAULT 0 COMMENT '今日新增大型矿机数量',
  `added_today_mill_middle` int(11) NOT NULL DEFAULT 0 COMMENT '今日新增中型矿机数量',
  `added_today_mill_min` int(11) NOT NULL DEFAULT 0 COMMENT '今日新增小型矿机数量',
  `redeem_mill_today_max` int(11) NOT NULL DEFAULT 0 COMMENT '今日赎回大型矿机数量',
  `redeem_mill_today_middle` int(11) NOT NULL DEFAULT 0 COMMENT '今日赎回中型矿机数量',
  `redeem_mill_today_min` int(11) NOT NULL DEFAULT 0 COMMENT '今日赎回小型矿机数量',
  `due_toady_mill_max` int(11) NOT NULL DEFAULT 0 COMMENT '今日到期大型矿机数量',
  `due_toady_mill_middle` int(11) NOT NULL DEFAULT 0 COMMENT '今日到期中型矿机数量',
  `due_toady_mill_min` int(11) NOT NULL DEFAULT 0 COMMENT '今日到期小型矿机数量',
  `added_mill_total_max` int(11) NOT NULL DEFAULT 0 COMMENT '累计新增大型矿机数量',
  `added_mill_total_middle` int(11) NOT NULL DEFAULT 0 COMMENT '累计新增中型矿机数量',
  `added_mill_total_min` int(11) NOT NULL DEFAULT 0 COMMENT '累计新增小型矿机数量',
  `redeem_mill_total_max` int(11) NOT NULL DEFAULT 0 COMMENT '累计赎回大型矿机数量',
  `redeem_mill_total_middle` int(11) NOT NULL DEFAULT 0 COMMENT '累计赎回中型矿机数量',
  `redeem_mill_total_min` int(11) NOT NULL DEFAULT 0 COMMENT '累计赎回小型矿机数量',
  `due_mill_total_max` int(11) NOT NULL DEFAULT 0 COMMENT '累计到期大型矿机数量',
  `due_mill_total_middle` int(11) NOT NULL DEFAULT 0 COMMENT '累计到期中型矿机数量',
  `due_mill_total_min` int(11) NOT NULL DEFAULT 0 COMMENT '累计到期小型矿机数量',
  `clear_time` date NULL DEFAULT NULL COMMENT '结算时间',
  `create_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2137 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wsc_mill_statistics_summary
-- ----------------------------
INSERT INTO `wsc_mill_statistics_summary` VALUES (2113, 1, 0, 0, 0, 0, 0, 0, 0, 0, 365, 1897, 303, 3, 233, 28, 0, 0, 0, '2020-03-01', '2020-03-24 14:44:19');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2114, 0, 3, 1, 0, 0, 0, 0, 0, 0, 365, 1900, 304, 3, 233, 28, 0, 0, 0, '2020-03-02', '2020-03-24 14:44:20');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2115, 5, 11, 0, 0, 0, 0, 0, 0, 0, 370, 1911, 304, 3, 233, 28, 0, 0, 0, '2020-03-03', '2020-03-24 14:44:20');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2116, 2, 0, 0, 0, 1, 0, 0, 0, 0, 372, 1911, 304, 3, 234, 28, 0, 0, 0, '2020-03-04', '2020-03-24 14:44:21');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2117, 2, 17, 0, 0, 0, 0, 0, 0, 0, 374, 1928, 304, 3, 234, 28, 0, 0, 0, '2020-03-05', '2020-03-24 14:44:21');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2118, 3, 19, 2, 0, 1, 2, 0, 0, 0, 377, 1947, 306, 3, 235, 30, 0, 0, 0, '2020-03-06', '2020-03-24 14:44:22');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2119, 2, 2, 4, 0, 1, 3, 0, 0, 0, 379, 1949, 310, 3, 236, 33, 0, 0, 0, '2020-03-07', '2020-03-24 14:44:22');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2120, 5, 5, 1, 0, 0, 0, 0, 0, 0, 384, 1954, 311, 3, 236, 33, 0, 0, 0, '2020-03-08', '2020-03-24 14:44:23');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2121, 4, 1, 0, 0, 0, 0, 0, 0, 0, 388, 1955, 311, 3, 236, 33, 0, 0, 0, '2020-03-09', '2020-03-24 14:44:23');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2122, 1, 8, 0, 0, 2, 0, 0, 0, 0, 389, 1963, 311, 3, 238, 33, 0, 0, 0, '2020-03-10', '2020-03-24 14:44:24');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2123, 5, 2, 0, 0, 0, 0, 0, 0, 0, 394, 1965, 311, 3, 238, 33, 0, 0, 0, '2020-03-11', '2020-03-24 14:44:24');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2124, 5, 4, 1, 0, 0, 0, 0, 0, 0, 399, 1969, 312, 3, 238, 33, 0, 0, 0, '2020-03-12', '2020-03-24 14:44:24');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2125, 4, 4, 2, 0, 3, 1, 0, 0, 0, 403, 1973, 314, 3, 241, 34, 0, 0, 0, '2020-03-13', '2020-03-24 14:44:25');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2126, 7, 9, 2, 0, 0, 0, 0, 0, 0, 410, 1982, 316, 3, 241, 34, 0, 0, 0, '2020-03-14', '2020-03-24 14:44:25');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2127, 0, 0, 3, 0, 0, 0, 0, 0, 0, 410, 1982, 319, 3, 241, 34, 0, 0, 0, '2020-03-15', '2020-03-24 14:44:26');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2128, 5, 2, 5, 0, 0, 1, 0, 0, 0, 415, 1984, 324, 3, 241, 35, 0, 0, 0, '2020-03-16', '2020-03-24 14:44:26');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2129, 3, 4, 1, 0, 1, 2, 0, 0, 0, 418, 1988, 325, 3, 242, 37, 0, 0, 0, '2020-03-17', '2020-03-24 14:44:27');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2130, 0, 6, 0, 0, 0, 0, 0, 0, 0, 418, 1994, 325, 3, 242, 37, 0, 0, 0, '2020-03-18', '2020-03-24 14:44:27');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2131, 11, 2, 1, 0, 0, 0, 0, 0, 0, 429, 1996, 326, 3, 242, 37, 0, 0, 0, '2020-03-19', '2020-03-24 14:44:28');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2132, 19, 13, 6, 0, 0, 0, 0, 0, 0, 448, 2009, 332, 3, 242, 37, 0, 0, 0, '2020-03-20', '2020-03-24 14:44:28');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2133, 4, 11, 3, 1, 0, 0, 0, 0, 0, 452, 2020, 335, 4, 242, 37, 0, 0, 0, '2020-03-21', '2020-03-24 14:44:29');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2134, 3, 11, 3, 0, 0, 1, 0, 0, 0, 455, 2031, 338, 4, 242, 38, 0, 0, 0, '2020-03-22', '2020-03-24 14:44:29');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2135, 10, 6, 4, 0, 0, 6, 0, 0, 0, 465, 2037, 342, 4, 242, 44, 0, 0, 0, '2020-03-23', '2020-03-24 14:44:30');
INSERT INTO `wsc_mill_statistics_summary` VALUES (2136, 0, 0, 1, 0, 0, 0, 0, 0, 0, 465, 2037, 343, 4, 242, 44, 0, 0, 0, '2020-03-24', '2020-03-24 14:00:27');

SET FOREIGN_KEY_CHECKS = 1;
