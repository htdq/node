
#### 获取短信验证码
- GET /api/user/phone/getsmscode
##### 参数

| 参数名称 | 参数类型 | 必填 |  描述 | 
|  ----  | ----  | ----  | ----  |
| phone  | string  | true	| 调用getcaptcha时输入的验证码|
| captcha| string  | true	| 调用getcaptcha时返回的验证码|

##### 返回值

| 参数名称 | 是否必须 | 数据类型 | 描述 | 取值范围 |
|  ----  | ----  | ----  | ----  | ----  |
| code	 | true  | int	    |返回码	        |2xx - 5xx - 10xx |
| msg	 | true	 | string	|请求处理结果   |"success" , "failed"|
| data	 | true	 | bool	    |是否成功处理   |

##### 示例
```JSON
{
    "code": 200,
    "msg": "success",
    "data": true
}
```

#### 用户注册
- POST /api/user/register
##### 参数

| 参数名称 | 参数类型 | 必填 |  描述 | 
|  ----  | ----  | ----  | ----  |
| username  | string  | true	| 用户名|
| phone  | string  | true	| 电话号码|
| password| string  | true	| 密码|
| rePassword| string  | true  | 确认密码|
| smsCode| string  | true	| 手机验证码|
| inviteCode| string  | false	| 邀请码|

##### 返回值

| 参数名称 | 是否必须 | 数据类型 | 描述 | 取值范围 |
|  ----  | ----  | ----  | ----  | ----  |
| code	 | true  | int	    |返回码	        |2xx - 5xx - 10xx |
| msg	 | true	 | string	|请求处理结果   |"success" , "failed"|
| data	 | true	 | bool	    |是否成功处理   |

##### 示例
```JSON
{
    "code": 200,
    "msg": "success",
    "data": true
}
```








