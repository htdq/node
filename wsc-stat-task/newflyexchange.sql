/*
 Navicat Premium Data Transfer

 Source Server         : localhsot
 Source Server Type    : MySQL
 Source Server Version : 50729
 Source Host           : localhost:3306
 Source Schema         : newflyexchange

 Target Server Type    : MySQL
 Target Server Version : 50729
 File Encoding         : 65001

 Date: 17/04/2020 18:39:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for balance_history_0
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_0`;
CREATE TABLE `balance_history_0`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_0
-- ----------------------------
INSERT INTO `balance_history_0` VALUES (1, 1, '32', '32', 32, 32.0000000000000000, 32.0000000000000000, '32', 1.000000);
INSERT INTO `balance_history_0` VALUES (2, 3, '2', '32', 32, 32.0000000000000000, 32.0000000000000000, '32', 32.000000);

-- ----------------------------
-- Table structure for balance_history_1
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_1`;
CREATE TABLE `balance_history_1`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_1
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_10
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_10`;
CREATE TABLE `balance_history_10`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_10
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_11
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_11`;
CREATE TABLE `balance_history_11`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_11
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_12
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_12`;
CREATE TABLE `balance_history_12`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_12
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_13
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_13`;
CREATE TABLE `balance_history_13`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_13
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_14
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_14`;
CREATE TABLE `balance_history_14`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_14
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_15
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_15`;
CREATE TABLE `balance_history_15`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_15
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_16
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_16`;
CREATE TABLE `balance_history_16`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_16
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_17
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_17`;
CREATE TABLE `balance_history_17`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_17
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_18
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_18`;
CREATE TABLE `balance_history_18`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_18
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_19
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_19`;
CREATE TABLE `balance_history_19`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_19
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_2
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_2`;
CREATE TABLE `balance_history_2`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_2
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_20
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_20`;
CREATE TABLE `balance_history_20`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_20
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_21
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_21`;
CREATE TABLE `balance_history_21`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_21
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_22
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_22`;
CREATE TABLE `balance_history_22`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_22
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_23
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_23`;
CREATE TABLE `balance_history_23`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_23
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_24
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_24`;
CREATE TABLE `balance_history_24`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_24
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_25
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_25`;
CREATE TABLE `balance_history_25`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_25
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_26
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_26`;
CREATE TABLE `balance_history_26`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_26
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_27
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_27`;
CREATE TABLE `balance_history_27`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_27
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_28
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_28`;
CREATE TABLE `balance_history_28`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_28
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_29
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_29`;
CREATE TABLE `balance_history_29`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_29
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_3
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_3`;
CREATE TABLE `balance_history_3`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_3
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_30
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_30`;
CREATE TABLE `balance_history_30`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_30
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_31
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_31`;
CREATE TABLE `balance_history_31`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_31
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_32
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_32`;
CREATE TABLE `balance_history_32`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_32
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_33
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_33`;
CREATE TABLE `balance_history_33`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_33
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_34
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_34`;
CREATE TABLE `balance_history_34`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_34
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_35
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_35`;
CREATE TABLE `balance_history_35`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_35
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_36
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_36`;
CREATE TABLE `balance_history_36`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_36
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_37
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_37`;
CREATE TABLE `balance_history_37`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_37
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_38
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_38`;
CREATE TABLE `balance_history_38`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_38
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_39
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_39`;
CREATE TABLE `balance_history_39`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_39
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_4
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_4`;
CREATE TABLE `balance_history_4`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_4
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_40
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_40`;
CREATE TABLE `balance_history_40`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_40
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_41
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_41`;
CREATE TABLE `balance_history_41`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_41
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_42
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_42`;
CREATE TABLE `balance_history_42`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_42
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_43
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_43`;
CREATE TABLE `balance_history_43`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_43
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_44
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_44`;
CREATE TABLE `balance_history_44`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_44
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_45
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_45`;
CREATE TABLE `balance_history_45`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_45
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_46
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_46`;
CREATE TABLE `balance_history_46`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_46
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_47
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_47`;
CREATE TABLE `balance_history_47`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_47
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_48
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_48`;
CREATE TABLE `balance_history_48`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_48
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_49
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_49`;
CREATE TABLE `balance_history_49`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_49
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_5
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_5`;
CREATE TABLE `balance_history_5`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_5
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_50
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_50`;
CREATE TABLE `balance_history_50`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_50
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_51
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_51`;
CREATE TABLE `balance_history_51`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_51
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_52
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_52`;
CREATE TABLE `balance_history_52`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_52
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_53
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_53`;
CREATE TABLE `balance_history_53`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_53
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_54
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_54`;
CREATE TABLE `balance_history_54`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_54
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_55
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_55`;
CREATE TABLE `balance_history_55`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_55
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_56
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_56`;
CREATE TABLE `balance_history_56`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_56
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_57
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_57`;
CREATE TABLE `balance_history_57`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_57
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_58
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_58`;
CREATE TABLE `balance_history_58`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_58
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_59
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_59`;
CREATE TABLE `balance_history_59`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_59
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_6
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_6`;
CREATE TABLE `balance_history_6`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_6
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_60
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_60`;
CREATE TABLE `balance_history_60`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_60
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_61
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_61`;
CREATE TABLE `balance_history_61`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_61
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_62
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_62`;
CREATE TABLE `balance_history_62`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_62
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_63
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_63`;
CREATE TABLE `balance_history_63`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_63
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_64
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_64`;
CREATE TABLE `balance_history_64`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_64
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_65
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_65`;
CREATE TABLE `balance_history_65`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_65
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_66
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_66`;
CREATE TABLE `balance_history_66`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_66
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_67
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_67`;
CREATE TABLE `balance_history_67`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_67
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_68
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_68`;
CREATE TABLE `balance_history_68`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_68
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_69
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_69`;
CREATE TABLE `balance_history_69`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_69
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_7
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_7`;
CREATE TABLE `balance_history_7`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_7
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_70
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_70`;
CREATE TABLE `balance_history_70`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_70
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_71
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_71`;
CREATE TABLE `balance_history_71`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_71
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_72
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_72`;
CREATE TABLE `balance_history_72`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_72
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_73
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_73`;
CREATE TABLE `balance_history_73`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_73
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_74
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_74`;
CREATE TABLE `balance_history_74`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_74
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_75
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_75`;
CREATE TABLE `balance_history_75`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_75
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_76
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_76`;
CREATE TABLE `balance_history_76`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_76
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_77
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_77`;
CREATE TABLE `balance_history_77`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_77
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_78
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_78`;
CREATE TABLE `balance_history_78`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_78
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_79
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_79`;
CREATE TABLE `balance_history_79`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_79
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_8
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_8`;
CREATE TABLE `balance_history_8`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_8
-- ----------------------------
INSERT INTO `balance_history_8` VALUES (1, 2, '32', '2', 2, 21.0000000000000000, 1.0000000000000000, '12', 32.000000);

-- ----------------------------
-- Table structure for balance_history_80
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_80`;
CREATE TABLE `balance_history_80`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_80
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_81
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_81`;
CREATE TABLE `balance_history_81`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_81
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_82
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_82`;
CREATE TABLE `balance_history_82`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_82
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_83
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_83`;
CREATE TABLE `balance_history_83`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_83
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_84
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_84`;
CREATE TABLE `balance_history_84`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_84
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_85
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_85`;
CREATE TABLE `balance_history_85`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_85
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_86
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_86`;
CREATE TABLE `balance_history_86`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_86
-- ----------------------------
INSERT INTO `balance_history_86` VALUES (1, 23, '23', '32', 323, 232.0000000000000000, 32.0000000000000000, '321', 1.000000);

-- ----------------------------
-- Table structure for balance_history_87
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_87`;
CREATE TABLE `balance_history_87`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_87
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_88
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_88`;
CREATE TABLE `balance_history_88`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_88
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_89
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_89`;
CREATE TABLE `balance_history_89`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 90 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_89
-- ----------------------------
INSERT INTO `balance_history_89` VALUES (89, 1, '12', '32', 32, 32.0000000000000000, 32.0000000000000000, '32', 32.000000);

-- ----------------------------
-- Table structure for balance_history_9
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_9`;
CREATE TABLE `balance_history_9`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_9
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_90
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_90`;
CREATE TABLE `balance_history_90`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_90
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_91
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_91`;
CREATE TABLE `balance_history_91`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 92 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_91
-- ----------------------------
INSERT INTO `balance_history_91` VALUES (91, 91, '23', '31', 31, 312.0000000000000000, 3123.0000000000000000, '12', 78.000000);

-- ----------------------------
-- Table structure for balance_history_92
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_92`;
CREATE TABLE `balance_history_92`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_92
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_93
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_93`;
CREATE TABLE `balance_history_93`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_93
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_94
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_94`;
CREATE TABLE `balance_history_94`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 95 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_94
-- ----------------------------
INSERT INTO `balance_history_94` VALUES (94, 1, '1', '1', 1, 1.0000000000000000, 1.0000000000000000, '1', 1.000000);

-- ----------------------------
-- Table structure for balance_history_95
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_95`;
CREATE TABLE `balance_history_95`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_95
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_96
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_96`;
CREATE TABLE `balance_history_96`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_96
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_97
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_97`;
CREATE TABLE `balance_history_97`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_97
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_98
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_98`;
CREATE TABLE `balance_history_98`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_98
-- ----------------------------

-- ----------------------------
-- Table structure for balance_history_99
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_99`;
CREATE TABLE `balance_history_99`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_99
-- ----------------------------
INSERT INTO `balance_history_99` VALUES (99, 9, '9', '9', 9, 9.0000000000000000, 9.0000000000000000, '9', 9.000000);
INSERT INTO `balance_history_99` VALUES (100, 12, '23', '321', 323, 3123.0000000000000000, 312.0000000000000000, '321', 312.000000);

-- ----------------------------
-- Table structure for balance_history_template
-- ----------------------------
DROP TABLE IF EXISTS `balance_history_template`;
CREATE TABLE `balance_history_template`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `asset` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `change` decimal(32, 16) NOT NULL,
  `balance` decimal(32, 16) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_asset`(`user_id`, `asset`) USING BTREE,
  INDEX `idx_user_asset_business`(`user_id`, `asset`, `business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_history_template
-- ----------------------------

-- ----------------------------
-- Table structure for balance_offest
-- ----------------------------
DROP TABLE IF EXISTS `balance_offest`;
CREATE TABLE `balance_offest`  (
  `id` int(11) NOT NULL,
  `offest` int(255) NULL DEFAULT 0
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of balance_offest
-- ----------------------------
INSERT INTO `balance_offest` VALUES (0, 2);
INSERT INTO `balance_offest` VALUES (1, 0);
INSERT INTO `balance_offest` VALUES (2, 0);
INSERT INTO `balance_offest` VALUES (3, 0);
INSERT INTO `balance_offest` VALUES (4, 0);
INSERT INTO `balance_offest` VALUES (5, 0);
INSERT INTO `balance_offest` VALUES (6, 0);
INSERT INTO `balance_offest` VALUES (7, 0);
INSERT INTO `balance_offest` VALUES (8, 1);
INSERT INTO `balance_offest` VALUES (9, 0);
INSERT INTO `balance_offest` VALUES (10, 0);
INSERT INTO `balance_offest` VALUES (11, 0);
INSERT INTO `balance_offest` VALUES (12, 0);
INSERT INTO `balance_offest` VALUES (13, 0);
INSERT INTO `balance_offest` VALUES (14, 0);
INSERT INTO `balance_offest` VALUES (15, 0);
INSERT INTO `balance_offest` VALUES (16, 0);
INSERT INTO `balance_offest` VALUES (17, 0);
INSERT INTO `balance_offest` VALUES (18, 0);
INSERT INTO `balance_offest` VALUES (19, 0);
INSERT INTO `balance_offest` VALUES (20, 0);
INSERT INTO `balance_offest` VALUES (21, 0);
INSERT INTO `balance_offest` VALUES (22, 0);
INSERT INTO `balance_offest` VALUES (23, 0);
INSERT INTO `balance_offest` VALUES (24, 0);
INSERT INTO `balance_offest` VALUES (25, 0);
INSERT INTO `balance_offest` VALUES (26, 0);
INSERT INTO `balance_offest` VALUES (27, 0);
INSERT INTO `balance_offest` VALUES (28, 0);
INSERT INTO `balance_offest` VALUES (29, 0);
INSERT INTO `balance_offest` VALUES (30, 0);
INSERT INTO `balance_offest` VALUES (31, 0);
INSERT INTO `balance_offest` VALUES (32, 0);
INSERT INTO `balance_offest` VALUES (33, 0);
INSERT INTO `balance_offest` VALUES (34, 0);
INSERT INTO `balance_offest` VALUES (35, 0);
INSERT INTO `balance_offest` VALUES (36, 0);
INSERT INTO `balance_offest` VALUES (37, 0);
INSERT INTO `balance_offest` VALUES (38, 0);
INSERT INTO `balance_offest` VALUES (39, 0);
INSERT INTO `balance_offest` VALUES (40, 0);
INSERT INTO `balance_offest` VALUES (41, 0);
INSERT INTO `balance_offest` VALUES (42, 0);
INSERT INTO `balance_offest` VALUES (43, 0);
INSERT INTO `balance_offest` VALUES (44, 0);
INSERT INTO `balance_offest` VALUES (45, 0);
INSERT INTO `balance_offest` VALUES (46, 0);
INSERT INTO `balance_offest` VALUES (47, 0);
INSERT INTO `balance_offest` VALUES (48, 0);
INSERT INTO `balance_offest` VALUES (49, 0);
INSERT INTO `balance_offest` VALUES (50, 0);
INSERT INTO `balance_offest` VALUES (51, 0);
INSERT INTO `balance_offest` VALUES (52, 0);
INSERT INTO `balance_offest` VALUES (53, 0);
INSERT INTO `balance_offest` VALUES (54, 0);
INSERT INTO `balance_offest` VALUES (55, 0);
INSERT INTO `balance_offest` VALUES (56, 0);
INSERT INTO `balance_offest` VALUES (57, 0);
INSERT INTO `balance_offest` VALUES (58, 0);
INSERT INTO `balance_offest` VALUES (59, 0);
INSERT INTO `balance_offest` VALUES (60, 0);
INSERT INTO `balance_offest` VALUES (61, 0);
INSERT INTO `balance_offest` VALUES (62, 0);
INSERT INTO `balance_offest` VALUES (63, 0);
INSERT INTO `balance_offest` VALUES (64, 0);
INSERT INTO `balance_offest` VALUES (65, 0);
INSERT INTO `balance_offest` VALUES (66, 0);
INSERT INTO `balance_offest` VALUES (67, 0);
INSERT INTO `balance_offest` VALUES (68, 0);
INSERT INTO `balance_offest` VALUES (69, 0);
INSERT INTO `balance_offest` VALUES (70, 0);
INSERT INTO `balance_offest` VALUES (71, 0);
INSERT INTO `balance_offest` VALUES (72, 0);
INSERT INTO `balance_offest` VALUES (73, 0);
INSERT INTO `balance_offest` VALUES (74, 0);
INSERT INTO `balance_offest` VALUES (75, 0);
INSERT INTO `balance_offest` VALUES (76, 0);
INSERT INTO `balance_offest` VALUES (77, 0);
INSERT INTO `balance_offest` VALUES (78, 0);
INSERT INTO `balance_offest` VALUES (79, 0);
INSERT INTO `balance_offest` VALUES (80, 0);
INSERT INTO `balance_offest` VALUES (81, 0);
INSERT INTO `balance_offest` VALUES (82, 0);
INSERT INTO `balance_offest` VALUES (83, 0);
INSERT INTO `balance_offest` VALUES (84, 0);
INSERT INTO `balance_offest` VALUES (85, 0);
INSERT INTO `balance_offest` VALUES (86, 1);
INSERT INTO `balance_offest` VALUES (87, 0);
INSERT INTO `balance_offest` VALUES (88, 0);
INSERT INTO `balance_offest` VALUES (89, 89);
INSERT INTO `balance_offest` VALUES (90, 0);
INSERT INTO `balance_offest` VALUES (91, 91);
INSERT INTO `balance_offest` VALUES (92, 0);
INSERT INTO `balance_offest` VALUES (93, 0);
INSERT INTO `balance_offest` VALUES (94, 94);
INSERT INTO `balance_offest` VALUES (95, 0);
INSERT INTO `balance_offest` VALUES (96, 0);
INSERT INTO `balance_offest` VALUES (97, 0);
INSERT INTO `balance_offest` VALUES (98, 0);
INSERT INTO `balance_offest` VALUES (99, 100);

-- ----------------------------
-- Table structure for deal_offest
-- ----------------------------
DROP TABLE IF EXISTS `deal_offest`;
CREATE TABLE `deal_offest`  (
  `id` int(11) NOT NULL,
  `offest` int(20) NULL DEFAULT 0
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of deal_offest
-- ----------------------------
INSERT INTO `deal_offest` VALUES (0, 1);
INSERT INTO `deal_offest` VALUES (1, 0);
INSERT INTO `deal_offest` VALUES (2, 0);
INSERT INTO `deal_offest` VALUES (3, 0);
INSERT INTO `deal_offest` VALUES (4, 0);
INSERT INTO `deal_offest` VALUES (5, 0);
INSERT INTO `deal_offest` VALUES (6, 0);
INSERT INTO `deal_offest` VALUES (7, 0);
INSERT INTO `deal_offest` VALUES (8, 0);
INSERT INTO `deal_offest` VALUES (9, 0);
INSERT INTO `deal_offest` VALUES (10, 0);
INSERT INTO `deal_offest` VALUES (11, 0);
INSERT INTO `deal_offest` VALUES (12, 0);
INSERT INTO `deal_offest` VALUES (13, 0);
INSERT INTO `deal_offest` VALUES (14, 0);
INSERT INTO `deal_offest` VALUES (15, 0);
INSERT INTO `deal_offest` VALUES (16, 0);
INSERT INTO `deal_offest` VALUES (17, 0);
INSERT INTO `deal_offest` VALUES (18, 0);
INSERT INTO `deal_offest` VALUES (19, 0);
INSERT INTO `deal_offest` VALUES (20, 0);
INSERT INTO `deal_offest` VALUES (21, 0);
INSERT INTO `deal_offest` VALUES (22, 0);
INSERT INTO `deal_offest` VALUES (23, 0);
INSERT INTO `deal_offest` VALUES (24, 0);
INSERT INTO `deal_offest` VALUES (25, 0);
INSERT INTO `deal_offest` VALUES (26, 0);
INSERT INTO `deal_offest` VALUES (27, 0);
INSERT INTO `deal_offest` VALUES (28, 0);
INSERT INTO `deal_offest` VALUES (29, 0);
INSERT INTO `deal_offest` VALUES (30, 0);
INSERT INTO `deal_offest` VALUES (31, 0);
INSERT INTO `deal_offest` VALUES (32, 0);
INSERT INTO `deal_offest` VALUES (33, 0);
INSERT INTO `deal_offest` VALUES (34, 0);
INSERT INTO `deal_offest` VALUES (35, 0);
INSERT INTO `deal_offest` VALUES (36, 0);
INSERT INTO `deal_offest` VALUES (37, 0);
INSERT INTO `deal_offest` VALUES (38, 0);
INSERT INTO `deal_offest` VALUES (39, 0);
INSERT INTO `deal_offest` VALUES (40, 0);
INSERT INTO `deal_offest` VALUES (41, 0);
INSERT INTO `deal_offest` VALUES (42, 0);
INSERT INTO `deal_offest` VALUES (43, 0);
INSERT INTO `deal_offest` VALUES (44, 0);
INSERT INTO `deal_offest` VALUES (45, 0);
INSERT INTO `deal_offest` VALUES (46, 0);
INSERT INTO `deal_offest` VALUES (47, 47);
INSERT INTO `deal_offest` VALUES (48, 0);
INSERT INTO `deal_offest` VALUES (49, 0);
INSERT INTO `deal_offest` VALUES (50, 0);
INSERT INTO `deal_offest` VALUES (51, 0);
INSERT INTO `deal_offest` VALUES (52, 0);
INSERT INTO `deal_offest` VALUES (53, 0);
INSERT INTO `deal_offest` VALUES (54, 0);
INSERT INTO `deal_offest` VALUES (55, 0);
INSERT INTO `deal_offest` VALUES (56, 0);
INSERT INTO `deal_offest` VALUES (57, 0);
INSERT INTO `deal_offest` VALUES (58, 0);
INSERT INTO `deal_offest` VALUES (59, 0);
INSERT INTO `deal_offest` VALUES (60, 0);
INSERT INTO `deal_offest` VALUES (61, 0);
INSERT INTO `deal_offest` VALUES (62, 0);
INSERT INTO `deal_offest` VALUES (63, 0);
INSERT INTO `deal_offest` VALUES (64, 0);
INSERT INTO `deal_offest` VALUES (65, 0);
INSERT INTO `deal_offest` VALUES (66, 0);
INSERT INTO `deal_offest` VALUES (67, 0);
INSERT INTO `deal_offest` VALUES (68, 0);
INSERT INTO `deal_offest` VALUES (69, 0);
INSERT INTO `deal_offest` VALUES (70, 0);
INSERT INTO `deal_offest` VALUES (71, 0);
INSERT INTO `deal_offest` VALUES (72, 0);
INSERT INTO `deal_offest` VALUES (73, 0);
INSERT INTO `deal_offest` VALUES (74, 0);
INSERT INTO `deal_offest` VALUES (75, 0);
INSERT INTO `deal_offest` VALUES (76, 0);
INSERT INTO `deal_offest` VALUES (77, 0);
INSERT INTO `deal_offest` VALUES (78, 0);
INSERT INTO `deal_offest` VALUES (79, 0);
INSERT INTO `deal_offest` VALUES (80, 0);
INSERT INTO `deal_offest` VALUES (81, 0);
INSERT INTO `deal_offest` VALUES (82, 0);
INSERT INTO `deal_offest` VALUES (83, 0);
INSERT INTO `deal_offest` VALUES (84, 0);
INSERT INTO `deal_offest` VALUES (85, 0);
INSERT INTO `deal_offest` VALUES (86, 0);
INSERT INTO `deal_offest` VALUES (87, 0);
INSERT INTO `deal_offest` VALUES (88, 0);
INSERT INTO `deal_offest` VALUES (89, 0);
INSERT INTO `deal_offest` VALUES (90, 0);
INSERT INTO `deal_offest` VALUES (91, 0);
INSERT INTO `deal_offest` VALUES (92, 1);
INSERT INTO `deal_offest` VALUES (93, 0);
INSERT INTO `deal_offest` VALUES (94, 0);
INSERT INTO `deal_offest` VALUES (95, 0);
INSERT INTO `deal_offest` VALUES (96, 0);
INSERT INTO `deal_offest` VALUES (97, 0);
INSERT INTO `deal_offest` VALUES (98, 0);
INSERT INTO `deal_offest` VALUES (99, 0);

-- ----------------------------
-- Table structure for order_deal_history_0
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_0`;
CREATE TABLE `order_deal_history_0`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_0
-- ----------------------------
INSERT INTO `order_deal_history_0` VALUES (1, 1, '1', '1', '1', 1, 1, 1, 1, 1.00000000, 1.00000000, 1.00000000, 1.00000000, 1, 1, 1.000000);

-- ----------------------------
-- Table structure for order_deal_history_1
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_1`;
CREATE TABLE `order_deal_history_1`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_1
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_10
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_10`;
CREATE TABLE `order_deal_history_10`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_10
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_11
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_11`;
CREATE TABLE `order_deal_history_11`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_11
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_12
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_12`;
CREATE TABLE `order_deal_history_12`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_12
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_13
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_13`;
CREATE TABLE `order_deal_history_13`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_13
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_14
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_14`;
CREATE TABLE `order_deal_history_14`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_14
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_15
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_15`;
CREATE TABLE `order_deal_history_15`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_15
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_16
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_16`;
CREATE TABLE `order_deal_history_16`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_16
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_17
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_17`;
CREATE TABLE `order_deal_history_17`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_17
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_18
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_18`;
CREATE TABLE `order_deal_history_18`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_18
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_19
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_19`;
CREATE TABLE `order_deal_history_19`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_19
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_2
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_2`;
CREATE TABLE `order_deal_history_2`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_2
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_20
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_20`;
CREATE TABLE `order_deal_history_20`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_20
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_21
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_21`;
CREATE TABLE `order_deal_history_21`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_21
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_22
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_22`;
CREATE TABLE `order_deal_history_22`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_22
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_23
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_23`;
CREATE TABLE `order_deal_history_23`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_23
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_24
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_24`;
CREATE TABLE `order_deal_history_24`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_24
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_25
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_25`;
CREATE TABLE `order_deal_history_25`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_25
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_26
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_26`;
CREATE TABLE `order_deal_history_26`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_26
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_27
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_27`;
CREATE TABLE `order_deal_history_27`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_27
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_28
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_28`;
CREATE TABLE `order_deal_history_28`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_28
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_29
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_29`;
CREATE TABLE `order_deal_history_29`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_29
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_3
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_3`;
CREATE TABLE `order_deal_history_3`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_3
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_30
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_30`;
CREATE TABLE `order_deal_history_30`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_30
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_31
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_31`;
CREATE TABLE `order_deal_history_31`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_31
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_32
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_32`;
CREATE TABLE `order_deal_history_32`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_32
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_33
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_33`;
CREATE TABLE `order_deal_history_33`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_33
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_34
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_34`;
CREATE TABLE `order_deal_history_34`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_34
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_35
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_35`;
CREATE TABLE `order_deal_history_35`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_35
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_36
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_36`;
CREATE TABLE `order_deal_history_36`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_36
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_37
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_37`;
CREATE TABLE `order_deal_history_37`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_37
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_38
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_38`;
CREATE TABLE `order_deal_history_38`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_38
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_39
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_39`;
CREATE TABLE `order_deal_history_39`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_39
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_4
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_4`;
CREATE TABLE `order_deal_history_4`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_4
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_40
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_40`;
CREATE TABLE `order_deal_history_40`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_40
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_41
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_41`;
CREATE TABLE `order_deal_history_41`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_41
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_42
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_42`;
CREATE TABLE `order_deal_history_42`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_42
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_43
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_43`;
CREATE TABLE `order_deal_history_43`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_43
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_44
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_44`;
CREATE TABLE `order_deal_history_44`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_44
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_45
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_45`;
CREATE TABLE `order_deal_history_45`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_45
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_46
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_46`;
CREATE TABLE `order_deal_history_46`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_46
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_47
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_47`;
CREATE TABLE `order_deal_history_47`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_47
-- ----------------------------
INSERT INTO `order_deal_history_47` VALUES (47, 47, '323', '32', '32', 32, 32, 32, 32, 32.00000000, 32.00000000, 21.00000000, 1232.00000000, 32, 32, 31.000000);

-- ----------------------------
-- Table structure for order_deal_history_48
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_48`;
CREATE TABLE `order_deal_history_48`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_48
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_49
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_49`;
CREATE TABLE `order_deal_history_49`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_49
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_5
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_5`;
CREATE TABLE `order_deal_history_5`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_5
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_50
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_50`;
CREATE TABLE `order_deal_history_50`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_50
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_51
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_51`;
CREATE TABLE `order_deal_history_51`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_51
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_52
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_52`;
CREATE TABLE `order_deal_history_52`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_52
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_53
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_53`;
CREATE TABLE `order_deal_history_53`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_53
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_54
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_54`;
CREATE TABLE `order_deal_history_54`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_54
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_55
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_55`;
CREATE TABLE `order_deal_history_55`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_55
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_56
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_56`;
CREATE TABLE `order_deal_history_56`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_56
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_57
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_57`;
CREATE TABLE `order_deal_history_57`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_57
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_58
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_58`;
CREATE TABLE `order_deal_history_58`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_58
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_59
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_59`;
CREATE TABLE `order_deal_history_59`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_59
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_6
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_6`;
CREATE TABLE `order_deal_history_6`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_6
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_60
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_60`;
CREATE TABLE `order_deal_history_60`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_60
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_61
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_61`;
CREATE TABLE `order_deal_history_61`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_61
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_62
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_62`;
CREATE TABLE `order_deal_history_62`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_62
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_63
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_63`;
CREATE TABLE `order_deal_history_63`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_63
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_64
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_64`;
CREATE TABLE `order_deal_history_64`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_64
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_65
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_65`;
CREATE TABLE `order_deal_history_65`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_65
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_66
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_66`;
CREATE TABLE `order_deal_history_66`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_66
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_67
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_67`;
CREATE TABLE `order_deal_history_67`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_67
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_68
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_68`;
CREATE TABLE `order_deal_history_68`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_68
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_69
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_69`;
CREATE TABLE `order_deal_history_69`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_69
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_7
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_7`;
CREATE TABLE `order_deal_history_7`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_7
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_70
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_70`;
CREATE TABLE `order_deal_history_70`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_70
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_71
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_71`;
CREATE TABLE `order_deal_history_71`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_71
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_72
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_72`;
CREATE TABLE `order_deal_history_72`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_72
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_73
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_73`;
CREATE TABLE `order_deal_history_73`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_73
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_74
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_74`;
CREATE TABLE `order_deal_history_74`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_74
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_75
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_75`;
CREATE TABLE `order_deal_history_75`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_75
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_76
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_76`;
CREATE TABLE `order_deal_history_76`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_76
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_77
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_77`;
CREATE TABLE `order_deal_history_77`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_77
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_78
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_78`;
CREATE TABLE `order_deal_history_78`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_78
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_79
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_79`;
CREATE TABLE `order_deal_history_79`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_79
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_8
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_8`;
CREATE TABLE `order_deal_history_8`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_8
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_80
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_80`;
CREATE TABLE `order_deal_history_80`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_80
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_81
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_81`;
CREATE TABLE `order_deal_history_81`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_81
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_82
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_82`;
CREATE TABLE `order_deal_history_82`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_82
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_83
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_83`;
CREATE TABLE `order_deal_history_83`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_83
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_84
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_84`;
CREATE TABLE `order_deal_history_84`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_84
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_85
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_85`;
CREATE TABLE `order_deal_history_85`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_85
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_86
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_86`;
CREATE TABLE `order_deal_history_86`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_86
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_87
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_87`;
CREATE TABLE `order_deal_history_87`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_87
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_88
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_88`;
CREATE TABLE `order_deal_history_88`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_88
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_89
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_89`;
CREATE TABLE `order_deal_history_89`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_89
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_9
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_9`;
CREATE TABLE `order_deal_history_9`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_9
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_90
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_90`;
CREATE TABLE `order_deal_history_90`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_90
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_91
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_91`;
CREATE TABLE `order_deal_history_91`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_91
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_92
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_92`;
CREATE TABLE `order_deal_history_92`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_92
-- ----------------------------
INSERT INTO `order_deal_history_92` VALUES (1, 1, '43', '43', '1', 23, 34, 43, 3, 3.00000000, 3.00000000, 3.00000000, 3.00000000, 2, 2, 2.000000);

-- ----------------------------
-- Table structure for order_deal_history_93
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_93`;
CREATE TABLE `order_deal_history_93`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_93
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_94
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_94`;
CREATE TABLE `order_deal_history_94`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_94
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_95
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_95`;
CREATE TABLE `order_deal_history_95`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_95
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_96
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_96`;
CREATE TABLE `order_deal_history_96`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_96
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_97
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_97`;
CREATE TABLE `order_deal_history_97`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_97
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_98
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_98`;
CREATE TABLE `order_deal_history_98`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_98
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_99
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_99`;
CREATE TABLE `order_deal_history_99`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_99
-- ----------------------------

-- ----------------------------
-- Table structure for order_deal_history_template
-- ----------------------------
DROP TABLE IF EXISTS `order_deal_history_template`;
CREATE TABLE `order_deal_history_template`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_deal_history_template
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_0
-- ----------------------------
DROP TABLE IF EXISTS `order_history_0`;
CREATE TABLE `order_history_0`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_0
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_1
-- ----------------------------
DROP TABLE IF EXISTS `order_history_1`;
CREATE TABLE `order_history_1`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_1
-- ----------------------------
INSERT INTO `order_history_1` VALUES (1, 1, 1, 1, '23', '32', 32.00000000, 23.00000000, 321.00000000, 321.0000, 32.0000, 32.00000000, 32.00000000, 32.00000000, 132.000000, 123.000000);

-- ----------------------------
-- Table structure for order_history_10
-- ----------------------------
DROP TABLE IF EXISTS `order_history_10`;
CREATE TABLE `order_history_10`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_10
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_11
-- ----------------------------
DROP TABLE IF EXISTS `order_history_11`;
CREATE TABLE `order_history_11`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_11
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_12
-- ----------------------------
DROP TABLE IF EXISTS `order_history_12`;
CREATE TABLE `order_history_12`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_12
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_13
-- ----------------------------
DROP TABLE IF EXISTS `order_history_13`;
CREATE TABLE `order_history_13`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_13
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_14
-- ----------------------------
DROP TABLE IF EXISTS `order_history_14`;
CREATE TABLE `order_history_14`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_14
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_15
-- ----------------------------
DROP TABLE IF EXISTS `order_history_15`;
CREATE TABLE `order_history_15`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_15
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_16
-- ----------------------------
DROP TABLE IF EXISTS `order_history_16`;
CREATE TABLE `order_history_16`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_16
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_17
-- ----------------------------
DROP TABLE IF EXISTS `order_history_17`;
CREATE TABLE `order_history_17`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_17
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_18
-- ----------------------------
DROP TABLE IF EXISTS `order_history_18`;
CREATE TABLE `order_history_18`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_18
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_19
-- ----------------------------
DROP TABLE IF EXISTS `order_history_19`;
CREATE TABLE `order_history_19`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_19
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_2
-- ----------------------------
DROP TABLE IF EXISTS `order_history_2`;
CREATE TABLE `order_history_2`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_2
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_20
-- ----------------------------
DROP TABLE IF EXISTS `order_history_20`;
CREATE TABLE `order_history_20`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_20
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_21
-- ----------------------------
DROP TABLE IF EXISTS `order_history_21`;
CREATE TABLE `order_history_21`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_21
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_22
-- ----------------------------
DROP TABLE IF EXISTS `order_history_22`;
CREATE TABLE `order_history_22`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_22
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_23
-- ----------------------------
DROP TABLE IF EXISTS `order_history_23`;
CREATE TABLE `order_history_23`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_23
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_24
-- ----------------------------
DROP TABLE IF EXISTS `order_history_24`;
CREATE TABLE `order_history_24`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_24
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_25
-- ----------------------------
DROP TABLE IF EXISTS `order_history_25`;
CREATE TABLE `order_history_25`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_25
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_26
-- ----------------------------
DROP TABLE IF EXISTS `order_history_26`;
CREATE TABLE `order_history_26`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_26
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_27
-- ----------------------------
DROP TABLE IF EXISTS `order_history_27`;
CREATE TABLE `order_history_27`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_27
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_28
-- ----------------------------
DROP TABLE IF EXISTS `order_history_28`;
CREATE TABLE `order_history_28`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_28
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_29
-- ----------------------------
DROP TABLE IF EXISTS `order_history_29`;
CREATE TABLE `order_history_29`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_29
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_3
-- ----------------------------
DROP TABLE IF EXISTS `order_history_3`;
CREATE TABLE `order_history_3`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_3
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_30
-- ----------------------------
DROP TABLE IF EXISTS `order_history_30`;
CREATE TABLE `order_history_30`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_30
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_31
-- ----------------------------
DROP TABLE IF EXISTS `order_history_31`;
CREATE TABLE `order_history_31`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_31
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_32
-- ----------------------------
DROP TABLE IF EXISTS `order_history_32`;
CREATE TABLE `order_history_32`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_32
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_33
-- ----------------------------
DROP TABLE IF EXISTS `order_history_33`;
CREATE TABLE `order_history_33`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_33
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_34
-- ----------------------------
DROP TABLE IF EXISTS `order_history_34`;
CREATE TABLE `order_history_34`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_34
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_35
-- ----------------------------
DROP TABLE IF EXISTS `order_history_35`;
CREATE TABLE `order_history_35`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_35
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_36
-- ----------------------------
DROP TABLE IF EXISTS `order_history_36`;
CREATE TABLE `order_history_36`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_36
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_37
-- ----------------------------
DROP TABLE IF EXISTS `order_history_37`;
CREATE TABLE `order_history_37`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_37
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_38
-- ----------------------------
DROP TABLE IF EXISTS `order_history_38`;
CREATE TABLE `order_history_38`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_38
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_39
-- ----------------------------
DROP TABLE IF EXISTS `order_history_39`;
CREATE TABLE `order_history_39`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_39
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_4
-- ----------------------------
DROP TABLE IF EXISTS `order_history_4`;
CREATE TABLE `order_history_4`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_4
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_40
-- ----------------------------
DROP TABLE IF EXISTS `order_history_40`;
CREATE TABLE `order_history_40`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_40
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_41
-- ----------------------------
DROP TABLE IF EXISTS `order_history_41`;
CREATE TABLE `order_history_41`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_41
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_42
-- ----------------------------
DROP TABLE IF EXISTS `order_history_42`;
CREATE TABLE `order_history_42`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_42
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_43
-- ----------------------------
DROP TABLE IF EXISTS `order_history_43`;
CREATE TABLE `order_history_43`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_43
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_44
-- ----------------------------
DROP TABLE IF EXISTS `order_history_44`;
CREATE TABLE `order_history_44`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_44
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_45
-- ----------------------------
DROP TABLE IF EXISTS `order_history_45`;
CREATE TABLE `order_history_45`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_45
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_46
-- ----------------------------
DROP TABLE IF EXISTS `order_history_46`;
CREATE TABLE `order_history_46`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_46
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_47
-- ----------------------------
DROP TABLE IF EXISTS `order_history_47`;
CREATE TABLE `order_history_47`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_47
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_48
-- ----------------------------
DROP TABLE IF EXISTS `order_history_48`;
CREATE TABLE `order_history_48`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_48
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_49
-- ----------------------------
DROP TABLE IF EXISTS `order_history_49`;
CREATE TABLE `order_history_49`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_49
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_5
-- ----------------------------
DROP TABLE IF EXISTS `order_history_5`;
CREATE TABLE `order_history_5`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_5
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_50
-- ----------------------------
DROP TABLE IF EXISTS `order_history_50`;
CREATE TABLE `order_history_50`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_50
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_51
-- ----------------------------
DROP TABLE IF EXISTS `order_history_51`;
CREATE TABLE `order_history_51`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_51
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_52
-- ----------------------------
DROP TABLE IF EXISTS `order_history_52`;
CREATE TABLE `order_history_52`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_52
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_53
-- ----------------------------
DROP TABLE IF EXISTS `order_history_53`;
CREATE TABLE `order_history_53`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_53
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_54
-- ----------------------------
DROP TABLE IF EXISTS `order_history_54`;
CREATE TABLE `order_history_54`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_54
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_55
-- ----------------------------
DROP TABLE IF EXISTS `order_history_55`;
CREATE TABLE `order_history_55`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_55
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_56
-- ----------------------------
DROP TABLE IF EXISTS `order_history_56`;
CREATE TABLE `order_history_56`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_56
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_57
-- ----------------------------
DROP TABLE IF EXISTS `order_history_57`;
CREATE TABLE `order_history_57`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_57
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_58
-- ----------------------------
DROP TABLE IF EXISTS `order_history_58`;
CREATE TABLE `order_history_58`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_58
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_59
-- ----------------------------
DROP TABLE IF EXISTS `order_history_59`;
CREATE TABLE `order_history_59`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_59
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_6
-- ----------------------------
DROP TABLE IF EXISTS `order_history_6`;
CREATE TABLE `order_history_6`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_6
-- ----------------------------
INSERT INTO `order_history_6` VALUES (2, 32, 32, 321, '312', '31', 31.00000000, 313.00000000, 131.00000000, 2312.0000, 31.0000, 31.00000000, 32.00000000, 32.00000000, 32.000000, 32.000000);

-- ----------------------------
-- Table structure for order_history_60
-- ----------------------------
DROP TABLE IF EXISTS `order_history_60`;
CREATE TABLE `order_history_60`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_60
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_61
-- ----------------------------
DROP TABLE IF EXISTS `order_history_61`;
CREATE TABLE `order_history_61`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_61
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_62
-- ----------------------------
DROP TABLE IF EXISTS `order_history_62`;
CREATE TABLE `order_history_62`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_62
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_63
-- ----------------------------
DROP TABLE IF EXISTS `order_history_63`;
CREATE TABLE `order_history_63`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_63
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_64
-- ----------------------------
DROP TABLE IF EXISTS `order_history_64`;
CREATE TABLE `order_history_64`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_64
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_65
-- ----------------------------
DROP TABLE IF EXISTS `order_history_65`;
CREATE TABLE `order_history_65`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_65
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_66
-- ----------------------------
DROP TABLE IF EXISTS `order_history_66`;
CREATE TABLE `order_history_66`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_66
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_67
-- ----------------------------
DROP TABLE IF EXISTS `order_history_67`;
CREATE TABLE `order_history_67`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_67
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_68
-- ----------------------------
DROP TABLE IF EXISTS `order_history_68`;
CREATE TABLE `order_history_68`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_68
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_69
-- ----------------------------
DROP TABLE IF EXISTS `order_history_69`;
CREATE TABLE `order_history_69`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_69
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_7
-- ----------------------------
DROP TABLE IF EXISTS `order_history_7`;
CREATE TABLE `order_history_7`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_7
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_70
-- ----------------------------
DROP TABLE IF EXISTS `order_history_70`;
CREATE TABLE `order_history_70`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_70
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_71
-- ----------------------------
DROP TABLE IF EXISTS `order_history_71`;
CREATE TABLE `order_history_71`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_71
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_72
-- ----------------------------
DROP TABLE IF EXISTS `order_history_72`;
CREATE TABLE `order_history_72`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_72
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_73
-- ----------------------------
DROP TABLE IF EXISTS `order_history_73`;
CREATE TABLE `order_history_73`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_73
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_74
-- ----------------------------
DROP TABLE IF EXISTS `order_history_74`;
CREATE TABLE `order_history_74`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_74
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_75
-- ----------------------------
DROP TABLE IF EXISTS `order_history_75`;
CREATE TABLE `order_history_75`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_75
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_76
-- ----------------------------
DROP TABLE IF EXISTS `order_history_76`;
CREATE TABLE `order_history_76`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_76
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_77
-- ----------------------------
DROP TABLE IF EXISTS `order_history_77`;
CREATE TABLE `order_history_77`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_77
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_78
-- ----------------------------
DROP TABLE IF EXISTS `order_history_78`;
CREATE TABLE `order_history_78`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_78
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_79
-- ----------------------------
DROP TABLE IF EXISTS `order_history_79`;
CREATE TABLE `order_history_79`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_79
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_8
-- ----------------------------
DROP TABLE IF EXISTS `order_history_8`;
CREATE TABLE `order_history_8`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_8
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_80
-- ----------------------------
DROP TABLE IF EXISTS `order_history_80`;
CREATE TABLE `order_history_80`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_80
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_81
-- ----------------------------
DROP TABLE IF EXISTS `order_history_81`;
CREATE TABLE `order_history_81`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_81
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_82
-- ----------------------------
DROP TABLE IF EXISTS `order_history_82`;
CREATE TABLE `order_history_82`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_82
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_83
-- ----------------------------
DROP TABLE IF EXISTS `order_history_83`;
CREATE TABLE `order_history_83`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_83
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_84
-- ----------------------------
DROP TABLE IF EXISTS `order_history_84`;
CREATE TABLE `order_history_84`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_84
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_85
-- ----------------------------
DROP TABLE IF EXISTS `order_history_85`;
CREATE TABLE `order_history_85`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_85
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_86
-- ----------------------------
DROP TABLE IF EXISTS `order_history_86`;
CREATE TABLE `order_history_86`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_86
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_87
-- ----------------------------
DROP TABLE IF EXISTS `order_history_87`;
CREATE TABLE `order_history_87`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_87
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_88
-- ----------------------------
DROP TABLE IF EXISTS `order_history_88`;
CREATE TABLE `order_history_88`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_88
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_89
-- ----------------------------
DROP TABLE IF EXISTS `order_history_89`;
CREATE TABLE `order_history_89`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_89
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_9
-- ----------------------------
DROP TABLE IF EXISTS `order_history_9`;
CREATE TABLE `order_history_9`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_9
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_90
-- ----------------------------
DROP TABLE IF EXISTS `order_history_90`;
CREATE TABLE `order_history_90`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_90
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_91
-- ----------------------------
DROP TABLE IF EXISTS `order_history_91`;
CREATE TABLE `order_history_91`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_91
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_92
-- ----------------------------
DROP TABLE IF EXISTS `order_history_92`;
CREATE TABLE `order_history_92`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_92
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_93
-- ----------------------------
DROP TABLE IF EXISTS `order_history_93`;
CREATE TABLE `order_history_93`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_93
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_94
-- ----------------------------
DROP TABLE IF EXISTS `order_history_94`;
CREATE TABLE `order_history_94`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_94
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_95
-- ----------------------------
DROP TABLE IF EXISTS `order_history_95`;
CREATE TABLE `order_history_95`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_95
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_96
-- ----------------------------
DROP TABLE IF EXISTS `order_history_96`;
CREATE TABLE `order_history_96`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_96
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_97
-- ----------------------------
DROP TABLE IF EXISTS `order_history_97`;
CREATE TABLE `order_history_97`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_97
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_98
-- ----------------------------
DROP TABLE IF EXISTS `order_history_98`;
CREATE TABLE `order_history_98`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_98
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_99
-- ----------------------------
DROP TABLE IF EXISTS `order_history_99`;
CREATE TABLE `order_history_99`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_99
-- ----------------------------

-- ----------------------------
-- Table structure for order_history_template
-- ----------------------------
DROP TABLE IF EXISTS `order_history_template`;
CREATE TABLE `order_history_template`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history_template
-- ----------------------------

-- ----------------------------
-- Table structure for order_offest
-- ----------------------------
DROP TABLE IF EXISTS `order_offest`;
CREATE TABLE `order_offest`  (
  `id` int(11) NOT NULL,
  `offest` int(25) NULL DEFAULT 0
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_offest
-- ----------------------------
INSERT INTO `order_offest` VALUES (0, 0);
INSERT INTO `order_offest` VALUES (1, 1);
INSERT INTO `order_offest` VALUES (2, 0);
INSERT INTO `order_offest` VALUES (3, 0);
INSERT INTO `order_offest` VALUES (4, 0);
INSERT INTO `order_offest` VALUES (5, 0);
INSERT INTO `order_offest` VALUES (6, 2);
INSERT INTO `order_offest` VALUES (7, 0);
INSERT INTO `order_offest` VALUES (8, 0);
INSERT INTO `order_offest` VALUES (9, 0);
INSERT INTO `order_offest` VALUES (10, 0);
INSERT INTO `order_offest` VALUES (11, 0);
INSERT INTO `order_offest` VALUES (12, 0);
INSERT INTO `order_offest` VALUES (13, 0);
INSERT INTO `order_offest` VALUES (14, 0);
INSERT INTO `order_offest` VALUES (15, 0);
INSERT INTO `order_offest` VALUES (16, 0);
INSERT INTO `order_offest` VALUES (17, 0);
INSERT INTO `order_offest` VALUES (18, 0);
INSERT INTO `order_offest` VALUES (19, 0);
INSERT INTO `order_offest` VALUES (20, 0);
INSERT INTO `order_offest` VALUES (21, 0);
INSERT INTO `order_offest` VALUES (22, 0);
INSERT INTO `order_offest` VALUES (23, 0);
INSERT INTO `order_offest` VALUES (24, 0);
INSERT INTO `order_offest` VALUES (25, 0);
INSERT INTO `order_offest` VALUES (26, 0);
INSERT INTO `order_offest` VALUES (27, 0);
INSERT INTO `order_offest` VALUES (28, 0);
INSERT INTO `order_offest` VALUES (29, 0);
INSERT INTO `order_offest` VALUES (30, 0);
INSERT INTO `order_offest` VALUES (31, 0);
INSERT INTO `order_offest` VALUES (32, 0);
INSERT INTO `order_offest` VALUES (33, 0);
INSERT INTO `order_offest` VALUES (34, 0);
INSERT INTO `order_offest` VALUES (35, 0);
INSERT INTO `order_offest` VALUES (36, 0);
INSERT INTO `order_offest` VALUES (37, 0);
INSERT INTO `order_offest` VALUES (38, 0);
INSERT INTO `order_offest` VALUES (39, 0);
INSERT INTO `order_offest` VALUES (40, 0);
INSERT INTO `order_offest` VALUES (41, 0);
INSERT INTO `order_offest` VALUES (42, 0);
INSERT INTO `order_offest` VALUES (43, 0);
INSERT INTO `order_offest` VALUES (44, 0);
INSERT INTO `order_offest` VALUES (45, 0);
INSERT INTO `order_offest` VALUES (46, 0);
INSERT INTO `order_offest` VALUES (47, 0);
INSERT INTO `order_offest` VALUES (48, 0);
INSERT INTO `order_offest` VALUES (49, 0);
INSERT INTO `order_offest` VALUES (50, 0);
INSERT INTO `order_offest` VALUES (51, 0);
INSERT INTO `order_offest` VALUES (52, 0);
INSERT INTO `order_offest` VALUES (53, 0);
INSERT INTO `order_offest` VALUES (54, 0);
INSERT INTO `order_offest` VALUES (55, 0);
INSERT INTO `order_offest` VALUES (56, 0);
INSERT INTO `order_offest` VALUES (57, 0);
INSERT INTO `order_offest` VALUES (58, 0);
INSERT INTO `order_offest` VALUES (59, 0);
INSERT INTO `order_offest` VALUES (60, 0);
INSERT INTO `order_offest` VALUES (61, 0);
INSERT INTO `order_offest` VALUES (62, 0);
INSERT INTO `order_offest` VALUES (63, 0);
INSERT INTO `order_offest` VALUES (64, 0);
INSERT INTO `order_offest` VALUES (65, 0);
INSERT INTO `order_offest` VALUES (66, 0);
INSERT INTO `order_offest` VALUES (67, 0);
INSERT INTO `order_offest` VALUES (68, 0);
INSERT INTO `order_offest` VALUES (69, 0);
INSERT INTO `order_offest` VALUES (70, 0);
INSERT INTO `order_offest` VALUES (71, 0);
INSERT INTO `order_offest` VALUES (72, 0);
INSERT INTO `order_offest` VALUES (73, 0);
INSERT INTO `order_offest` VALUES (74, 0);
INSERT INTO `order_offest` VALUES (75, 0);
INSERT INTO `order_offest` VALUES (76, 0);
INSERT INTO `order_offest` VALUES (77, 0);
INSERT INTO `order_offest` VALUES (78, 0);
INSERT INTO `order_offest` VALUES (79, 0);
INSERT INTO `order_offest` VALUES (80, 0);
INSERT INTO `order_offest` VALUES (81, 0);
INSERT INTO `order_offest` VALUES (82, 0);
INSERT INTO `order_offest` VALUES (83, 0);
INSERT INTO `order_offest` VALUES (84, 0);
INSERT INTO `order_offest` VALUES (85, 0);
INSERT INTO `order_offest` VALUES (86, 0);
INSERT INTO `order_offest` VALUES (87, 0);
INSERT INTO `order_offest` VALUES (88, 0);
INSERT INTO `order_offest` VALUES (89, 0);
INSERT INTO `order_offest` VALUES (90, 0);
INSERT INTO `order_offest` VALUES (91, 0);
INSERT INTO `order_offest` VALUES (92, 0);
INSERT INTO `order_offest` VALUES (93, 0);
INSERT INTO `order_offest` VALUES (94, 0);
INSERT INTO `order_offest` VALUES (95, 0);
INSERT INTO `order_offest` VALUES (96, 0);
INSERT INTO `order_offest` VALUES (97, 0);
INSERT INTO `order_offest` VALUES (98, 0);
INSERT INTO `order_offest` VALUES (99, 0);

-- ----------------------------
-- Table structure for user_deal_history_0
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_0`;
CREATE TABLE `user_deal_history_0`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_0
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_1
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_1`;
CREATE TABLE `user_deal_history_1`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_1
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_10
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_10`;
CREATE TABLE `user_deal_history_10`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_10
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_11
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_11`;
CREATE TABLE `user_deal_history_11`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_11
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_12
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_12`;
CREATE TABLE `user_deal_history_12`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_12
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_13
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_13`;
CREATE TABLE `user_deal_history_13`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_13
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_14
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_14`;
CREATE TABLE `user_deal_history_14`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_14
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_15
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_15`;
CREATE TABLE `user_deal_history_15`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_15
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_16
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_16`;
CREATE TABLE `user_deal_history_16`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_16
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_17
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_17`;
CREATE TABLE `user_deal_history_17`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_17
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_18
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_18`;
CREATE TABLE `user_deal_history_18`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_18
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_19
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_19`;
CREATE TABLE `user_deal_history_19`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_19
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_2
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_2`;
CREATE TABLE `user_deal_history_2`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_2
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_20
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_20`;
CREATE TABLE `user_deal_history_20`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_20
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_21
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_21`;
CREATE TABLE `user_deal_history_21`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_21
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_22
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_22`;
CREATE TABLE `user_deal_history_22`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_22
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_23
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_23`;
CREATE TABLE `user_deal_history_23`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_23
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_24
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_24`;
CREATE TABLE `user_deal_history_24`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_24
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_25
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_25`;
CREATE TABLE `user_deal_history_25`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_25
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_26
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_26`;
CREATE TABLE `user_deal_history_26`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_26
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_27
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_27`;
CREATE TABLE `user_deal_history_27`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_27
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_28
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_28`;
CREATE TABLE `user_deal_history_28`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_28
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_29
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_29`;
CREATE TABLE `user_deal_history_29`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_29
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_3
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_3`;
CREATE TABLE `user_deal_history_3`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_3
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_30
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_30`;
CREATE TABLE `user_deal_history_30`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_30
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_31
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_31`;
CREATE TABLE `user_deal_history_31`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_31
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_32
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_32`;
CREATE TABLE `user_deal_history_32`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_32
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_33
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_33`;
CREATE TABLE `user_deal_history_33`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_33
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_34
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_34`;
CREATE TABLE `user_deal_history_34`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_34
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_35
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_35`;
CREATE TABLE `user_deal_history_35`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_35
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_36
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_36`;
CREATE TABLE `user_deal_history_36`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_36
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_37
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_37`;
CREATE TABLE `user_deal_history_37`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_37
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_38
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_38`;
CREATE TABLE `user_deal_history_38`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_38
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_39
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_39`;
CREATE TABLE `user_deal_history_39`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_39
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_4
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_4`;
CREATE TABLE `user_deal_history_4`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_4
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_40
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_40`;
CREATE TABLE `user_deal_history_40`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_40
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_41
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_41`;
CREATE TABLE `user_deal_history_41`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_41
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_42
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_42`;
CREATE TABLE `user_deal_history_42`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_42
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_43
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_43`;
CREATE TABLE `user_deal_history_43`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_43
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_44
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_44`;
CREATE TABLE `user_deal_history_44`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_44
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_45
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_45`;
CREATE TABLE `user_deal_history_45`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_45
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_46
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_46`;
CREATE TABLE `user_deal_history_46`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_46
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_47
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_47`;
CREATE TABLE `user_deal_history_47`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_47
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_48
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_48`;
CREATE TABLE `user_deal_history_48`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_48
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_49
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_49`;
CREATE TABLE `user_deal_history_49`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_49
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_5
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_5`;
CREATE TABLE `user_deal_history_5`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_5
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_50
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_50`;
CREATE TABLE `user_deal_history_50`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_50
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_51
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_51`;
CREATE TABLE `user_deal_history_51`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_51
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_52
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_52`;
CREATE TABLE `user_deal_history_52`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_52
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_53
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_53`;
CREATE TABLE `user_deal_history_53`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_53
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_54
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_54`;
CREATE TABLE `user_deal_history_54`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_54
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_55
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_55`;
CREATE TABLE `user_deal_history_55`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_55
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_56
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_56`;
CREATE TABLE `user_deal_history_56`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_56
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_57
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_57`;
CREATE TABLE `user_deal_history_57`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_57
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_58
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_58`;
CREATE TABLE `user_deal_history_58`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_58
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_59
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_59`;
CREATE TABLE `user_deal_history_59`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_59
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_6
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_6`;
CREATE TABLE `user_deal_history_6`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_6
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_60
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_60`;
CREATE TABLE `user_deal_history_60`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_60
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_61
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_61`;
CREATE TABLE `user_deal_history_61`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_61
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_62
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_62`;
CREATE TABLE `user_deal_history_62`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_62
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_63
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_63`;
CREATE TABLE `user_deal_history_63`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_63
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_64
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_64`;
CREATE TABLE `user_deal_history_64`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_64
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_65
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_65`;
CREATE TABLE `user_deal_history_65`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_65
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_66
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_66`;
CREATE TABLE `user_deal_history_66`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_66
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_67
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_67`;
CREATE TABLE `user_deal_history_67`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_67
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_68
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_68`;
CREATE TABLE `user_deal_history_68`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_68
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_69
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_69`;
CREATE TABLE `user_deal_history_69`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_69
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_7
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_7`;
CREATE TABLE `user_deal_history_7`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_7
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_70
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_70`;
CREATE TABLE `user_deal_history_70`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_70
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_71
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_71`;
CREATE TABLE `user_deal_history_71`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_71
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_72
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_72`;
CREATE TABLE `user_deal_history_72`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_72
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_73
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_73`;
CREATE TABLE `user_deal_history_73`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_73
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_74
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_74`;
CREATE TABLE `user_deal_history_74`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_74
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_75
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_75`;
CREATE TABLE `user_deal_history_75`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_75
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_76
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_76`;
CREATE TABLE `user_deal_history_76`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_76
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_77
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_77`;
CREATE TABLE `user_deal_history_77`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_77
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_78
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_78`;
CREATE TABLE `user_deal_history_78`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_78
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_79
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_79`;
CREATE TABLE `user_deal_history_79`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_79
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_8
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_8`;
CREATE TABLE `user_deal_history_8`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_8
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_80
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_80`;
CREATE TABLE `user_deal_history_80`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_80
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_81
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_81`;
CREATE TABLE `user_deal_history_81`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_81
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_82
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_82`;
CREATE TABLE `user_deal_history_82`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_82
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_83
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_83`;
CREATE TABLE `user_deal_history_83`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_83
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_84
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_84`;
CREATE TABLE `user_deal_history_84`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_84
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_85
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_85`;
CREATE TABLE `user_deal_history_85`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_85
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_86
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_86`;
CREATE TABLE `user_deal_history_86`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_86
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_87
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_87`;
CREATE TABLE `user_deal_history_87`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_87
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_88
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_88`;
CREATE TABLE `user_deal_history_88`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_88
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_89
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_89`;
CREATE TABLE `user_deal_history_89`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_89
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_9
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_9`;
CREATE TABLE `user_deal_history_9`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_9
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_90
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_90`;
CREATE TABLE `user_deal_history_90`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_90
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_91
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_91`;
CREATE TABLE `user_deal_history_91`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_91
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_92
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_92`;
CREATE TABLE `user_deal_history_92`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_92
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_93
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_93`;
CREATE TABLE `user_deal_history_93`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_93
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_94
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_94`;
CREATE TABLE `user_deal_history_94`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_94
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_95
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_95`;
CREATE TABLE `user_deal_history_95`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_95
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_96
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_96`;
CREATE TABLE `user_deal_history_96`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_96
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_97
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_97`;
CREATE TABLE `user_deal_history_97`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_97
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_98
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_98`;
CREATE TABLE `user_deal_history_98`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_98
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_99
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_99`;
CREATE TABLE `user_deal_history_99`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_99
-- ----------------------------

-- ----------------------------
-- Table structure for user_deal_history_template
-- ----------------------------
DROP TABLE IF EXISTS `user_deal_history_template`;
CREATE TABLE `user_deal_history_template`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deal_id` bigint(20) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `deal_order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `fee` decimal(32, 8) NOT NULL,
  `dealer_fee` decimal(32, 8) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Taker, 2 Maker',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `time` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_deal_history_template
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_0
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_0`;
CREATE TABLE `user_order_history_0`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_0
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_1
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_1`;
CREATE TABLE `user_order_history_1`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_1
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_10
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_10`;
CREATE TABLE `user_order_history_10`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_10
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_11
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_11`;
CREATE TABLE `user_order_history_11`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_11
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_12
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_12`;
CREATE TABLE `user_order_history_12`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_12
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_13
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_13`;
CREATE TABLE `user_order_history_13`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_13
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_14
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_14`;
CREATE TABLE `user_order_history_14`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_14
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_15
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_15`;
CREATE TABLE `user_order_history_15`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_15
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_16
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_16`;
CREATE TABLE `user_order_history_16`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_16
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_17
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_17`;
CREATE TABLE `user_order_history_17`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_17
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_18
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_18`;
CREATE TABLE `user_order_history_18`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_18
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_19
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_19`;
CREATE TABLE `user_order_history_19`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_19
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_2
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_2`;
CREATE TABLE `user_order_history_2`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_2
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_20
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_20`;
CREATE TABLE `user_order_history_20`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_20
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_21
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_21`;
CREATE TABLE `user_order_history_21`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_21
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_22
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_22`;
CREATE TABLE `user_order_history_22`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_22
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_23
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_23`;
CREATE TABLE `user_order_history_23`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_23
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_24
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_24`;
CREATE TABLE `user_order_history_24`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_24
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_25
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_25`;
CREATE TABLE `user_order_history_25`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_25
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_26
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_26`;
CREATE TABLE `user_order_history_26`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_26
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_27
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_27`;
CREATE TABLE `user_order_history_27`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_27
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_28
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_28`;
CREATE TABLE `user_order_history_28`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_28
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_29
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_29`;
CREATE TABLE `user_order_history_29`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_29
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_3
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_3`;
CREATE TABLE `user_order_history_3`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_3
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_30
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_30`;
CREATE TABLE `user_order_history_30`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_30
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_31
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_31`;
CREATE TABLE `user_order_history_31`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_31
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_32
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_32`;
CREATE TABLE `user_order_history_32`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_32
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_33
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_33`;
CREATE TABLE `user_order_history_33`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_33
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_34
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_34`;
CREATE TABLE `user_order_history_34`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_34
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_35
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_35`;
CREATE TABLE `user_order_history_35`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_35
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_36
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_36`;
CREATE TABLE `user_order_history_36`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_36
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_37
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_37`;
CREATE TABLE `user_order_history_37`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_37
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_38
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_38`;
CREATE TABLE `user_order_history_38`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_38
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_39
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_39`;
CREATE TABLE `user_order_history_39`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_39
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_4
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_4`;
CREATE TABLE `user_order_history_4`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_4
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_40
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_40`;
CREATE TABLE `user_order_history_40`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_40
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_41
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_41`;
CREATE TABLE `user_order_history_41`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_41
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_42
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_42`;
CREATE TABLE `user_order_history_42`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_42
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_43
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_43`;
CREATE TABLE `user_order_history_43`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_43
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_44
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_44`;
CREATE TABLE `user_order_history_44`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_44
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_45
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_45`;
CREATE TABLE `user_order_history_45`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_45
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_46
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_46`;
CREATE TABLE `user_order_history_46`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_46
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_47
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_47`;
CREATE TABLE `user_order_history_47`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_47
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_48
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_48`;
CREATE TABLE `user_order_history_48`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_48
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_49
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_49`;
CREATE TABLE `user_order_history_49`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_49
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_5
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_5`;
CREATE TABLE `user_order_history_5`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_5
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_50
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_50`;
CREATE TABLE `user_order_history_50`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_50
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_51
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_51`;
CREATE TABLE `user_order_history_51`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_51
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_52
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_52`;
CREATE TABLE `user_order_history_52`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_52
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_53
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_53`;
CREATE TABLE `user_order_history_53`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_53
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_54
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_54`;
CREATE TABLE `user_order_history_54`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_54
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_55
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_55`;
CREATE TABLE `user_order_history_55`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_55
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_56
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_56`;
CREATE TABLE `user_order_history_56`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_56
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_57
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_57`;
CREATE TABLE `user_order_history_57`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_57
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_58
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_58`;
CREATE TABLE `user_order_history_58`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_58
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_59
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_59`;
CREATE TABLE `user_order_history_59`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_59
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_6
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_6`;
CREATE TABLE `user_order_history_6`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_6
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_60
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_60`;
CREATE TABLE `user_order_history_60`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_60
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_61
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_61`;
CREATE TABLE `user_order_history_61`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_61
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_62
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_62`;
CREATE TABLE `user_order_history_62`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_62
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_63
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_63`;
CREATE TABLE `user_order_history_63`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_63
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_64
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_64`;
CREATE TABLE `user_order_history_64`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_64
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_65
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_65`;
CREATE TABLE `user_order_history_65`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_65
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_66
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_66`;
CREATE TABLE `user_order_history_66`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_66
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_67
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_67`;
CREATE TABLE `user_order_history_67`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_67
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_68
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_68`;
CREATE TABLE `user_order_history_68`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_68
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_69
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_69`;
CREATE TABLE `user_order_history_69`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_69
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_7
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_7`;
CREATE TABLE `user_order_history_7`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_7
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_70
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_70`;
CREATE TABLE `user_order_history_70`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_70
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_71
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_71`;
CREATE TABLE `user_order_history_71`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_71
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_72
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_72`;
CREATE TABLE `user_order_history_72`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_72
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_73
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_73`;
CREATE TABLE `user_order_history_73`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_73
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_74
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_74`;
CREATE TABLE `user_order_history_74`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_74
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_75
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_75`;
CREATE TABLE `user_order_history_75`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_75
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_76
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_76`;
CREATE TABLE `user_order_history_76`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_76
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_77
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_77`;
CREATE TABLE `user_order_history_77`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_77
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_78
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_78`;
CREATE TABLE `user_order_history_78`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_78
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_79
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_79`;
CREATE TABLE `user_order_history_79`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_79
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_8
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_8`;
CREATE TABLE `user_order_history_8`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_8
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_80
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_80`;
CREATE TABLE `user_order_history_80`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_80
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_81
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_81`;
CREATE TABLE `user_order_history_81`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_81
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_82
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_82`;
CREATE TABLE `user_order_history_82`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_82
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_83
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_83`;
CREATE TABLE `user_order_history_83`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_83
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_84
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_84`;
CREATE TABLE `user_order_history_84`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_84
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_85
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_85`;
CREATE TABLE `user_order_history_85`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_85
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_86
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_86`;
CREATE TABLE `user_order_history_86`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_86
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_87
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_87`;
CREATE TABLE `user_order_history_87`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_87
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_88
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_88`;
CREATE TABLE `user_order_history_88`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_88
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_89
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_89`;
CREATE TABLE `user_order_history_89`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_89
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_9
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_9`;
CREATE TABLE `user_order_history_9`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_9
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_90
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_90`;
CREATE TABLE `user_order_history_90`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_90
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_91
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_91`;
CREATE TABLE `user_order_history_91`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_91
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_92
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_92`;
CREATE TABLE `user_order_history_92`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_92
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_93
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_93`;
CREATE TABLE `user_order_history_93`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_93
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_94
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_94`;
CREATE TABLE `user_order_history_94`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_94
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_95
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_95`;
CREATE TABLE `user_order_history_95`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_95
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_96
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_96`;
CREATE TABLE `user_order_history_96`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_96
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_97
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_97`;
CREATE TABLE `user_order_history_97`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_97
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_98
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_98`;
CREATE TABLE `user_order_history_98`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_98
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_99
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_99`;
CREATE TABLE `user_order_history_99`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_99
-- ----------------------------

-- ----------------------------
-- Table structure for user_order_history_template
-- ----------------------------
DROP TABLE IF EXISTS `user_order_history_template`;
CREATE TABLE `user_order_history_template`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Limit, 2 Market',
  `side` tinyint(3) UNSIGNED NOT NULL COMMENT '1 Ask, 2 Bid',
  `user_id` int(10) UNSIGNED NOT NULL,
  `market` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(32, 8) NOT NULL,
  `amount` decimal(32, 8) NOT NULL,
  `left` decimal(32, 8) NOT NULL,
  `taker_fee` decimal(32, 4) NOT NULL,
  `maker_fee` decimal(32, 4) NOT NULL,
  `deal_asset` decimal(30, 8) NOT NULL,
  `deal_money` decimal(30, 8) NOT NULL,
  `deal_fee` decimal(30, 8) NOT NULL,
  `create_at` decimal(32, 6) NOT NULL,
  `update_at` decimal(32, 6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_market`(`user_id`, `market`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order_history_template
-- ----------------------------

-- ----------------------------
-- Procedure structure for balance_history_template_process_function
-- ----------------------------
DROP PROCEDURE IF EXISTS `balance_history_template_process_function`;
delimiter ;;
CREATE PROCEDURE `balance_history_template_process_function`()
BEGIN#Routine body goes here...
	DECLARE
		i INT DEFAULT 0;
	WHILE
			i < 100 DO		
			SET @SQL = CONCAT(
				'CREATE TABLE  balance_history_', i ,' LIKE balance_history_template');			
		PREPARE stmt 
		FROM
			@SQL;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
		
		SET i = i + 1;
		
	END WHILE;
	
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for batchInsert
-- ----------------------------
DROP PROCEDURE IF EXISTS `batchInsert`;
delimiter ;;
CREATE PROCEDURE `batchInsert`()
BEGIN
	DECLARE
		i INT DEFAULT 0;
	START TRANSACTION;
	WHILE
			i < 100 DO
			SET @SQL = CONCAT(
				'insert into balance_history_template_',
				i,
					'(id,user_id,asset,business,business_id,`change`,balance,detail,time) VALUES(NULL,',
					i,
						'你好',
						'你好123',
						'22',
						'23.0',
						'21.00',
						'你好1231232',
						'1' ,')' 
					);
				PREPARE stmt 
				FROM
					@SQL;
				EXECUTE stmt;
				DEALLOCATE PREPARE stmt;
				
				SET i = i + 1;
				
			END WHILE;
			COMMIT;
		
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for batchoffest
-- ----------------------------
DROP PROCEDURE IF EXISTS `batchoffest`;
delimiter ;;
CREATE PROCEDURE `batchoffest`()
BEGIN
	DECLARE
		i INT DEFAULT 0;
	START TRANSACTION;
	WHILE
			i < 100 DO
					INSERT into balance_offest VALUES(i,0);
				
				SET i = i + 1;
				
			END WHILE;
			COMMIT;
		
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for insertnewfly
-- ----------------------------
DROP PROCEDURE IF EXISTS `insertnewfly`;
delimiter ;;
CREATE PROCEDURE `insertnewfly`()
BEGIN#Routine body goes here...
	DECLARE
		i INT DEFAULT 0;
	
			SET @SQL = CONCAT(
				'CREATE TABLE  newfly.t_exchange_order_history  LIKE order_history_template');			
		PREPARE stmt 
		FROM
			@SQL;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
		
	
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for order_detal_history_template_function
-- ----------------------------
DROP PROCEDURE IF EXISTS `order_detal_history_template_function`;
delimiter ;;
CREATE PROCEDURE `order_detal_history_template_function`()
BEGIN#Routine body goes here...
	DECLARE
		i INT DEFAULT 0;
	WHILE
			i < 100 DO		
			SET @SQL = CONCAT(
				'CREATE TABLE  order_deal_history_', i ,' LIKE order_deal_history_template');			
		PREPARE stmt 
		FROM
			@SQL;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
		
		SET i = i + 1;
		
	END WHILE;
	
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for order_history_template_function
-- ----------------------------
DROP PROCEDURE IF EXISTS `order_history_template_function`;
delimiter ;;
CREATE PROCEDURE `order_history_template_function`()
BEGIN#Routine body goes here...
	DECLARE
		i INT DEFAULT 0;
	WHILE
			i < 100 DO		
			SET @SQL = CONCAT(
				'CREATE TABLE  order_history_', i ,' LIKE order_history_template');			
		PREPARE stmt 
		FROM
			@SQL;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
		
		SET i = i + 1;
		
	END WHILE;
	
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for p_update_balance_history_template_10s_fix
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_update_balance_history_template_10s_fix`;
delimiter ;;
CREATE PROCEDURE `p_update_balance_history_template_10s_fix`()
BEGIN
	DECLARE
		i INT DEFAULT 0;
	DECLARE
		t_error INTEGER DEFAULT 0;
	START TRANSACTION;
	WHILE
			i < 100 DO
			
			SET @SQL = CONCAT( 'insert into newfly.t_exchange_balance_history
				(user_id,asset,business,business_id,`change`,balance,detail,time) select t1.user_id,t1.asset,t1.business,t1.business_id,t1.change 
			,t1.balance,t1.detail,t1.time from balance_history_template_', i, ' as t1 ,balance_offest b WHERE t1.id>b.offest and b.id=', i );
		
		SET @UPDATESQL = CONCAT( 'UPDATE balance_offest b SET b.offest=( SELECT id FROM balance_history_template_', i, ' ORDER BY id DESC LIMIT 0,1)' 'WHERE b.id=', i );
		


		PREPARE stmt 
		FROM
			@SQL;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
		PREPARE updatestat 
		FROM
			@UPDATESQL;
		EXECUTE updatestat;
		DEALLOCATE PREPARE updatestat;
		
		SET i = i + 1;
		
	END WHILE;
	IF
		t_error = 1 THEN
			ROLLBACK;
		ELSE COMMIT;
		
	END IF;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for p_update_balance_history_template_trigger
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_update_balance_history_template_trigger`;
delimiter ;;
CREATE PROCEDURE `p_update_balance_history_template_trigger`()
BEGIN
	DECLARE
		i INT DEFAULT 0;
	WHILE
			i < 100 DO
		
		SET @TRIGGER = CONCAT( "CREATE TRIGGER balance_history_template_", i, " AFTER INSERT ON balance_history_template_", i, " FOR EACH ROW ");

	
			PREPARE TRIGGERtemplate 
		FROM
			@TRIGGER;
		EXECUTE TRIGGERtemplate;
		DEALLOCATE PREPARE TRIGGERtemplate;
		
		SET i = i + 1;
		
	END WHILE;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for p_update_order_deal_history_template_10s_fix
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_update_order_deal_history_template_10s_fix`;
delimiter ;;
CREATE PROCEDURE `p_update_order_deal_history_template_10s_fix`()
BEGIN
	DECLARE
		i INT DEFAULT 0;
	DECLARE
		t_error INTEGER DEFAULT 0;
	START TRANSACTION;
	WHILE
			i < 100 DO
			
			SET @SQL = CONCAT( 'insert into newfly.t_exchange_user_order_deal_history
			(deal_id,market,asset,user_id,dealer_id,order_id,deal_order_id,price,amount,fee,deal_fee,role,side,time) SELECT t1.deal_id,t1.market,t1.asset,t1.user_id,t1.dealer_id,t1.order_id,t1.deal_order_id,t1.price,t1.amount,t1.fee,t1.deal_fee,t1.role,t1.side,t1.time from order_deal_history_template_', i, ' as t1 ,deal_offest b WHERE t1.id>b.offest and b.id=', i );
		
		SET @UPDATESQL = CONCAT( 'UPDATE deal_offest b SET b.offest=( SELECT id FROM order_deal_history_template_', i, ' ORDER BY id DESC LIMIT 0,1)' 'WHERE b.id=', i );
		PREPARE stmt 
		FROM
			@SQL;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
		PREPARE updatestat 
		FROM
			@UPDATESQL;
		EXECUTE updatestat;
		DEALLOCATE PREPARE updatestat;
		
		SET i = i + 1;
		
	END WHILE;
	IF
		t_error = 1 THEN
			ROLLBACK;
		ELSE COMMIT;
		
	END IF;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for p_update_order_history_template_10s_fix
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_update_order_history_template_10s_fix`;
delimiter ;;
CREATE PROCEDURE `p_update_order_history_template_10s_fix`()
BEGIN
	DECLARE
		i INT DEFAULT 0;
		DECLARE t_error INTEGER DEFAULT 0;  
		START TRANSACTION;
	WHILE
			i < 100 DO			
			SET @SQL = CONCAT( 'insert into newfly.t_exchange_user_order_history
				(type,side,user_id,market,source,price,amount,`left`,taker_fee,maker_fee,deal_asset,deal_money,deal_fee,create_at,update_at) SELECT t1.type,t1.side,t1.user_id,t1.market,t1.source,t1.price,t1.amount,t1.`left`,t1.taker_fee,t1.maker_fee,t1.deal_asset,t1.deal_money,t1.deal_fee,t1.create_at,t1.update_at from order_history_template_', i , ' as t1 ,order_offest b WHERE t1.id>b.offest and b.id=', i );   
	
			SET @UPDATESQL = CONCAT( 'UPDATE order_offest b SET b.offest=( SELECT id FROM order_history_template_', i  ,' ORDER BY id DESC LIMIT 0,1)' 'WHERE b.id=',i);  
			
		
			
		PREPARE stmt 
		FROM
			@SQL;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
		
		PREPARE updatestat 
		FROM
			@UPDATESQL;
		EXECUTE updatestat;
		DEALLOCATE PREPARE updatestat;
		
		SET i = i + 1;

		
	END WHILE;
			IF t_error = 1 THEN  
             ROLLBACK;  
        ELSE  
             COMMIT; 
END IF; 
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for user_deal_history_template_function
-- ----------------------------
DROP PROCEDURE IF EXISTS `user_deal_history_template_function`;
delimiter ;;
CREATE PROCEDURE `user_deal_history_template_function`()
BEGIN#Routine body goes here...
	DECLARE
		i INT DEFAULT 0;
	WHILE
			i < 100 DO		
			SET @SQL = CONCAT(
				'CREATE TABLE  user_deal_history_', i ,' LIKE user_deal_history_template');			
		PREPARE stmt 
		FROM
			@SQL;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
		
		SET i = i + 1;
		
	END WHILE;
	
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for user_order_history_template_function
-- ----------------------------
DROP PROCEDURE IF EXISTS `user_order_history_template_function`;
delimiter ;;
CREATE PROCEDURE `user_order_history_template_function`()
BEGIN#Routine body goes here...
	DECLARE
		i INT DEFAULT 0;
	WHILE
			i < 100 DO		
			SET @SQL = CONCAT(
				'CREATE TABLE  user_order_history_', i ,' LIKE user_order_history_template');			
		PREPARE stmt 
		FROM
			@SQL;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
		
		SET i = i + 1;
		
	END WHILE;
	
END
;;
delimiter ;

-- ----------------------------
-- Event structure for balance_history_template_event
-- ----------------------------
DROP EVENT IF EXISTS `balance_history_template_event`;
delimiter ;;
CREATE EVENT `balance_history_template_event`
ON SCHEDULE
EVERY '10' SECOND STARTS '2020-04-16 11:00:00'
ON COMPLETION PRESERVE
DO BEGIN 
CALL p_update_balance_history_template_10s_fix();
END
;;
delimiter ;

-- ----------------------------
-- Event structure for order_deal_history_template_event
-- ----------------------------
DROP EVENT IF EXISTS `order_deal_history_template_event`;
delimiter ;;
CREATE EVENT `order_deal_history_template_event`
ON SCHEDULE
EVERY '10' SECOND STARTS '2020-04-16 11:00:00'
ON COMPLETION PRESERVE
DO BEGIN 
CALL p_update_order_deal_history_template_10s_fix();
END
;;
delimiter ;

-- ----------------------------
-- Event structure for order_history_template_event
-- ----------------------------
DROP EVENT IF EXISTS `order_history_template_event`;
delimiter ;;
CREATE EVENT `order_history_template_event`
ON SCHEDULE
EVERY '10' SECOND STARTS '2020-04-16 11:00:00'
ON COMPLETION PRESERVE
DO BEGIN 
CALL p_update_order_history_template_10s_fix();
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
