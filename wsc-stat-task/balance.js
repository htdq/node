const config = require('./config.js');
const mysqlUtil = require('./mysql-util.js');
const log4js = require('log4js');

//每日数据汇总
log4js.configure({
    appenders: {
        today: {
            type: 'file',
            filename: 'logs/today/log.log',
            pattern: 'trace-yyyy-MM-dd.log',
            alwaysIncludePattern: true
        }
    },
    categories: { default: { appenders: ['today'], level: 'debug' } }
});

const logger = log4js.getLogger('today');

let mysql = null;
var yester = 1;

Date.prototype.Format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "q+": Math.floor((this.getMonth() + 3) / 3) //季度 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}


//数据统计到newfly db


async function savebalance(data){
    for(let n=0;n<data.length; n++){
        if(data[n].length!=0){
        let Object=JSON.parse(data[n]);
        try {
            mysql.startTransactionAsync();  
            // let offest_sql = `select offest from deal_offest where id=${n}`;
            // let offest_data=await (mysql.queryAsync(offest_sql));
            // let offestdb = getDataOffest(offest_data);

            let newfly_balance_sql = `SELECT id FROM balance_history_${n} ORDER BY id DESC LIMIT 0,1;`;
            let balance_data_max_id=await (mysql.queryAsync(newfly_balance_sql));
            let max_id = getDataOffest(balance_data_max_id);
            for(let i in Object){
                let balance_sql=`insert into newfly.t_exchange_balance_history
                (
                    user_id,
                    asset,
                    business,
                    business_id,
                    \`change\`,
                    balance,
                    detail,
                    time
                ) 
                values
                (
                  ${Object[i].user_id},${Object[i].asset},
                  ${Object[i].business},${Object[i].business_id},
                  ${Object[i].change},${Object[i].balance},
                  ${Object[i].detail},${Object[i].time}
                )`;
                await mysql.queryAsync(balance_sql); 
            }
    
            let balance_offest = ` UPDATE balance_offest b SET b.offest=${max_id} WHERE b.id=${n}`;  //更新offest
    
            await mysql.queryAsync(balance_offest);
            
            mysql.commitAsync;

        } catch (error) {
            mysql.rollbackAsync();  //事务回滚
           // mysql.closeAsync();  //释放连接
            logger.error(error);
        }
             
    }

}
}

async function balance_census(startTemp, endTemp) {
    let data = new Array;   //不能使用const修饰 
    let num = 100;
    for (let n = 0; n < num; n++) {
        let balance_sql=`select 
        t1.user_id,t1.asset,t1.business,t1.business_id,t1.change ,t1.balance,t1.detail,t1.time 
        from balance_history_${n} as t1 ,balance_offest b 
        WHERE t1.id>b.offest 
        and b.id=${n}`;
        let balance_data=await mysql.queryAsync(balance_sql);
        let datadb = getData(balance_data);
        data.push(datadb);
    }

    await savebalance(data);
    console.log("balance-data-------"+data);

}



async function orderdealsave(data){
    for (let n = 0; n < data.length; n++) {
        if (data[n].length != 0) {
            let Object = JSON.parse(data[n]);
            try {
                mysql.startTransactionAsync();
                let offest = `select offest from deal_offest where id=${n}`;
                let offestdb = getDataOffest(await (mysql.queryAsync(offest)));

                let order_sql = `SELECT id FROM order_deal_history_${n} ORDER BY id DESC LIMIT 0,1;`;
                let order_data= await (mysql.queryAsync(order_sql));
                let max_id = getDataOffest(order_data);

                for (let i in Object) {
                    let order_data_sql = `insert into newfly.t_exchange_deal_history
                    (
                    deal_id,
                    market,
                    asset,
                    money,
                    user_id,
                    dealer_id,
                    order_id,
                    deal_order_id,
                    price,
                    amount,
                    fee,
                    dealer_fee,
                    role,
                    side,
                    time
                    ) 
                    values
                    (
                    ${Object[i].deal_id},${Object[i].market},
                    ${Object[i].money},${Object[i].asset},
                    ${Object[i].user_id},${Object[i].dealer_id},
                    ${Object[i].order_id}, ${Object[i].deal_order_id},
                    ${Object[i].price}, ${Object[i].amount},
                    ${Object[i].fee},${Object[i].dealer_fee},
                    ${Object[i].role},${Object[i].side},
                    ${Object[i].time}
                    ) `;
                    //偏移量增加
                    if (max_id - offestdb > 0) {
                      await mysql.queryAsync(order_data_sql);                      
                    } //合并数据
    
                }
    
                let deal_offest = ` UPDATE deal_offest b SET b.offest=${max_id} WHERE b.id=${n}`;  //更新offest
                await mysql.queryAsync(deal_offest);   
                
            } catch (error) {
                mysql.rollbackAsync();
               // mysql.closeAsync();  //释放连接
                logger.error(error);
            }

            mysql.commitAsync();
        

        }
      
    }
}


/**
 * 订单详情 合并数据
 * @param {} startTemp 
 * @param {*} endTemp 
 */
async function order_deal_history_census(startTemp, endTemp) {
    let data = new Array;
    let num = 100;
    for (let n = 0; n < num; n++) {
        let order_sql=`SELECT t1.deal_id,t1.market,t1.asset,t1.money,t1.
        user_id,t1.dealer_id,t1.order_id,t1.deal_order_id,t1.price,t1.amount,t1.fee,
        t1.dealer_fee,t1.role,t1.side,t1.time 

        from order_deal_history_${n} as t1 ,deal_offest b 

        WHERE t1.id>b.offest and b.id=${n}`;
        let order_data=await(mysql.queryAsync(order_sql));
        let datadb = getData(order_data);
        data.push(datadb);
    }

   await orderdealsave(data);
    //{"deal_id":23,"market":"32","asset":"32","user_id":32,"dealer_id":32,"order_id":3232,"deal_order_id":32,"price":32,"amount":32,"fee":32,"deal_fee":32,"role":32,"side":32,"time":32}
    console.log("order-deal-data-----" + data);

}




async function ordersave(data){
    for (let n = 0; n < data.length; n++) {
        if (data[n].length != 0) {
            let Object = JSON.parse(data[n]);
            try {
                mysql.startTransactionAsync();
                let order_offest_sql = `select offest from order_offest where id=${n}`;
                let order_offest=await (mysql.queryAsync(order_offest_sql));
                let offestdb = getDataOffest(order_offest);
    
                let order_detail_max_id_sql = `SELECT id FROM order_history_${n} ORDER BY id DESC LIMIT 0,1`;
                let order_detail_max_id=await (mysql.queryAsync(order_detail_max_id_sql));
                let max_id = getDataOffest(order_detail_max_id);
                for (let i in Object) {
                    let balance_sql = `insert into newfly.t_exchange_order_history
                    (
                        type,
                        side,
                        user_id,
                        market,
                        source,
                        price,
                        amount,
                        \`left\`,
                        taker_fee,
                        maker_fee,
                        deal_asset,
                        deal_money,
                        deal_fee,
                        create_at,
                        update_at
                    ) 
                    values
                    (
                        ${Object[i].type},${Object[i].side},
                        ${Object[i].user_id},${Object[i].market},
                        ${Object[i].source},${Object[i].price},
                        ${Object[i].amount},${Object[i].left},
                        ${Object[i].taker_fee},${Object[i].maker_fee},
                        ${Object[i].deal_asset},${Object[i].deal_money},
                        ${Object[i].deal_fee},${Object[i].create_at},
                        ${Object[i].update_at}
                    ) `;
                    //偏移量增加
                    if (max_id - offestdb > 0) {
                        await mysql.queryAsync(balance_sql);
                    } //合并数据
    
                }
    
                let deal_offest = ` UPDATE order_offest b SET b.offest=${max_id} WHERE b.id=${n}`;  //更新offest
    
                await mysql.queryAsync(deal_offest); 

                mysql.commitAsync();
            } catch (error) {
                mysql.rollbackAsync();
               // mysql.closeAsync();  //释放连接
                logger.error(error);
                
            }
        

        }
     
    }
}

/**
 * 订单信息表 合并数据
 * @param {*} startTemp 
 * @param {*} endTemp 
 */
async function order_history_template(startTemp, endTemp) {
    let data = new Array;
    let num = 100;
    for (let n = 0; n < num; n++) {
        let order_detail_sql=`SELECT t1.type,t1.side,t1.user_id,t1.market,t1.source,t1.price,t1.amount,
        t1.left,t1.taker_fee,t1.maker_fee,t1.deal_asset,t1.deal_money,t1.deal_fee,t1.create_at,t1.update_at 
        from order_history_${n} as t1 ,order_offest b 
        WHERE t1.id>b.offest and b.id=${n}`;
        let order_detail_data=await mysql.queryAsync(order_detail_sql);
        let datadb = getData(order_detail_data);
        data.push(datadb);
    }
            
    await ordersave(data);
    
    //{"type":2,"side":2,"user_id":2,"market":"2","source":"2","price":2,"amount":2,"left":1,"taker_fee":2,"maker_fee":2,"deal_asset":2,"deal_money":2,"deal_fee":2,"create_at":2,"update_at":2}


    console.log("order-history_data-----" + data);

}



function getData(data) {
    return JSON.stringify(data);
}

function getDataOffest(data) {
    if (data.length == 0) return 0;
    return data instanceof Array ? Object.values(data[0])[0] : Object.values(data)[0]
}

async function startService() {
    logger.info("service staring...");
    try {
        mysql = await mysqlUtil.connectAsync(config.dbOption);
    } catch (e) {
        logger.error("mysql conn failed." + e);
    }

    if (mysql == null) {
        logger.error("mysql conn failed.");
        return;
    }

    let flag = false;
    let table = {};
    while (true) {
        let day2 = new Date();
        let hour = day2.getHours();//得到小时
        let minu = day2.getMinutes();//得到分钟

        //let CURDATE = new Date().Format("yyyy-MM-dd ");

        var start = new Date();
        var startTemp = start.getFullYear() + "-" + (start.getMonth() + 1) + "-" + start.getDate();
        let secondes = start.getSeconds();
        var dateTemp = start;
        dateTemp.setDate(dateTemp.getDate() + 1);
        var endTemp = dateTemp.getFullYear() + "-" + (dateTemp.getMonth() + 1) + "-" + dateTemp.getDate();
        console.log(startTemp+"-"+secondes);
        console.log(secondes);
        if (secondes % 10 == 0) {
            logger.info("start load data to newfly  database  .. " + startTemp + endTemp);
            await balance_census(startTemp, endTemp);
            await order_deal_history_census(start, endTemp);
            await order_history_template(start, endTemp);
        }
        await new Promise((resolve, reject) => { setTimeout(resolve, 1000); });
    }
}

startService();