const log4js = require('log4js');
const https = require('./https-util.js');
//引入es
const es = require('elasticsearch');
//email邮件
const nodemailer = require('nodemailer');
//smtp服务
const smtpTransport = require('nodemailer-smtp-transport');


//每日数据汇总
log4js.configure({
    appenders: {
        today: {
            type: 'file',
            filename: 'logs/today/log.log',
            pattern: 'trace-yyyy-MM-dd.log',
            alwaysIncludePattern: true
        }
    },
    categories: { default: { appenders: ['today'], level: 'debug' } }
});

const logger = log4js.getLogger('today');

Date.prototype.Format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "q+": Math.floor((this.getMonth() + 3) / 3) //季度 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}



let transport = nodemailer.createTransport(smtpTransport({
    host: "smtp.qq.com", // 主机
    secure: true, // 使用 SSL
    secureConnection: true, // 使用 SSL
    port: 465, // SMTP 端口
    auth: {
        user: "1947832562@qq.com", // 账号
        pass: "" // 密码 邮件smtp服务的密码(不是QQ登录密码)
    }
}));

// 设置邮件内容
let mailOptions = {
    from: "1947832562@qq.com", // 发件地址
    to: "1947832562@qq.com", // 收件列表
    subject: "服务器异常", // 标题
    text: "",
    html: "<b>thanks a for visiting!</b> 世界，你好！" // html 内容
}

//连接es
//
let url = "https://api.telegram.org/bot1227313949:AAHLVEnxBk6zvacMN2p1RzqNRJTsVKq53uk/sendMessage";
async function getES(searchInfo) {
    const esClient = await es.Client({
        host: 'http://192.168.0.198:9200',
        log: 'error'
    });
    let logobj = {};
    try {
        let resp = await esClient.search(searchInfo);
        let result = resp.hits.hits; //结果
        for (var i = 0; i < result.length; i++) {
            logobj.index = result[i]._index;  //索引
            logobj.type = result[i]._type //类型
            logobj.id = result[i]._id;  //id
            logobj.source = result[i]._source; //log信息
            logobj.socre = result[i]._socre == 0 ? 0 : result[i]._socre;
            let loginfo = logobj.source.message;
            if (loginfo != undefined) {
                let logmsg = JSON.parse(loginfo);
                if (logmsg.level == "ERROR") {
                    console.log("error");
                    //发送telegram通知
                    //chat_id=-462472280&text=wanshaobo
                    // let data= await https.getAsync(url,{chat_id:"-462472280",text:"hello world"}); 

                    // 发送邮件
                    // mailOptions.html=logmsg.message;      
                    // try {
                    //     let resp=await transport.sendMail(mailOptions); 
                    // } catch (error) {
                    //     console.log(error);
                    //     log.error(error);
                    // }finally{
                    //     transport.close(); // 如果没用，关闭连接池
                    // }

                    transport.sendMail(mailOptions,function(error,resp){
                        if(error){
                            console.error(error);
                        }else{
                            console.log(resp);
                        }
                        transport.close();
                    });

                }
            }
        }
    } catch (error) {
        console.trace(error.message);

    }
}




async function getEsData(from, size) {
    //设置过滤条件
    var search =
    {
        index: 'elk', //日志索引
        type: '_doc',
        body: {
            query: {
                "bool": {
                    "must": [],
                    "filter": [
                        {
                            "match_all": {}
                        }
                    ],
                    "should": [],
                    "must_not": []
                }
            },
            "from": from,
            "size": size,
            "sort": [],
            "aggs": {}
        }
    };
    let data = await getES(search);
    //  console.log(data);
}



async function startService() {
    let startsize = 0;
    let begin = true;
    while (true) {
        var start = new Date();
        var startTemp = start.getFullYear() + "-" + (start.getMonth() + 1) + "-" + start.getDate();
        let secondes = start.getSeconds();
        var dateTemp = start;
        dateTemp.setDate(dateTemp.getDate() + 1);
        var endTemp = dateTemp.getFullYear() + "-" + (dateTemp.getMonth() + 1) + "-" + dateTemp.getDate();
        console.log(startTemp + "-" + secondes);
        console.log(secondes);

        if (secondes % 5 == 0) {
            logger.info("start load data to es  .. " + startTemp + endTemp);
            if (begin) {
                startsize = startsize; //0
                begin = false;
            } else {
                startsize = startsize + 10;
            }
            await getEsData(startsize, 10);

        }
        await new Promise((resolve, reject) => { setTimeout(resolve, 1000); });
    }
}

startService();

