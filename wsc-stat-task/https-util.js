const https = require('https');
const querystring = require('querystring');
const URL = require('url');

let get = function (url, query) {
	  let _url = URL.parse(url);
	  console.log(_url);
	let content = null;
	if(query!=null) {
		content = querystring.stringify(query);  
	}
	let path = _url.pathname;
	if(content!=null) {
		path = path + '?' + content;
		console.log(path);
	}
	
	let options = {
		hostname: _url.host,
		port: _url.port,  
		path: path ,  
		method: 'GET',
		headers: {
			'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
			'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:70.0) Gecko/20100101 Firefox/70.0'
		} 
	};
	return new Promise(function (resolve, reject) {
		let req = https.request(options, res=> {  
			let ret = {
				statusCode: res.statusCode,
				headers: res.headers,
				data: null
			};
			res.setEncoding('utf8'); 
			let dd = ''
			res.on('data', chunk=> { 
				dd += chunk
			});
			res.on('end', secCheck => {
				//console.log('ret: ' + JSON.stringify(ret));
				//console.log('end: ' + dd);		
				resolve(JSON.parse(dd))
			})
		});
  
		req.on('error', e => {  
			reject(e)
		});
  
		req.end() 
	})
};

let post = function (url, data) {
	let _url = URL.parse(url);
	
	let options = {
		hostname: _url.host,
		port: _url.port,  
		path:  _url.path,
		method: 'POST',
		headers: {
			'Accept':'text/html,application/xhtml+xml,application/xml,application/json;q=0.9,*/*;q=0.8',
			'Content-Type': 'application/json; charset=UTF-8',
			'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:70.0) Gecko/20100101 Firefox/70.0'
		}
	};
	return new Promise(function (resolve, reject) {
		let timeoutEventId = null;
		let req = https.request(options, function (res) {
			let ret = {
				statusCode: res.statusCode,
				headers: res.headers,
				data: null
			};
		
			res.setEncoding('utf8');
			let dd = '';
			res.on('data', function (chunk) { 
				dd += chunk
			});
			res.on('end', secCheck => {
				clearTimeout(timeoutEventId);
				try{
					//console.log('ret: ' + JSON.stringify(ret));
					//console.log('end: ' + dd);
					let result = JSON.parse(dd);
					resolve(result)
				}catch(e){
					reject(e)
				}
			});
			res.on('close',function(){
        		clearTimeout(timeoutEventId)
      		});
		});
		req.on('error', function (e) {
			clearTimeout(timeoutEventId)
			reject(e)
		});
		req.on("timeout", function() {
			req.abort();
			reject("timeout")
		});
		timeoutEventId = setTimeout(function(){
       		req.emit('timeout', {message:'have been timeout...'});
    	},10000);
		
		let content = JSON.stringify(data);
		req.write(content);
		req.end()
	})
};

module.exports = {
	getAsync: get,
	postAsync: post
};