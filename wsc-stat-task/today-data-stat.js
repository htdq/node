//每日数据汇总
const config = require('./config.js');
const mysqlUtil = require('./mysql-util.js');
const log4js = require('log4js');

log4js.configure({
    appenders: {
        today: {
            type: 'file',
            filename: 'logs/today/log.log',
            pattern: 'trace-yyyy-MM-dd.log',
            alwaysIncludePattern: true
        }
    },
    categories: { default: { appenders: ['today'], level: 'debug' } }
});

const logger = log4js.getLogger('today');

let mysql = null;
var yester = 1;

var todayDataSum = {
    "today_new_user": 0,
    "total_user": 0,
    "added_today_user": 0,
    "all_valid_user": 0,

    "added_today_mill_max": 0,
    "added_today_mill_middle": 0,
    "added_today_mill_min": 0,

    "redeem_mill_today_max": 0,
    "redeem_mill_today_middle": 0,
    "redeem_mill_today_min": 0,

    "due_toady_mill_max": 0,
    "due_toady_mill_middle": 0,
    "due_toady_mill_min": 0,


    "today_revenue_sharing": 0,
    "today_community_benefits": 0,
    "today_peer_income": 0,
    "today_node_income": 0,
    "today_dividend_income": 0,
    "today_very_poor_community": 0,
    "today_node_range": 0,
    "today_supernode": 0,

    "v0": 0,
    "v1": 0,
    "v2": 0,
    "v3": 0,
    "v4": 0,
    "v5": 0,



    "today_dividend_income": 0,
    "today_very_poor_community": 0,
    "today_node_range": 0,
    "today_supernode": 0,

    "eth_usd_today_eth": 0,
    "eth_usd_today_usd": 0,

    "wsc_usd_today_wsc": 0,
    "wsc_usd_today_usd": 0,

    "wsc_eth_today_wsc": 0,
    "wsc_eth_today_eth": 0,

    "usd_eth_today_usd": 0,
    "usd_eth_today_eth": 0,

    "recharge_eth_today": 0,
    "withdraw_eth_today": 0,
    "today_mining_income": 0,
    "today_destroy": 0
}

Date.prototype.Format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "q+": Math.floor((this.getMonth() + 3) / 3) //季度 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

//今日矿机 
async function todayMillData(startTemp, endTemp) {
    //今日新增大型矿机
    todayDataSum.added_today_mill_max = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from mill_order  
        where mill_code = '大型矿机' and pay_status=1 and create_time >= '${startTemp}' and create_time < '${endTemp}' `));

    //今日新增中型矿机
    todayDataSum.added_today_mill_middle = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from mill_order 
         where mill_code = '中型矿机' and pay_status=1 and create_time  >= '${startTemp}' and create_time < '${endTemp}' `));

    //今日新增小型矿机
    todayDataSum.added_today_mill_min = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from mill_order 
         where mill_code = '小型矿机' and pay_status=1 and create_time  >= '${startTemp}' and create_time < '${endTemp}' `));


    //今日赎回大型矿机数量
    todayDataSum.redeem_mill_today_max = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from mill_recycle_order 
         where money = 5000 and create_time >= '${startTemp}' and create_time < '${endTemp}'  and 'status' =1 `));

    //今日赎回中型矿机数量
    todayDataSum.redeem_mill_today_middle = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from mill_recycle_order 
         where money = 1000 and create_time >= '${startTemp}' and create_time < '${endTemp}'  and 'status' =1 `));

    //今日赎回小型矿机数量
    todayDataSum.redeem_mill_today_min = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from mill_recycle_order 
         where money = 500 and create_time >= '${startTemp}' and create_time < '${endTemp}'  and 'status' =1 `));


    //今日到期大型矿机数量
    todayDataSum.due_toady_mill_max = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from my_mill 
         where mill_code  = '大型矿机' and end_time >=  '${startTemp}' and end_time < DATE_SUB('${startTemp}',INTERVAL ${yester} DAY)  and 'status'=0`));

    //今日到期中型矿机数量
    todayDataSum.due_toady_mill_middle = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from my_mill 
         where mill_code = '中型矿机' and end_time >=  '${startTemp}' and end_time < DATE_SUB('${startTemp}',INTERVAL ${yester} DAY)  and 'status'=0`));

    // 今日到期小型矿机数量
    todayDataSum.due_toady_mill_min = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from my_mill 
         where mill_code = '小型矿机' and end_time >=  '${startTemp}' and  end_time < DATE_SUB('${startTemp}',INTERVAL ${yester} DAY)  and 'status'=0 `));
}

//今日累计新增：矿机 
async function sel_sql_wsc_mill_statistics_summary(startTemp, endTemp) {

    await todayMillData(startTemp, endTemp);

    logger.info("sel_sql_wsc_mill_statistics_summary exec end.");

    //累计新增大型矿机
    let added_mill_total_max = getData(await mysql.queryAsync(`
    SELECT IFNULL(added_mill_total_max,0) FROM wsc_mill_statistics_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc limit 1`)) + todayDataSum.added_today_mill_max;

    //累计新增中型矿机
    let added_mill_total_middle = getData(await mysql.queryAsync(`
    SELECT IFNULL(added_mill_total_middle,0) FROM wsc_mill_statistics_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc limit 1`)) + todayDataSum.added_today_mill_middle;

    //累计新增小型矿机
    let added_mill_total_min = getData(await mysql.queryAsync(`
    SELECT IFNULL(added_mill_total_min,0) FROM wsc_mill_statistics_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc limit 1`)) + todayDataSum.added_today_mill_min;


    //累计赎回大型矿机数量
    let redeem_mill_total_max = getData(await mysql.queryAsync(`
    select IFNULL(redeem_mill_total_max,0) FROM wsc_mill_statistics_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc limit 1`)) + todayDataSum.redeem_mill_today_max;

    //累计赎回中型矿机数量
    let redeem_mill_total_middle = getData(await mysql.queryAsync(`
    select IFNULL(redeem_mill_total_middle,0) FROM wsc_mill_statistics_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc  limit 1`)) + todayDataSum.redeem_mill_today_middle;

    //累计赎回小型矿机数量
    let redeem_mill_total_min = getData(await mysql.queryAsync(`
    select IFNULL(redeem_mill_total_min,0) FROM wsc_mill_statistics_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc  limit 1`)) + todayDataSum.redeem_mill_today_min;

    //累计到期大型矿机数量
    let due_mill_total_max = getData(await mysql.queryAsync(`
    select IFNULL(due_mill_total_max,0) FROM wsc_mill_statistics_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc  limit 1`)) + todayDataSum.due_toady_mill_max;

    //累计到期中型矿机数量
    let due_mill_total_middle = getData(await mysql.queryAsync(`
    select IFNULL(due_mill_total_middle,0) FROM wsc_mill_statistics_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc  limit 1`)) + todayDataSum.due_toady_mill_middle;

    // 累计到期小型矿机数量
    let due_mill_total_min = getData(await mysql.queryAsync(`
    select IFNULL(due_mill_total_min,0) FROM wsc_mill_statistics_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc  limit 1`)) + todayDataSum.due_toady_mill_min;

    let count = getData(await mysql.queryAsync(
        `select IFNULL(count(added_today_mill_max),0) from wsc_mill_statistics_summary
         where clear_time = '${startTemp}' `));

    if (count == 0) {
        let sql_wsc_mill_statistics_summary = `insert into wsc_mill_statistics_summary
        (
            added_today_mill_max,
            added_today_mill_middle,
            added_today_mill_min,
    
            added_mill_total_max,
            added_mill_total_middle,
            added_mill_total_min,
    
            redeem_mill_today_max,
            redeem_mill_today_middle,
            redeem_mill_today_min,
    
            redeem_mill_total_max,
            redeem_mill_total_middle,
            redeem_mill_total_min,
    
            due_toady_mill_max,
            due_toady_mill_middle,
            due_toady_mill_min,
    
            due_mill_total_max,
            due_mill_total_middle,
            due_mill_total_min ,
            clear_time
           ) 
        VALUES (
            ${todayDataSum.added_today_mill_max},
            ${todayDataSum.added_today_mill_middle},
            ${todayDataSum.added_today_mill_min},
    
            ${added_mill_total_max},
            ${added_mill_total_middle},
            ${added_mill_total_min},
    
            ${todayDataSum.redeem_mill_today_max},
            ${todayDataSum.redeem_mill_today_middle},
            ${todayDataSum.redeem_mill_today_min},
    
            ${redeem_mill_total_max},
            ${redeem_mill_total_middle},
            ${redeem_mill_total_min},
    
            ${todayDataSum.due_toady_mill_max},
            ${todayDataSum.due_toady_mill_middle},
            ${todayDataSum.due_toady_mill_min},
    
            ${due_mill_total_max},
            ${due_mill_total_middle},
            ${due_mill_total_min},
            '${startTemp}'
            )`;
        await mysql.queryAsync(sql_wsc_mill_statistics_summary);
        logger.info("sel_sql_wsc_mill_statistics_summary exit end.");
    } else {
        let upd_wsc_mill_statistics_summary =
            `UPDATE wsc_mill_statistics_summary m1
    set 
        m1.added_today_mill_max = ${todayDataSum.added_today_mill_max},
        m1.added_today_mill_middle = ${todayDataSum.added_today_mill_middle},
        m1.added_today_mill_min = ${todayDataSum.added_today_mill_min},

        m1.added_mill_total_max  =   ${added_mill_total_max},
        m1.added_mill_total_middle = ${added_mill_total_middle},
        m1.added_mill_total_min =  ${added_mill_total_min}, 

        m1.redeem_mill_today_max = ${todayDataSum.redeem_mill_today_max},
        m1.redeem_mill_today_middle = ${todayDataSum.redeem_mill_today_middle}, 
        m1.redeem_mill_today_min  = ${todayDataSum.redeem_mill_today_min}, 

        m1.redeem_mill_total_max = ${redeem_mill_total_max},
        m1.redeem_mill_total_middle =${redeem_mill_total_middle}, 
        m1.redeem_mill_total_min = ${redeem_mill_total_min},

        m1.due_toady_mill_max =  ${todayDataSum.due_toady_mill_max},
        m1.due_toady_mill_middle = ${todayDataSum.due_toady_mill_middle}, 
        m1.due_toady_mill_min =${todayDataSum.due_toady_mill_min},

        m1.due_mill_total_max =  ${due_mill_total_max},
        m1.due_mill_total_middle= ${due_mill_total_middle},
        m1.due_mill_total_min =  ${due_mill_total_min},
        m1.clear_time ='${startTemp}'
        WHERE m1.clear_time >= '${startTemp}' `
        await mysql.queryAsync(upd_wsc_mill_statistics_summary);
        logger.info("upd_sql_wsc_mill_statistics_summary exit end.");
    }
}


//今日USD收益 
async function todayImcomeData(startTemp, endTemp) {

    //今日分享收益
    todayDataSum.today_revenue_sharing = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
            where in_type = 1 and is_cal = 0 and creat_time >='${startTemp}' and creat_time < '${endTemp}' `));

    //今日社区收益
    todayDataSum.today_community_benefits = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
            where in_type = 2 and is_cal = 0 and creat_time >='${startTemp}' and creat_time < '${endTemp}' `));

    //今日平级收益
    todayDataSum.today_peer_income = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records
            where in_type = 2 and is_cal = 2 and creat_time >='${startTemp}' and creat_time < '${endTemp}' `));

    //今日节点收益
    todayDataSum.today_node_income = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
            where in_type = 5 and is_cal = 0 and creat_time >='${startTemp}' and creat_time < '${endTemp}' `));

    //今日分红收益
    todayDataSum.today_dividend_income = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
            where in_type = 3 and is_cal = 0 and creat_time >='${startTemp}' and creat_time < '${endTemp}' `));

    //今日社区极差
    todayDataSum.today_very_poor_community = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
            where in_type = 2 and is_cal = 1 and creat_time >='${startTemp}' and creat_time < '${endTemp}' `));

    //今日节点极差
    todayDataSum.today_node_range = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
            where in_type = 5 and is_cal = 1 and creat_time >='${startTemp}' and creat_time < '${endTemp}' `));

    //今日超级节点
    todayDataSum.today_supernode = getData(await mysql.queryAsync(
        `select IFNULL(sum(usd_num),0) from fvirtualwallet_swc_records 
            where in_type = 4 and is_cal = 0 and creat_time >='${startTemp}' and creat_time < '${endTemp}' `));
}

//今日累计新增：USD 
async function sel_sql_wsc_usd_income_summary(startTemp, endTemp) {
    logger.info("sel_sql_wsc_wsc_income_summary exec end.");

    await todayImcomeData(startTemp, endTemp);

    //今日累计分享收益
    let total_revenue_sharing = getData(await mysql.queryAsync(`
    SELECT IFNULL(sum(total_revenue_sharing),0) FROM wsc_income_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by clear_time limit 1`)) + todayDataSum.today_revenue_sharing;

    //今日累计社区收益
    let total_community_benefits = getData(await mysql.queryAsync(`
    SELECT IFNULL(sum(total_community_benefits),0) FROM wsc_income_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}'order by clear_time limit 1`)) + todayDataSum.today_community_benefits;

    //今日累计平级收益
    let total_peer_income = getData(await mysql.queryAsync(`
    SELECT IFNULL(sum(total_peer_income),0) FROM wsc_income_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}'order by clear_time limit 1`)) + todayDataSum.today_peer_income;

    //今日累计节点收益
    let total_node_income = getData(await mysql.queryAsync(`
    SELECT IFNULL(sum(total_node_income),0) FROM wsc_income_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}'order by clear_time limit 1`)) + todayDataSum.today_node_income;

    //今日累计分红收益
    let total_dividend_income = getData(await mysql.queryAsync(`
    SELECT IFNULL(sum(total_dividend_income),0) FROM wsc_income_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}'order by clear_time limit 1`)) + todayDataSum.today_dividend_income;

    //今日累计社区极差
    let total_very_poor_community = getData(await mysql.queryAsync(`
    SELECT IFNULL(sum(total_very_poor_community),0) FROM wsc_income_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}'order by clear_time limit 1`)) + todayDataSum.today_very_poor_community;

    //今日累计节点极差
    let total_node_range = getData(await mysql.queryAsync(`
    SELECT IFNULL(sum(total_node_range),0) FROM wsc_income_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}'order by clear_time limit 1`)) + todayDataSum.today_node_range;

    //今日累计超级节点
    let total_supernode = getData(await mysql.queryAsync(`
    SELECT IFNULL(sum(total_supernode),0) FROM wsc_income_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}'order by clear_time limit 1`)) + todayDataSum.today_supernode;

    //判断今日是否有数据
    let count = getData(await mysql.queryAsync(
        `select IFNULL(count(today_community_benefits),0) from wsc_income_summary 
             where clear_time ='${startTemp}'`));

    if (count == 0) {
        sql_wsc_wsc_income_summary = `insert into wsc_income_summary
            (today_revenue_sharing,
                today_community_benefits,
                today_peer_income,
                today_node_income,
                today_dividend_income,
                today_very_poor_community,
                today_node_range,
                today_supernode,

                total_revenue_sharing,
                total_community_benefits,
                total_peer_income,
                total_node_income,

                total_dividend_income,
                total_very_poor_community,
                total_node_range,
                total_supernode,
                clear_time
            )
        VALUES(
        ${todayDataSum.today_revenue_sharing},
        ${todayDataSum.today_community_benefits},
        ${todayDataSum.today_peer_income},
        ${todayDataSum.today_node_income},
        ${todayDataSum.today_dividend_income},
        ${todayDataSum.today_very_poor_community},
        ${todayDataSum.today_node_range},
        ${todayDataSum.today_supernode},

        ${total_revenue_sharing},
        ${total_community_benefits},
        ${total_peer_income},
        ${total_node_income},

        ${total_dividend_income},
        ${total_very_poor_community},
        ${total_node_range},
        ${total_supernode},
        '${startTemp}')`;
        await mysql.queryAsync(sql_wsc_wsc_income_summary);
        logger.info("sel_sql_wsc_wsc_income_summary exit end.");
    } else {
        let upd_wsc_mill_statistics_summary =
            `UPDATE wsc_income_summary is1 set
        is1.today_revenue_sharing =   ${todayDataSum.today_revenue_sharing},
        is1.today_community_benefits = ${todayDataSum.today_community_benefits},
        is1.today_peer_income =  ${todayDataSum.today_peer_income},
        is1.today_node_income  =   ${todayDataSum.today_node_income},
        is1.today_dividend_income = ${todayDataSum.today_dividend_income},
        is1.today_very_poor_community =   ${todayDataSum.today_very_poor_community},
        is1.today_node_range = ${todayDataSum.today_node_range},
        is1.today_supernode = ${todayDataSum.today_supernode}, 

        is1.total_revenue_sharing =${total_revenue_sharing},
        is1.total_community_benefits = ${total_community_benefits},
        is1.total_peer_income  = ${total_peer_income},
        is1.total_node_income =  ${total_node_income},
        is1.total_dividend_income = ${total_dividend_income},
        is1.total_very_poor_community = ${total_very_poor_community},
        is1.total_node_range = ${total_node_range},
        is1.total_supernode = ${total_supernode},
        is1.clear_time = '${startTemp}'
        WHERE is1.clear_time = '${startTemp}'`
        await mysql.queryAsync(upd_wsc_mill_statistics_summary);
        logger.info("upd_sql_wsc_wsc_income_summary exit end.");
    }
}


//今日会员，转账，ETH，WSC 
async function todayStatData(startTemp, endTemp) {
    //今日新增用户数量
    todayDataSum.today_new_user = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from fuser 
            where fRegisterTime >='${startTemp}' and fRegisterTime < '${endTemp}' `));

    //全站用户数量
    todayDataSum.total_user = getData(await mysql.queryAsync(
        `select IFNULL(count(*),0) from fuser 
            where fRegisterTime < '${startTemp}'`));

    //今日新增有效用户数量
    todayDataSum.added_today_user = getData(await mysql.queryAsync(
        `SELECT IFNULL(count(*),0) 
            from (
                select fId,u.fLoginName,IFNULL(count(*),0),m.status 
                from fuser u inner JOIN my_mill m on u.fLoginName = m.fLoginName 
                where m.status!=0 and u.fRegisterTime >='${startTemp}' and u.fRegisterTime < '${endTemp}' GROUP BY u.fId 
                ) a`));

    //全站有效用户数量
    todayDataSum.all_valid_user = getData(await mysql.queryAsync(
        `SELECT IFNULL(count(*),0) from (
                select fId,u.fLoginName,IFNULL(count(*),0),m.status 
                from fuser u inner JOIN my_mill m on u.fLoginName = m.fLoginName 
                where m.status!=0 and u.fRegisterTime < '${startTemp}'   GROUP BY u.fId 
                ) a `));

    //V0
    todayDataSum.v0 = getData(await mysql.queryAsync(
        `SELECT IFNULL(count(*),0) 
                from (
                    select fId,u.fLoginName,IFNULL(count(*),0),m.status 
                    from fuser u inner JOIN my_mill m on u.fLoginName = m.fLoginName 
                    where m.status!=0 and u.fRegisterTime < '${startTemp}' GROUP BY u.fId 
                    ) 
                    a INNER JOIN fvirtualwallet_swc_result r on a.fId=r.fuserId 
                    where r.vip_level = 0`));
    //V1
    todayDataSum.v1 = getData(await mysql.queryAsync(
        `SELECT IFNULL(count(*),0) 
            from (
                select fId,u.fLoginName,IFNULL(count(*),0),m.status 
                from fuser u inner JOIN my_mill m on u.fLoginName = m.fLoginName 
                where m.status!=0 and u.fRegisterTime < '${startTemp}' GROUP BY u.fId 
                ) 
                a INNER JOIN fvirtualwallet_swc_result r on a.fId=r.fuserId 
                where r.vip_level = 1`));
    //V2
    todayDataSum.v2 = getData(await mysql.queryAsync(
        `SELECT IFNULL(count(*),0) 
            from (
                select fId,u.fLoginName,IFNULL(count(*),0),m.status 
                from fuser u inner JOIN my_mill m on u.fLoginName = m.fLoginName 
                where m.status!=0 and u.fRegisterTime < '${startTemp}' GROUP BY u.fId 
                ) 
                a INNER JOIN fvirtualwallet_swc_result r on a.fId=r.fuserId 
                where r.vip_level = 2`));
    //V3
    todayDataSum.v3 = getData(await mysql.queryAsync(
        `SELECT IFNULL(count(*),0) 
            from (
                select fId,u.fLoginName,IFNULL(count(*),0),m.status 
                from fuser u inner JOIN my_mill m on u.fLoginName = m.fLoginName 
                where m.status!=0 and u.fRegisterTime < '${startTemp}' GROUP BY u.fId 
                ) 
                a INNER JOIN fvirtualwallet_swc_result r on a.fId=r.fuserId 
                where r.vip_level = 3`));
    //V4
    todayDataSum.v4 = getData(await mysql.queryAsync(
        `SELECT IFNULL(count(*),0) 
            from (
                select fId,u.fLoginName,IFNULL(count(*),0),m.status 
                from fuser u inner JOIN my_mill m on u.fLoginName = m.fLoginName 
                where m.status!=0 and u.fRegisterTime < '${startTemp}' GROUP BY u.fId 
                ) 
                a INNER JOIN fvirtualwallet_swc_result r on a.fId=r.fuserId 
                where r.vip_level = 4`));
    //V5
    todayDataSum.v5 = getData(await mysql.queryAsync(
        `SELECT IFNULL(count(*),0) 
            from (
                select fId,u.fLoginName,IFNULL(count(*),0),m.status 
                from fuser u inner JOIN my_mill m on u.fLoginName = m.fLoginName 
                where m.status!=0 and u.fRegisterTime < '${startTemp}' GROUP BY u.fId 
                ) 
                a INNER JOIN fvirtualwallet_swc_result r on a.fId=r.fuserId 
                where r.vip_level = 5`));

    //今日ETH兑换USD兑出ETH
    todayDataSum.eth_usd_today_eth = getData(await mysql.queryAsync(
        `select IFNULL(sum(outerAmout),0) from c2c_order  
            where outerCurrencyId = 4 and innerCurrencyId = 6 and orderStatus = 2 and createDate >='${startTemp}' and createDate <'${endTemp}'`));

    //今日ETH兑换USD兑入USD
    todayDataSum.eth_usd_today_usd = getData(await mysql.queryAsync(
        `select IFNULL(sum(innerAmout),0) from c2c_order  
            where outerCurrencyId = 4 and innerCurrencyId = 6 and orderStatus = 2 and createDate >='${startTemp}' and createDate <'${endTemp}'`));

    //今日WSC兑换USD兑出WSC
    todayDataSum.wsc_usd_today_wsc = getData(await mysql.queryAsync(
        `select IFNULL(sum(outerAmout),0) from c2c_order  
            where outerCurrencyId = 58 and innerCurrencyId = 6 and orderStatus = 2 and createDate >='${startTemp}' and createDate <'${endTemp}'`));

    //今日WSC兑换USD兑入USD
    todayDataSum.wsc_usd_today_usd = getData(await mysql.queryAsync(
        `select IFNULL(sum(innerAmout),0)  from c2c_order  
            where outerCurrencyId = 58 and innerCurrencyId = 6 and orderStatus = 2 and createDate >='${startTemp}' and createDate <'${endTemp}'`));

    //今日WSC兑换ETH兑出WSC
    todayDataSum.wsc_eth_today_wsc = getData(await mysql.queryAsync(
        `select IFNULL(sum(outerAmout),0) from c2c_order  
            where outerCurrencyId = 58 and innerCurrencyId = 4 and orderStatus = 2 and createDate >='${startTemp}' and createDate <'${endTemp}'`));

    //今日WSC兑换ETH兑入ETH
    todayDataSum.wsc_eth_today_eth = getData(await mysql.queryAsync(
        `select IFNULL(sum(innerAmout),0)  from c2c_order  
            where outerCurrencyId = 58 and innerCurrencyId = 4 and orderStatus = 2 and createDate >='${startTemp}' and createDate <'${endTemp}'`));

    //今日USD兑换ETH兑出USD
    todayDataSum.usd_eth_today_usd = getData(await mysql.queryAsync(
        `select IFNULL(sum(outerAmout),0) from c2c_order  
            where outerCurrencyId = 6 and innerCurrencyId = 4 and orderStatus = 2 and createDate >='${startTemp}' and createDate <'${endTemp}'`));

    //今日USD兑换ETH兑入ETH
    todayDataSum.usd_eth_today_eth = getData(await mysql.queryAsync(
        `select IFNULL(sum(innerAmout),0) from c2c_order  
            where outerCurrencyId = 6 and innerCurrencyId = 4 and orderStatus = 2 and createDate >='${startTemp}' and createDate <'${endTemp}'`));

    //今日充值ETH数量
    todayDataSum.recharge_eth_today = getData(await mysql.queryAsync(
        `select IFNULL(sum(fAmount),0) from fvirtualcaptualoperation 
            where fVi_fId2 = 4 and fType = 1 and fStatus = 3 and fCreateTime >='${startTemp}' and fCreateTime < '${endTemp}'`));

    //今日提现ETH数量
    todayDataSum.withdraw_eth_today = getData(await mysql.queryAsync(
        `select IFNULL(sum(fAmount),0) from fvirtualcaptualoperation 
            where fVi_fId2 = 4 and fType = 2 and fStatus = 3 and fCreateTime >='${startTemp}' and fCreateTime < '${endTemp}'`));

    //今日挖矿收益wsc
    todayDataSum.today_mining_income = getData(await mysql.queryAsync(`select IFNULL(sum(rec_num),0) from fvirtualwallet_swc_records  
        where in_type = 0 and creat_time >='${startTemp}' and creat_time < '${endTemp}'`));
    //今日销毁wsc
    todayDataSum.today_destroy = getData(await mysql.queryAsync(`select IFNULL(sum(gratuity),0) from c2c_order  
        where createDate >='${startTemp}' and createDate < '${endTemp}' `));

}

//今日累计新增：会员，转账，ETH，WSC
async function sel_sql_wsc_statistics_summary(startTemp, endTemp) {

    await todayStatData(startTemp, endTemp);

    //累计ETH兑换USD兑出ETH
    let eth_usd_total_eth = getData(await mysql.queryAsync(`
    SELECT IFNULL(sum(eth_usd_total_eth),0) FROM wsc_statistics_summary 
    WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc limit 1`)) + todayDataSum.eth_usd_today_eth;

    //累计ETH兑换USD兑入USD
    let eth_usd_total_usd = getData(await mysql.queryAsync(`
    SELECT IFNULL(sum(eth_usd_total_usd),0) FROM wsc_statistics_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc  limit 1`)) + todayDataSum.eth_usd_today_usd;

    //累计WSC兑换USD兑出WSC
    let wsc_usd_total_wsc = getData(await mysql.queryAsync(`
    SELECT IFNULL(sum(wsc_usd_total_wsc),0) FROM wsc_statistics_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc  limit 1`)) + todayDataSum.wsc_usd_today_wsc;

    //累计WSC兑换USD兑入USD
    let wsc_usd_total_usd = getData(await mysql.queryAsync(`
    SELECT IFNULL(sum(wsc_usd_total_usd),0) FROM wsc_statistics_summary 
    WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}'order by  clear_time desc  limit 1`)) + todayDataSum.wsc_usd_today_usd;

    //累计WSC兑换ETH兑出WSC
    let wsc_eth_total_wsc = getData(await mysql.queryAsync(`
    SELECT IFNULL(sum(wsc_eth_total_wsc),0) FROM wsc_statistics_summary 
    WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc limit 1`)) + todayDataSum.wsc_eth_today_wsc;

    //累计WSC兑换ETH兑入ETH
    let wsc_eth_total_eth = getData(await mysql.queryAsync(`
    SELECT IFNULL(sum(wsc_eth_total_eth),0) FROM wsc_statistics_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc  limit 1`)) + todayDataSum.wsc_eth_today_eth;

    //累计USD兑换ETH兑出USD
    let usd_eth_total_usd = getData(await mysql.queryAsync(`
    SELECT IFNULL(sum(usd_eth_total_usd),0) FROM wsc_statistics_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc  limit 1`)) + todayDataSum.usd_eth_today_usd;

    //累计USD兑换ETH兑入ETH
    let usd_eth_total_eth = getData(await mysql.queryAsync(`
    SELECT IFNULL(sum(usd_eth_total_eth),0) FROM wsc_statistics_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc  limit 1`)) + todayDataSum.usd_eth_today_eth;


    //累计充值ETH数量
    let recharge_eth_total = getData(await mysql.queryAsync(`
    SELECT IFNULL(recharge_eth_total,0) FROM wsc_statistics_summary 
    WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc  limit 1`)) + todayDataSum.recharge_eth_today;

    //累计提现ETH数量
    let withdraw_eth_total = getData(await mysql.queryAsync(`
    SELECT IFNULL(withdraw_eth_total,0) FROM wsc_statistics_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc  limit 1`)) + todayDataSum.withdraw_eth_today;


    //累计挖矿收益wsc
    let total_mining_income = getData(await mysql.queryAsync(`
    SELECT IFNULL(total_mining_income,0) FROM wsc_statistics_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc  limit 1`)) + todayDataSum.today_mining_income;

    //累计销毁wsc
    let total_destroy = getData(await mysql.queryAsync(`
    SELECT IFNULL(total_destroy,0) FROM wsc_statistics_summary
     WHERE  clear_time >=DATE_SUB('${startTemp}',INTERVAL ${yester} DAY) and clear_time < '${startTemp}' order by  clear_time desc  limit 1`)) + todayDataSum.today_destroy;

    let count = getData(await mysql.queryAsync(
        `select IFNULL(count(today_new_user),0) from wsc_statistics_summary where clear_time =  '${startTemp}' `));

    if (count == 0) {

        sql_wsc_statistics_summary = `insert into wsc_statistics_summary
            (
                today_new_user,
                total_user,
                added_today_user,
                all_valid_user,

                eth_usd_today_eth,
                eth_usd_today_usd,
                wsc_usd_today_wsc,
                wsc_usd_today_usd,
                wsc_eth_today_wsc,
                wsc_eth_today_eth,
                usd_eth_today_usd,
                usd_eth_today_eth,

                eth_usd_total_eth,
                eth_usd_total_usd,
                wsc_usd_total_wsc,
                wsc_usd_total_usd,
                wsc_eth_total_wsc,
                wsc_eth_total_eth,
                usd_eth_total_usd,
                usd_eth_total_eth,

                 v0 ,
                 v1 ,
                 v2 ,
                 v3 ,
                 v4 ,
                 v5 ,



                recharge_eth_today,
                withdraw_eth_today,
                recharge_eth_total,
                withdraw_eth_total,

                today_mining_income,
                today_destroy,
                total_mining_income,
                total_destroy,
                clear_time
            )
        VALUES(

            ${todayDataSum.today_new_user},
            ${todayDataSum.total_user},
            ${todayDataSum.added_today_user},
            ${todayDataSum.all_valid_user},

            ${todayDataSum.eth_usd_today_eth},
            ${todayDataSum.eth_usd_today_usd},
            ${todayDataSum.wsc_usd_today_wsc},
            ${todayDataSum.wsc_usd_today_usd},
            ${todayDataSum.wsc_eth_today_wsc},
            ${todayDataSum.wsc_eth_today_eth},
            ${todayDataSum.usd_eth_today_usd},
            ${todayDataSum.usd_eth_today_eth},

            ${eth_usd_total_eth},
            ${eth_usd_total_usd},
            ${wsc_usd_total_wsc},
            ${wsc_usd_total_usd},
            ${wsc_eth_total_wsc},
            ${wsc_eth_total_eth},
            ${usd_eth_total_usd},
            ${usd_eth_total_eth},

            ${todayDataSum.v0},
            ${todayDataSum.v1},
            ${todayDataSum.v2},
            ${todayDataSum.v3},
            ${todayDataSum.v4},
            ${todayDataSum.v5},


            ${todayDataSum.recharge_eth_today},
            ${todayDataSum.withdraw_eth_today},
            ${recharge_eth_total},
            ${withdraw_eth_total},

            ${todayDataSum.today_mining_income},
            ${todayDataSum.today_destroy},
            ${total_mining_income},
            ${total_destroy},
            '${startTemp}')`;
        await mysql.queryAsync(sql_wsc_statistics_summary);
        logger.info("member,Transfer,ETHData,WSCData Time");
    } else {
        let upd_wsc_mill_statistics_summary =
            `UPDATE wsc_statistics_summary s1 set
    s1.today_new_user =  ${todayDataSum.today_new_user},
    s1.total_user = ${todayDataSum.total_user},
    s1.added_today_user = ${todayDataSum.added_today_user},
    s1.all_valid_user  =  ${todayDataSum.all_valid_user},

    s1.eth_usd_today_eth =  ${todayDataSum.eth_usd_today_eth}, 
    s1.eth_usd_today_usd =   ${todayDataSum.eth_usd_today_usd},
    s1.wsc_usd_today_wsc = ${todayDataSum.wsc_usd_today_wsc},
    s1.wsc_usd_today_usd =  ${todayDataSum.wsc_usd_today_usd},

    s1.wsc_eth_today_wsc  =  ${todayDataSum.wsc_eth_today_wsc},
    s1.wsc_eth_today_eth =  ${todayDataSum.wsc_eth_today_eth},
    s1.usd_eth_today_usd = ${todayDataSum.usd_eth_today_usd}, 
    s1.usd_eth_today_eth = ${todayDataSum.usd_eth_today_eth},

    s1.eth_usd_total_eth =   ${eth_usd_total_eth},
    s1.eth_usd_total_usd = ${eth_usd_total_usd},
    s1.wsc_usd_total_wsc = ${wsc_usd_total_wsc},
    s1.wsc_usd_total_usd  =  ${wsc_usd_total_usd},

    s1.wsc_eth_total_wsc =  ${wsc_eth_total_wsc},
    s1.wsc_eth_total_eth = ${wsc_eth_total_eth},
    s1.usd_eth_total_usd =  ${usd_eth_total_usd},
    s1.usd_eth_total_eth = ${usd_eth_total_eth},

    s1.v0 = ${todayDataSum.v0},
    s1.v1 = ${todayDataSum.v1},
    s1.v2 = ${todayDataSum.v2},
    s1.v3 = ${todayDataSum.v3},
    s1.v4 = ${todayDataSum.v4},
    s1.v5 = ${todayDataSum.v5},



    s1.recharge_eth_today  =  ${todayDataSum.recharge_eth_today}, 
    s1.withdraw_eth_today = ${todayDataSum.withdraw_eth_today}, 
    s1.recharge_eth_total = ${recharge_eth_total}, 
    s1.withdraw_eth_total = ${withdraw_eth_total}, 

    s1.today_mining_income = ${todayDataSum.today_mining_income},
    s1.today_destroy =  ${todayDataSum.today_destroy},
    s1.total_mining_income = ${total_mining_income},
    s1.total_destroy = ${total_destroy},



    s1.clear_time = '${startTemp}'
    WHERE s1.clear_time = '${startTemp}'`
        await mysql.queryAsync(upd_wsc_mill_statistics_summary);
        logger.info("upd_sql_wsc_mill_statistics_summary exit end.");
    }
}


function getData(data) {
    return data instanceof Array ? Object.values(data[0])[0] : Object.values(data)[0]
}

async function startService() {
    logger.info("service staring...");
    try {
        mysql = await mysqlUtil.connectAsync(config.dbOption);
    } catch (e) {
        logger.error("mysql conn failed." + e);
    }

    if (mysql == null) {
        logger.error("mysql conn failed.");
        return;
    }

    let flag = false;
    let table = {};
    while (true) {
        let day2 = new Date();
        let hour = day2.getHours();//得到小时
        let minu = day2.getMinutes();//得到分钟

        //let CURDATE = new Date().Format("yyyy-MM-dd ");

        var start = new Date();
        var startTemp = start.getFullYear() + "-" + (start.getMonth() + 1) + "-" + start.getDate();
        var dateTemp = start;
        dateTemp.setDate(dateTemp.getDate() + 1);
        var endTemp = dateTemp.getFullYear() + "-" + (dateTemp.getMonth() + 1) + "-" + dateTemp.getDate();
        console.log(startTemp)
        console.log(endTemp)
        if (minu == 0) {
            logger.info("stat yesterDay data insert .. " + startTemp + endTemp);
            await sel_sql_wsc_mill_statistics_summary(startTemp, endTemp);
            await sel_sql_wsc_usd_income_summary(startTemp, endTemp);
            await sel_sql_wsc_statistics_summary(startTemp, endTemp);
            if (hour == 0) {
                flag = false;
            }
        } else if (flag) {
            //不是00.00则进入
        } else if (hour == 0) {
            let yesterDay = new Date();
            yesterDay = yesterDay.setDate(yesterDay.getDate() - 1);
            yesterDay = new Date(yesterDay);
            yesterDay = yesterDay.Format("yyyy-MM-dd");

            if (table[yesterDay]) {
                flag = table[yesterDay];
                continue;
            }
            logger.info("stat yesterDay data update .. " + startTemp, endTemp);
            await sel_sql_wsc_mill_statistics_summary(startTemp, endTemp);
            await sel_sql_wsc_usd_income_summary(startTemp, endTemp);
            await sel_sql_wsc_statistics_summary(startTemp, endTemp);
            table[yesterDay] = true;
        }
        await new Promise((resolve, reject) => { setTimeout(resolve, 60000); });
    }
}
startService();



