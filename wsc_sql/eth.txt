 insert into eth_account_check(uid,clear_time,phone,username) select fId ,'2019-08-05',fTelephone,floginName from fuser


UPDATE eth_account_check e
    INNER JOIN (
        SELECT
            IFNULL( sum( fAmount ), 0 ) AS total,
            FUs_fId2 AS userId 
        FROM
            fvirtualcaptualoperation 
        WHERE
            fVi_fId2 = 4 
            AND fType = 1 
            AND fStatus = 3 
            AND fCreateTime >= '2019-08-02' 
            AND fCreateTime < '2019-08-03' 
        GROUP BY
            FUs_fId2 
        ) AS t1 
        SET e.eth_top_up = t1.total 
    WHERE
        e.uid = t1.userId 
        AND e.clear_time = '2019-08-02'
				
				
				
				UPDATE eth_account_check e
    INNER JOIN (
        SELECT
            userId,
            IFNULL( sum( innerAmout ), 0 ) AS total 
        FROM
            c2c_order 
        WHERE
            outerCurrencyId = 58 
            AND innerCurrencyId = 4 
            AND orderStatus = 2 
            AND createDate >= '2019-08-05' 
            AND createDate < '2019-08-06' 
        GROUP BY
            userId 
        ) AS t1 
        SET e.eth_in = t1.total 
    WHERE
        e.uid = t1.userId 
        AND e.clear_time = '2019-08-02'
				
				
				
				UPDATE eth_account_check e
        INNER JOIN (
            SELECT
                IFNULL( sum( innerAmout ), 0 ) AS total,
                userId 
            FROM
                c2c_order 
            WHERE
                outerCurrencyId = 6 
                AND innerCurrencyId = 4 
                AND orderStatus = 2 
                AND createDate >= '2019-08-05' 
                AND createDate < '2019-08-06' 
            GROUP BY
                userId 
            ) AS t1 
            SET e.eth_in = e.eth_in + t1.total 
        WHERE
            e.uid = t1.userId 
            AND e.clear_time = '2019-08-02'
						
						UPDATE eth_account_check e
        INNER JOIN (
            SELECT
                userId,
                IFNULL( sum( outerAmout ), 0 ) AS total
            FROM
                c2c_order 
            WHERE
                outerCurrencyId = 4 
                AND innerCurrencyId = 6 
                AND orderStatus = 2 
                AND createDate >= '2019-08-05' 
                AND createDate < '2019-08-06' 
            GROUP BY
                userId 
            ) AS t1 
            SET e.eth_out = t1.total
        WHERE
            e.uid = t1.userId 
            AND e.clear_time = '2019-08-02'
						
						
						
						UPDATE wsc_account_check w
        INNER JOIN (
            SELECT
                fuid,
                IFNULL( sum( fTotal ), 0 ) AS total 
            FROM
                fvirtualwallet_main 
            WHERE
                fVi_fId = 4 
                AND fLastUpdateTime > SUBDATE( '2019-08-05', INTERVAL '1' DAY ) 
                AND fLastUpdateTime < '2019-08-05' 
            GROUP BY
                fuid 
            ) AS t1 
            SET w.yester_balance = CAST(
            t1.total AS DECIMAL ( 28, 8 )) 
        WHERE
            t1.fuid = w.uid 
            AND w.clear_time = '2019-08-02'
						
						
						UPDATE eth_account_check 
    SET today_balance = IFNULL( yester_balance, 0 )+ IFNULL( eth_top_up, 0 )+ IFNULL( eth_in, 0 )- IFNULL( eth_withdraw, 0 )- IFNULL( eth_out, 0 ) 
    WHERE
        clear_time = '2019-08-02'
				
				UPDATE eth_account_check 
    SET account_differ = IFNULL( yester_balance, 0 )+ IFNULL( eth_top_up, 0 )+ IFNULL( eth_in, 0 )- IFNULL( eth_withdraw, 0 )- IFNULL( eth_out, 0 ) - IFNULL( today_balance, 0)
    WHERE
        clear_time = '2019-08-02'
						
						
				
UPDATE eth_account_check e
    INNER JOIN (
        SELECT
            IFNULL( sum( fAmount ), 0 ) AS total,
            FUs_fId2 AS userId 
        FROM
            fvirtualcaptualoperation 
        WHERE
            fVi_fId2 = 4 
            AND fType = 2 
            AND fStatus = 3 
            AND fCreateTime >= '2019-08-02' 
            AND fCreateTime < '2019-08-03' 
        GROUP BY
            FUs_fId2 
        ) AS t1 
        SET e.eth_withdraw = t1.total 
    WHERE
        e.uid = t1.userId 
        AND e.clear_time = '2019-08-02'